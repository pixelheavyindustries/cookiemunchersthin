using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class MonoBehaviorIAP : MonoBehaviour
{       
    public bool CanPurchaseDetermined { get; protected set; }
	protected bool _billingSupported;
    public bool CanPurchase { get {return BreakerPanel.PurchasingEnabled && _billingSupported && !Prefs.KidModeEnabled;}  }
	public bool HaveReceivedProducts;
	
	
#if UNITY_IPHONE
    void OnEnable()
    {
        Debug.Log ("MyStore EN");
    }
    void OnDisable()
    {
       Debug.Log ("MyStore DIS");
                // Remove all the event handlers
        StoreKitManager.purchaseSuccessful -= purchaseSuccessful;
        StoreKitManager.purchaseCancelled -= purchaseCancelled;
        StoreKitManager.purchaseFailed -= purchaseFailed;
        StoreKitManager.receiptValidationFailed -= receiptValidationFailed;
        StoreKitManager.receiptValidationRawResponseReceived -= receiptValidationRawResponseReceived;
        StoreKitManager.receiptValidationSuccessful -= receiptValidationSuccessful;
        StoreKitManager.productListReceived -= productListReceived;
        StoreKitManager.productListRequestFailed -= productListRequestFailed;
        StoreKitManager.restoreTransactionsFailed -= restoreTransactionsFailed;
        StoreKitManager.restoreTransactionsFinished -= restoreTransactionsFinished;
    }
#endif
       
    void Awake(){
		Debug.Log ("MyStore Awake");
        // Listens to all the StoreKit events.  All event listeners MUST be removed before this object is disposed!
        StoreKitManager.purchaseSuccessful += purchaseSuccessful;
        StoreKitManager.purchaseCancelled += purchaseCancelled;
        StoreKitManager.purchaseFailed += purchaseFailed;
        StoreKitManager.receiptValidationFailed += receiptValidationFailed;
        StoreKitManager.receiptValidationRawResponseReceived += receiptValidationRawResponseReceived;
        StoreKitManager.receiptValidationSuccessful += receiptValidationSuccessful;
        StoreKitManager.productListReceived += productListReceived;
        StoreKitManager.productListRequestFailed += productListRequestFailed;
        StoreKitManager.restoreTransactionsFailed += restoreTransactionsFailed;
        StoreKitManager.restoreTransactionsFinished += restoreTransactionsFinished;
		
		_billingSupported = StoreKitBinding.canMakePayments();
		Debug.Log ("Can make payments? " + _billingSupported);
		var productIdentifiers = new string[] {"com.pixelheavyindustries.cookiemunchers.coinpacka"};
		StoreKitBinding.requestProductData(productIdentifiers);
    }
       
 
#if UNITY_IPHONE
   
       
       
	public  static void productListReceived( List<StoreKitProduct> productList )
	{
//		HaveReceivedProducts = true;		
		Debug.Log( "total productsReceived: " + productList.Count );
				
		// Do something more useful with the products than printing them to the console
		foreach( StoreKitProduct product in productList ){
			Debug.Log( product.ToString() + "\n" );          
		}
	}
 
       
       
        public static void productListRequestFailed( string error )
        {
                Debug.Log( "productListRequestFailed: " + error );
        }
 
 
/*    public static void CheckHasBought(){
        foreach(StoreKitTransaction transAc in  StoreKitBinding.getAllSavedTransactions()){
            //Debug.Log("CheckHasBought="+transAc.ToString());
            if(transAc.productIdentifier == "UnlockCustomImages"){
                MyStore.hasBoughtPhotoUnlock = true;
                                PlayerPrefs.SetInt("IAP1", 1);
                        } else if(transAc.productIdentifier == "RemoveAds"){
                MyStore.hasBoughtAdRemove = true;
                        PlayerPrefs.SetInt("IAP2", 1);
                                if(M2HIOSADS2.SP!=null) M2HIOSADS2.SP.StopAllAds();
                } else
                Debug.LogError("Unknown prodyuct! "+transAc.productIdentifier);
         }
    }
     */  
 
        public static void receiptValidationSuccessful()
        {
                Debug.Log( "receipt validation successful" );
        }
       
       
        public static void receiptValidationFailed( string error )
        {
                Debug.Log( "receipt validation failed with error: " + error );
        }
       
       
        public static void receiptValidationRawResponseReceived( string response )
        {
                Debug.Log( "receipt validation raw response: " + response );
        }
       
 
        public static void purchaseFailed( string error )
        {
 //                EtceteraBinding.showAlertWithTitleMessageAndButton("Purchase failed", "Please wait a few seconds and try again ("+error+")", "OK");
                Debug.Log( "purchase failed with error: " + error );
        }
       
 
        public static void purchaseCancelled( string error )
        {
                Debug.Log( "purchase cancelled with error: " + error );
        }
       
       
        public static void purchaseSuccessful( StoreKitTransaction transaction )
        {
			//                FlurryBinding.logEvent("purchase", false);
			
			Dictionary<string,string> myDict = new Dictionary<string, string>();
			myDict.Add ("product", transaction.productIdentifier);
			//                FlurryBinding.logEventWithParameters("purchasedProduct", myDict, false);
			
			
//			Debug.Log("CheckHasBought="+transaction.ToString());
//			if(transaction.productIdentifier == "UnlockCustomImages"){
//			hasBoughtPhotoUnlock = true;
//			Jigsaw.SP.StartCoroutine(Jigsaw.SP.SelectCustomImage());
//			PlayerPrefs.SetInt("IAP1", 1);
//			}else if(transaction.productIdentifier == "RemoveAds"){
//			MyStore.hasBoughtAdRemove = true;
//			if(M2HIOSADS2.SP!=null) M2HIOSADS2.SP.StopAllAds();
//			PlayerPrefs.SetInt("IAP2", 1);
//			}else
//			Debug.LogError("Unknown prodyuct! "+transaction.productIdentifier);
//			
			
//			Debug.Log( "purchased product: " + transaction );
        }
       
       
        public static void restoreTransactionsFailed( string error )
        {
                Debug.Log( "restoreTransactionsFailed: " + error );
        }
       
       
        public static void restoreTransactionsFinished()
        {
                Debug.Log( "restoreTransactionsFinished" );
        }
#endif
}