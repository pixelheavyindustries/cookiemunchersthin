using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IOSInAppPurchasingManager : InAppPurchasingManager {
	
	public const string CoinPackA = "com.pixelheavyindustries.cookiemunchers.coinpacka";
	public const string CoinPack1000 = "com.pixelheavyindustries.cookiemunchers.coins.1000";
	
	public new static void RestartLoad()
	{
		Debug.Log ("RestartLoad: SessionStatus: " + SessionStatus + ", Status.ReadyForPurchase: " + Status.ReadyForPurchase);
		NumLoadTries = 0;
		if (SessionStatus < Status.ReadyForPurchase)
			Init ();
	}
	
	public new static void Init()
	{
		Debug.Log ("IOSInAppPurchasingManager.init");
		InAppPurchasingManager.Init();
		
		if (SimulatePurchaseUnavailable)
			_billingSupported = false;
		else
			_billingSupported = StoreKitBinding.canMakePayments();
		
		CanPurchaseDetermined = true;
		
		StoreKitManager.productListReceived -= productListReceived;
		StoreKitManager.productListRequestFailed -= productListRequestFailed;
        StoreKitManager.purchaseSuccessful -= purchaseSuccessful;
        StoreKitManager.purchaseCancelled -= purchaseCancelled;
        StoreKitManager.purchaseFailed -= purchaseFailed;
        StoreKitManager.restoreTransactionsFailed -= restoreTransactionsFailed;
        StoreKitManager.restoreTransactionsFinished -= restoreTransactionsFinished;
		
		if (!SimulateNoStoreResponse)
			StoreKitManager.productListReceived += productListReceived;
		StoreKitManager.productListRequestFailed += productListRequestFailed;
        StoreKitManager.purchaseSuccessful += purchaseSuccessful;
        StoreKitManager.purchaseCancelled += purchaseCancelled;
        StoreKitManager.purchaseFailed += purchaseFailed;
        StoreKitManager.restoreTransactionsFailed += restoreTransactionsFailed;
        StoreKitManager.restoreTransactionsFinished += restoreTransactionsFinished;
		
		var productIdentifiers = new string[] {CoinPackA, CoinPack1000};
		if (SimulateBadProductList)
			productIdentifiers = new string[] {"NonexistentProductId"};
		
		AllProducts = new List<ProductDetails>();
		StoreKitBinding.requestProductData(productIdentifiers);
		SessionStatus = Status.LoadingProducts;
		
		Debug.Log ("Init finished: " + 
			(CanPurchaseDetermined ? "determined, " : "not determined, ") + 
			(CanPurchase ? "Can purchase, " : "Can't purchase"));
	}
	
	public static void Destroy()
	{
		StoreKitManager.productListReceived -= productListReceived;
		StoreKitManager.productListRequestFailed -= productListRequestFailed;
        StoreKitManager.purchaseSuccessful -= purchaseSuccessful;
        StoreKitManager.purchaseCancelled -= purchaseCancelled;
        StoreKitManager.purchaseFailed -= purchaseFailed;
        StoreKitManager.restoreTransactionsFailed -= restoreTransactionsFailed;
        StoreKitManager.restoreTransactionsFinished -= restoreTransactionsFinished;
	}

	public static void productListReceived( List<StoreKitProduct> productList )
	{
		Debug.Log ("productListReceived: " + 
			(SimulateStoreErrorResponse ? "Simulate Store Error, " : "") + 
			"ProductList.Count: " + productList.Count + 
			", NumLoadTries: " + NumLoadTries);
		
		if (SimulateStoreErrorResponse)
		{
			productListRequestFailed("SKErrorUnknown");
			return;
		}
		
		if (productList.Count == 0)
		{
			if (NumLoadTries < MaxLoadTries)
			{
				NumLoadTries++;
				Init ();
				return;
			}
		}
		
		AreProductsRetrieved = true;
		SessionStatus = Status.ReadyForPurchase;
		
		foreach(StoreKitProduct p in productList)
		{
			productHistory.Add ("----> " + p.productIdentifier + "  " + p.description);
			AllProducts.Add (new ProductDetails(){
				ID = p.productIdentifier,
				Price = float.Parse(p.price),
				DisplayPrice = p.formattedPrice,
				Description = p.title
			});
		}
	}
		
	public static void productListRequestFailed( string error )
    {
		Debug.Log ("productListRequestFailed: " + NumLoadTries);
		if (NumLoadTries < MaxLoadTries)
		{
			NumLoadTries++;
			Init ();
			return;
		}
		
    	Debug.Log( "productListRequestFailed: final: "  + NumLoadTries + "   " + error );
		SessionStatus = InAppPurchasingManager.Status.LoadingProductsFailed;
		Error = error;
    }	
	
    public static new bool PurchaseProduct(string productId)
    {
		if (SessionStatus == Status.InSession)
			return false;
		
		SessionStatus = Status.InSession;
		StoreKitBinding.purchaseProduct(productId, 1);
		CurrentSession = new PurchaseSession { ProductId = productId };
		return true;
    }
	
    public static void purchaseFailed( string error )
    {
        Debug.Log( "purchase failed with error: " + error );
		if (CurrentSession != null)
		{
			CurrentSession.GrantProduct = false;
			CurrentSession.GrantProductExplanation = error;
		}

		SessionStatus = Status.PurchaseFailed;
    }
   

    public static void purchaseCancelled( string error )
    {
        Debug.Log( "purchase cancelled with error: " + error );
		if (CurrentSession != null)
		{
			CurrentSession.GrantProduct = false;
			CurrentSession.GrantProductExplanation = error;
		}
		
		SessionStatus = Status.PurchaseFailed;
    }
   
   
    public static void purchaseSuccessful( StoreKitTransaction transaction )
    {
		Debug.Log("Purchase successful: " + transaction.productIdentifier);
		
		GrantProduct(transaction.productIdentifier);
		
		if (CurrentSession != null)
		{
			CurrentSession.GrantProduct = true;
			CurrentSession.GrantProductExplanation = "Purchase successful!";
		}
		
		SessionStatus = Status.PurchaseSuccessful;
    }
	
    public static void restoreTransactionsFailed( string error )
    {
        Debug.Log( "restoreTransactionsFailed " + error );
    }
	
    public static void restoreTransactionsFinished()
    {
        Debug.Log( "restoreTransactionsCompleted" );
    }

	private static new void GrantProduct(string productId)
	{
		switch (productId)
		{
		case CoinPack1000:
			Prefs.Money += 1000;
			break;
			
		default:
			break;
		}
		
	}
}
