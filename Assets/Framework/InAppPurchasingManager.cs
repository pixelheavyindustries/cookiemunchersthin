using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class InAppPurchasingManager {
	
	public enum Status
	{
		Uninitialized,
		LoadingProducts,
		LoadingProductsFailed,
		ReadyForPurchase,
		InSession,
		PurchaseSuccessful,
		PurchaseFailed
	}
	
	public class ProductDetails
	{
		public string ID;
		public string Description;
		public float Price;
		public String DisplayPrice;
	}
	
	public class PurchaseSession{
		public string ProductId;
		public bool? GrantProduct;
		public string GrantProductExplanation;
	}
	
	public static List<ProductDetails> AllProducts { get; protected set; }
	
	public static PurchaseSession CurrentSession = null;	
	public static int NumLoadTries = 0;
	public static int MaxLoadTries = 5;
	
	public static Status SessionStatus = Status.Uninitialized;
	public static String Error = "";
	public static bool AreProductsRetrieved { get; protected set; }
    public static bool CanPurchaseDetermined { get; protected set; }
	protected static bool _billingSupported;
    public static bool CanPurchase { get {return BreakerPanel.PurchasingEnabled && _billingSupported && !Prefs.KidModeEnabled;}  }
	
	
	//flags for testing various errors
	public static bool SimulateNoStoreResponse = false;
	public static bool SimulateStoreErrorResponse = false;
	public static bool SimulateBadProductList = false;	
	public static bool SimulatePurchaseUnavailable = false;
	
	public static void RestartLoad()
	{
		Debug.Log ("Base.RestartLoad: SessionStatus: " + SessionStatus + ", Status.ReadyForPurchase: " + Status.ReadyForPurchase);
		NumLoadTries = 0;
		if (SessionStatus < Status.ReadyForPurchase)
			Init ();
	}
	
    public static void Init()
    {
		Debug.Log ("InAppPurchasingManager.base.init");
		AllProducts = new List<ProductDetails>();
		AreProductsRetrieved = false;
		CanPurchaseDetermined = false;
    }
	
    //for testing purchasing
    public static List<string> productHistory = new List<string>();

    public static bool PurchaseProduct(string productId)
    {
		return false;
/*        productHistory.Add("purchase " + productId);
        IABAndroid.purchaseProduct(productId);*/
    }

    public static void RestoreEverything()
    {
/*        productHistory.Add("Restore everything");
        IABAndroid.restoreTransactions();*/
    }

    public static void PurchaseSucceededEvent(string productId)
    {
/*        productHistory.Add("Success: " + productId);
        OnAttackModePurchased();*/
    }

    public static void PurchaseCancelledEvent(string productId)
    {
        productHistory.Add("Cancel " + productId);
    }

    public static void PurchaseRefundedEvent(string productId)
    {
        productHistory.Add("Refund " + productId);
    }

    public static void PurchaseFailedEvent(string productId)
    {
        productHistory.Add("Failed " + productId);
    }

    private static void OnCoinsPurchased()
    {
        productHistory.Add("Finish");
/*        Prefs.PremiumPackUnlocked = true;
        ScreenDimensions.Reinitialize(PlatformSpecifics.AllowAds && !Prefs.PremiumPackUnlocked);
        PlatformSpecifics.HideAd();*/
    }
	
	private static void GrantProduct(string productId)
	{
	}
}

//TODO: on clicky endless mode
//  premium1unlocked?
//      yes: start endles
//      no:  show purchase dialog

//  purchase dialog:
//  wat do?
//  details
//  buy it button
//  restore button
