using UnityEngine;
using System.Collections.Generic;


public class MoPubUIManager : MonoBehaviour
{
#if UNITY_ANDROID
	void OnGUI()
	{
		float yPos = 5.0f;
		float xPos = 5.0f;
		float width = ( Screen.width >= 800 || Screen.height >= 800 ) ? 320 : 160;
		float height = ( Screen.width >= 800 || Screen.height >= 800 ) ? 80 : 40;
		float heightPlus = height + 10.0f;
	
	
		if( GUI.Button( new Rect( xPos, yPos, width, height ), "Show Ad Banner" ) )
		{
			// place it on the bottom
			var density = MoPubAndroid.getScreenDensity();
			int y = (int)( Screen.height - 50 * density );
            MoPubAndroid.createBanner("agltb3B1Yi1pbmNyDQsSBFNpdGUYlffJEgw", 0, y);
		}
	
	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Destroy Banner" ) )
		{
			MoPubAndroid.destroyBanner();
		}
	
	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Set Banner Keywords" ) )
		{
			MoPubAndroid.setBannerKeywords( "coffee,tea" );
		}
	
	
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Report App Open" ) )
		{
			MoPubAndroid.reportApplicationOpen();
		}
	
	
		xPos = Screen.width - width - 5.0f;
		yPos = 5.0f;
		
		if( GUI.Button( new Rect( xPos, yPos, width, height ), "Request Interstitial" ) )
		{
			MoPubAndroid.requestInterstitalAd( "YOUR_AD_UNIT_ID" );
		}
		
		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Show Interstitial" ) )
		{
			MoPubAndroid.showInterstitalAd();
		}
		
		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Show Banner" ) )
		{
			MoPubAndroid.hideBanner( false );
		}
		
		
		if( GUI.Button( new Rect( xPos, yPos += heightPlus, width, height ), "Hide Banner" ) )
		{
			MoPubAndroid.hideBanner( true );
		}

	}
#endif
}
