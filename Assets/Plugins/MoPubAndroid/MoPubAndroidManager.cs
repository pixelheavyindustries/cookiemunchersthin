using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class MoPubAndroidManager : MonoBehaviour
{
	// Fired when a new ad is loaded
	public static event Action onAdLoadedEvent;
	
	// Fired when an ad fails to load
	public static event Action onAdFailedEvent;
	
	// Fired when an interstitial ad is loaded
	public static event Action onInterstitialLoadedEvent;
	
	// Fired when an interstitial ad fails to load
	public static event Action onInterstitialFailedEvent;


	void Awake()
	{
		// Set the GameObject name to the class name for easy access from Obj-C
		gameObject.name = this.GetType().ToString();
		DontDestroyOnLoad( this );
	}


	public void onAdLoaded( string empty )
	{
		if( onAdLoadedEvent != null )
			onAdLoadedEvent();
	}


	public void onAdFailed( string empty )
	{
		if( onAdFailedEvent != null )
			onAdFailedEvent();
	}


	public void onInterstitialLoaded( string empty )
	{
		if( onInterstitialLoadedEvent != null )
			onInterstitialLoadedEvent();
	}


	public void onInterstitialFailed( string empty )
	{
		if( onInterstitialFailedEvent != null )
			onInterstitialFailedEvent();
	}


}

