using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class MoPubAndroidEventListener : MonoBehaviour
{

	void OnEnable()
	{
		// Listen to all events for illustration purposes
		MoPubAndroidManager.onAdLoadedEvent += onAdLoadedEvent;
		MoPubAndroidManager.onAdFailedEvent += onAdFailedEvent;
		MoPubAndroidManager.onInterstitialLoadedEvent += onInterstitialLoadedEvent;
		MoPubAndroidManager.onInterstitialFailedEvent += onInterstitialFailedEvent;
	}


	void OnDisable()
	{
		// Remove all event handlers
		MoPubAndroidManager.onAdLoadedEvent -= onAdLoadedEvent;
		MoPubAndroidManager.onAdFailedEvent -= onAdFailedEvent;
		MoPubAndroidManager.onInterstitialLoadedEvent -= onInterstitialLoadedEvent;
		MoPubAndroidManager.onInterstitialFailedEvent -= onInterstitialFailedEvent;
	}



	void onAdLoadedEvent()
	{
		Debug.Log( "onAdLoadedEvent" );
	}


	void onAdFailedEvent()
	{
		Debug.Log( "onAdFailedEvent" );
	}


	void onInterstitialLoadedEvent()
	{
		Debug.Log( "onInterstitialLoadedEvent" );
	}


	void onInterstitialFailedEvent()
	{
		Debug.Log( "onInterstitialFailedEvent" );
	}


}


