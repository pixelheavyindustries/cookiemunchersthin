using UnityEngine;
using System.Collections;

public class RenderDepths{
	public static int BACKGROUND = 5000;
	public static int TUMMY_METER = -10;
	public static int DIALOG = -100;
	public static int DIALOG_ELEMENT = -110;
	public static int DIALOG_OVERLAY = -120;
}
