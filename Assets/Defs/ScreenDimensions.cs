using UnityEngine;
using System.Collections;

public class ScreenDimensions {

    public static float CameraFov = 60.0f;
    public static float CameraDistance = 50.0f;
    public static Vector2 ScreenDimensionsInMeters;
    public static float PixelsPerMeter;
    public static Rect GameAreaInMeters;
    public static Rect GameAreaInPixels;
    public static float BlockSize;
    public static int NumBlocksHigh = 20;
    public static bool Ads { get; private set; }
    private static int ADHEIGHT = 70;
    public static int AdHeight { get; private set; }

//    public int NumBlocksWide = 14;

    private static bool _initialized = false;
    public static bool Initialized { get { return _initialized; } }

    public static void RepositionCamera()
    {
        Camera.main.fieldOfView = CameraFov;
        Camera.main.transform.position = new Vector3(0, 0, 0 - CameraDistance);
    }

    public static void Reinitialize(bool ads)
    {
        _initialized = false;
        Initialize(ads);
    }

    public static void Initialize(bool ads)
    {
        if (_initialized)
            return;

        _initialized = true;

        //even if ads is true, turn it off if we're a web build
        Ads = ads && !BuildConfig.IsWeb();
        AdHeight = Ads ? ADHEIGHT : 0;

        //setup the camera
        Camera.main.fieldOfView = CameraFov;
        Camera.main.transform.position = new Vector3(0, 0, 0 - CameraDistance);

        ScreenDimensionsInMeters = PHIUtil.GetScreenDimensions();
        BlockSize = ScreenDimensionsInMeters.y / NumBlocksHigh;
        PixelsPerMeter = Screen.height / ScreenDimensionsInMeters.y;

        var heightInMeters = ScreenDimensionsInMeters.y;
        var widthInMeters = ScreenDimensionsInMeters.x;
        GameAreaInMeters = new Rect(0 - widthInMeters / 2, 0 - heightInMeters / 2, widthInMeters, heightInMeters);

        GameAreaInPixels = new Rect(0, 0, Screen.width, Screen.height - AdHeight);
    }

	void Start () {
	}
}
