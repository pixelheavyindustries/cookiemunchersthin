using UnityEngine;
using System.Collections;
using DifficultyLevel = ClimberMain.DifficultyLevel;
using System;

public class Prefs : CookieManagerBase {

    public static string Pid
    {
        get
        {
            var currVal = GetStringCookie("Pid", "");
            if (currVal == "")
            {
                var guid = Guid.NewGuid().ToString("N");
                if (BuildConfig.IsWeb())
                    guid = "WEB" + guid.Substring(3);

                SetCookie("Pid", guid);
                return guid;
            }

            return currVal;
        }

        set
        {
            SetCookie("Pid", value);
        }
    }

    public static int Version
    {
        get { return GetIntCookie("NumericVersion", 0); }
        set { SetCookie("NumericVersion", value); }
    }

    public static int HighestDifficultyUnlocked
    {
        get { return GetIntCookie("HighestDifficultyUnlocked", 1); }
        set { SetCookie("HighestDifficultyUnlocked", value); }
    }

    public static int CurrentDifficulty
    {
        get { return GetIntCookie("CurrentDifficulty", 0); }
        set { SetCookie("CurrentDifficulty", value); }
    }

    public static int GetMissionLevel(DifficultyLevel diff)
    {
        return GetIntCookie("MissionLevel" + diff.ToString(), 1);
    }
    public static void SetMissionLevel(DifficultyLevel diff, int val)
    {
        SetCookie("MissionLevel" + diff.ToString(), val);
    }

    public static bool GetTrainingComplete()
    {
        return GetHighestMissionLevelUnlocked(ClimberMain.DifficultyLevel.Easy) > 4;
    }
	
    public const string MODE_TRAINING = "training";
    public const string MODE_CLASSIC = "classic";
    public const string MODE_ENDLESS = "endless";
    public const string MODE_ATTACK = "attack";
    public const string MODE_CRAZY8S = "crazy8s";
    public const string MODE_BONUSROUND = "bonusround";

    public static string GetGameMode()
    {
        return GetStringCookie("GameMode", MODE_CLASSIC);
    }
    public static void SetGameMode(string mode)
    {
        SetCookie("GameMode", mode);
    }

    public static float GetEndlessRecord(DifficultyLevel diff)
    {
        return GetFloatCookie("EndlessRecord" + diff.ToString(), 0);
    }
    public static void SetEndlessRecord(DifficultyLevel diff, float val)
    {
        SetCookie("EndlessRecord" + diff.ToString(), val);
    }

    public static float GetEndlessHeightRecord(DifficultyLevel diff)
    {
        return GetFloatCookie("EndlessHeightRecord" + diff.ToString(), 0);
    }
    public static void SetEndlessHeightRecord(DifficultyLevel diff, float val)
    {
        SetCookie("EndlessHeightRecord" + diff.ToString(), val);
    }
	
    public static int GetHighestMissionLevelPossible(DifficultyLevel diff)
    {
        return GetIntCookie("GetHighestMissionLevelPossible" + diff.ToString(), 1);
    }
    public static void SetHighestMissionLevelPossible(DifficultyLevel diff, int val)
    {
        SetCookie("GetHighestMissionLevelPossible" + diff.ToString(), val);
    }

    public static int GetHighestMissionLevelUnlocked(DifficultyLevel diff)
    {
        return GetIntCookie("HighestMissionLevel" + diff.ToString(), 1);
    }
    public static void SetHighestMissionLevelUnlocked(DifficultyLevel diff, int val)
    {
        SetCookie("HighestMissionLevel" + diff.ToString(), val);
    }
	
    public static int GetLastStarScore()
    {
        return GetIntCookie("LastStarScore", 0);
    }
    public static void SetLastStarScore(int val)
    {
        SetCookie("LastStarScore", val);
    }
	
    public static int GetStarScore(DifficultyLevel diff, int mission)
    {
        return GetIntCookie("StarScore" + diff.ToString() + "_" + mission.ToString(), 0);
    }
    public static void SetStarScore(DifficultyLevel diff, int mission, int val)
    {
        SetCookie("StarScore" + diff.ToString() + "_" + mission.ToString(), val);
    }

    public static float Get2StarScore(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("TwoStarScore", diff, subDiff, 1f, 1f);
    }
    public static void Set2StarScore(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("TwoStarScore", diff, upper, val);
    }

    public static float Get3StarScore(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("ThreeStarScore", diff, subDiff, 2f, 2f);
    }
    public static void Set3StarScore(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("ThreeStarScore", diff, upper, val);
    }

    public static bool HaveShownIntroComic
    {
        get { return GetIntCookie("HaveShownIntroComic", 0) > 0; }
        set { SetCookie("HaveShownIntroComic", value ? 1 : 0); }
    }

    public static int GetTotalPlays(DifficultyLevel diff, int level)
    {
        return GetIntCookie(String.Format("TotalPlays_{0}_{1}", (int)diff, level), 0);
    }
    public static void SetTotalPlays(DifficultyLevel diff, int level, int plays)
    {
        SetCookie(String.Format("TotalPlays_{0}_{1}", (int)diff, level), plays);
    }

    public static void SetBonkProofingMultiplier(float mult)
    {
        SetCookie("BonkProofingMultiplier", mult);
    }
    public static float GetBonkProofingMultiplier()
    {
        return GetFloatCookie("BonkProofingMultiplier", 3f);
    }

    public static void SetComboTimeLimit(float limit)
    {
        SetCookie("ComboTimeLimit", limit);
    }
    public static float GetComboTimeLimit(DifficultyLevel diff)
    {
        return UpgradeManager.GetUpgradeValue(UpgradeType.ComboTimeout);
        //return GetFloatCookie("ComboTimeLimit", .8f);
    }

    public static void SetBonusBoostLevel(int level)
    {
        SetCookie("BonusBoostLevel", level);
    }
    public static int GetBonusBoostLevel()
    {
        return GetIntCookie("BonusBoostLevel", 4);
    }

    public static void SetBonusBoostSpeed(float level)
    {
        SetCookie("BonusBoostSpeed", level);
    }
    public static float GetBonusBoostSpeed()
    {
        return GetFloatCookie("BonusBoostSpeed", 40f);
    }

    public static int Money
    {
        get { return GetIntCookie("Money", 50); }
        set { SetCookie("Money", value); }
    }

    public static int MoneyThisLevel
    {
        get { return GetIntCookie("MoneyThisLevel", 0); }
        set { SetCookie("MoneyThisLevel", value); }
    }

    public static int LifetimeMoney
    {
        get { return GetIntCookie("LifetimeMoney", 0); }
        set { SetCookie("LifetimeMoney", value); }
    }

    public static bool ShowDialog(string dialogId)
    {
        return GetIntCookie("ShowDialog_" + dialogId, 1) == 1;
    }

    public static void SetShowDialog(string dialogId, bool show)
    {
        SetCookie("ShowDialog_" + dialogId, show ? 1 : 0);
    }

    public static float ZipperSpeed
    {
        get { return GetFloatCookie("ZipperSpeed", 45); }
        set { SetCookie("ZipperSpeed", value); }
    }

    public static float AltitudeMultiplier
    {
        get { return GetFloatCookie("AltitudeMultiplier", 1000f); }
        set { SetCookie("AltitudeMultiplier", value); }
    }


    public static bool HaveSetDefaults
    {
        get { return GetIntCookie("HaveSetDefaults", 0) > 0; }
        set { SetCookie("HaveSetDefaults", value ? 1 : 0); }
    }

    public static float GetStaminaBurn(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("StaminaBurn", diff, subDiff, 2f, 2f);
    }
    public static void SetStaminaBurn(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("StaminaBurn", diff, upper, val);
    }

    public static float GetStaminaFill(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("StaminaFill", diff, subDiff, 2f, 2f);
    }
    public static void SetStaminaFill(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("StaminaFill", diff, upper, val);
    }

    public static float GetMaxSpeed(DifficultyLevel diff)
    {
        return GetFloatCookie("MaxSpeed" + diff.ToString(), 2f);
    }
    public static void SetMaxSpeed(DifficultyLevel diff, float val)
    {
        SetCookie("MaxSpeed" + diff.ToString(), val);
    }

    public static float GetCookieMoveTime(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("CookieMoveTime", diff, subDiff, 2f, 2f);
    }
    public static void SetCookieMoveTime(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("CookieMoveTime", diff, upper, val);
    }

    public static float GetBonusOccurence(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("BonusOccurence", diff, subDiff, 2f, 2f);
    }
    public static void SetBonusOccurence(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("BonusOccurence", diff, upper, val);
    }

    public static float GetObstacleOccurence(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("ObstacleOccurence", diff, subDiff, 2f, 2f);
    }
    public static void SetObstacleOccurence(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("ObstacleOccurence", diff, upper, val);
    }

    public static float GetGoodCookies(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("GoodCookies", diff, subDiff, 2f, 2f);
    }
    public static void SetGoodCookies(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("GoodCookies", diff, upper, val);
    }

    public static float GetWinAltitude(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("WinAltitude", diff, subDiff, 2f, 2f);
    }
    public static void SetWinAltitude(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("WinAltitude", diff, upper, val);
    }

    public static float GetAsteroidDamage(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("AsteroidDamage", diff, subDiff, 2f, 2f);
    }
    public static void SetAsteroidDamage(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("AsteroidDamage", diff, upper, val);
    }

    public static float GetGoodCookieMaxSeparation(DifficultyLevel diff, float subDiff)
    {
        return GetRangedValue("GoodCookieMaxSeparation", diff, subDiff, 2f, 2f);
    }
    public static void SetGoodCookieMaxSeparation(DifficultyLevel diff, bool upper, float val)
    {
        SetRangedValue("GoodCookieMaxSeparation", diff, upper, val);
    }

    public static Cookie.Color GetLastMuncherColor()
    {
        return (Cookie.Color)GetIntCookie("LastMuncherColor", 0);
    }
    public static void SetLastMuncherColor(Cookie.Color color)
    {
        SetCookie("LastMuncherColor", (int)color);
    }
    public static Cookie.CookieShape GetLastMuncherShape()
    {
        return (Cookie.CookieShape)GetIntCookie("LastMuncherShape", 1);
    }
    public static void SetLastMuncherShape(Cookie.CookieShape shape)
    {
        SetCookie("LastMuncherShape", (int)shape);
    }


    private static float GetRangedValue(string key, DifficultyLevel diff, float subDiff, float defaultLower, float defaultUpper)
    {
        var lower = GetFloatCookie(key + diff.ToString() + "lower", defaultLower);
        var upper = GetFloatCookie(key + diff.ToString() + "upper", defaultUpper);
        return lower + (upper - lower) * subDiff;
    }

    private static void SetRangedValue(string key, DifficultyLevel diff, bool upper, float val)
    {
        SetCookie(key + diff.ToString() + (upper ? "upper" : "lower"), val);
    }

    public static float GetCookieSeparation(DifficultyLevel diff)
    {
        return GetFloatCookie("CookieSeparation" + diff.ToString(), 1f);
    }
    public static void SetCookieSeparation(DifficultyLevel diff, float val)
    {
        SetCookie("CookieSeparation" + diff.ToString(), val);
    }

    public static int GetUpgradeLevel(UpgradeType type)
    {
        return GetIntCookie("Upgrade_" + type.ToString(), 0);
    }
    public static void SetUpgradeLevel(UpgradeType type, int val)
    {
        SetCookie("Upgrade_" + type.ToString(), val);
    }

    public static bool Muted
    {
        get { return GetIntCookie("Muted", 0) > 0; }
        set { SetCookie("Muted", value ? 1 : 0); }
    }

    public static bool PremiumPackUnlocked
    {
        get { return GetIntCookie("PremiumPackUnlocked", 0) > 0; }
        set { SetCookie("PremiumPackUnlocked", value ? 1 : 0); }
    }
	
    public static bool KidModeEnabled
    {
        get { return GetIntCookie("KidModeEnabled", 0) > 0; }
        set { SetCookie("KidModeEnabled", value ? 1 : 0); }
    }
	
    public static bool ShowNextLevelButtonOnVending
    {
        get { return GetIntCookie("ShowNextLevelButtonOnVending", 0) > 0; }
        set { SetCookie("ShowNextLevelButtonOnVending", value ? 1 : 0); }
    }
	
	public static bool HaveShownHardInstructions
	{
        get { return GetIntCookie("HaveShownHardInstructions", 0) > 0; }
        set { SetCookie("HaveShownHardInstructions", value ? 1 : 0); }
	}

	public static bool HaveShownExpertInstructions
	{
        get { return GetIntCookie("HaveShownExpertInstructions", 0) > 0; }
        set { SetCookie("HaveShownExpertInstructions", value ? 1 : 0); }
	}
/*    public static bool EndlessModeUnlocked
    {
        get { return GetIntCookie("EndlessModeUnlocked", 0) > 0; }
        set { SetCookie("EndlessModeUnlocked", value ? 1 : 0); }
    }

    public static bool AttackModeUnlocked
    {
        get { return GetIntCookie("AttackModeUnlocked", 0) > 0; }
        set { SetCookie("AttackModeUnlocked", value ? 1 : 0); }
    }*/
}
