using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VendingMachineRack : MonoBehaviour {

    protected Upgrade _upgrade;

    public string Name { get { return _upgrade.Name; } }

    public bool CanDispense { get { return _upgrade.CurrLevel < _upgrade.MaxLevel; } }
    public int CurrLevel { get { return _upgrade.CurrLevel; } }
    public int MaxLevel { get { return _upgrade.MaxLevel; } }

    public int CurrCost { get { return _upgrade.Upgrades[CurrLevel].Price; } }
    public String CurrDescription { get { return _upgrade.Upgrades[CurrLevel].Description; } }
	
	private GameObject _emptyRack;
	private GameObject _viewButton;
    private List<GameObject> _items;
    private List<GameObject> _dispensedItems;
    private bool _dispensing = false;
    private GameObject _dispensingItem;

    public void Init(Upgrade upgrade, Vector3 position, float itemWidth)
    {
        this._upgrade = upgrade;
        _items = new List<GameObject>();
        _dispensedItems = new List<GameObject>();

		
        _emptyRack = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended,
                                        "menus/vending/EmptyCurl", 150, true, 1, 1);
        var bbProps = _emptyRack.GetComponent<BillboardProps>();
        bbProps.SetWidthInMeters(itemWidth);
        _emptyRack.transform.position = position + new Vector3(0, 0, 2);
		
		if (CurrLevel < MaxLevel)
		{
			_viewButton = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended,
                                        "menus/vending/View", 50, true, 1, 1);
			_viewButton.transform.position = position + new Vector3(2f, -2.5f, -1f);
		}
		
        //start + 1 since we don't want to make one for the current level
        for (var i = CurrLevel + 1; i <= _upgrade.MaxLevel; i++)
        {
			//Debug.Log ("Making " + _upgrade.Type + "  " + i);
            //make one
            try
            {
                var bonus = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended,
                                                GetImagePath(), 150, true, 1, 1);

                bbProps = bonus.GetComponent<BillboardProps>();
                bbProps.SetWidthInMeters(itemWidth);
                bonus.transform.position = position + new Vector3(0, 0, (i - CurrLevel)/5f);
                bonus.AddComponent<Tweenk>();
                _items.Add(bonus);
            }
            catch (Exception e) {
//                Debug.LogError(e.Message);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
	
	}
	
	public String GetImagePath()
	{
		return String.Format("Menus/Vending/{0}1", _upgrade.Type.ToString());
	}

    public void Dispense()
    {
        if (_dispensing)
            return;

        _dispensing = true;

        if (_items.Count > 0)
        {
            UpgradeManager.DoUpgrade(_upgrade.Type);

            CookieSoundManager.PlayVendingMachineSound();
            _dispensingItem = _items[0];
            _items.RemoveAt(0);
            _dispensedItems.Add(_dispensingItem);
            var tweenk = _dispensingItem.GetComponent<Tweenk>();
            tweenk.Callback(1.5f, CrawlTowardsFront);
        }
    }

    public bool CrawlTowardsFront()
    {
        var tweenk = _dispensingItem.GetComponent<Tweenk>();
        tweenk.Y(_dispensingItem.transform.position.y - .75f, 1.8f, Tweenk.Easing.EaseNone);
        tweenk.Z(-2, 1.8f, Tweenk.Easing.EaseNone);
        tweenk.Callback(1.9f, FallTowardsBottom);
        return true;
    }

    public bool FallTowardsBottom()
    {
        var tweenk = _dispensingItem.GetComponent<Tweenk>();
        tweenk.Y(_dispensingItem.transform.position.y - 50f, 1f, Tweenk.Easing.EaseInQuad);
        tweenk.Callback(.9f, DispenseComplete);
        return true;
    }

    public bool DispenseComplete()
    {
        //print("Dispense complete");
        _dispensing = false;
        return true;
    }

    public void Destroy()
    {
		Destroy (_emptyRack);
		if (_viewButton != null)
			Destroy (_viewButton);
		
        foreach (var i in _items)
            Destroy(i);

        foreach (var di in _dispensedItems)
            Destroy(di);
    }
}
