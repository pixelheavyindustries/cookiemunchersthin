using UnityEngine;
using System.Collections;

public class IntroComicScreen : MenuScreen
{
	// Update is called once per frame
    public override void RenderMainLayer()
    {
		var btnTex = TextureCache.GetCachedTexture("Gui/Dialog/Next") as Texture2D;
		var btnHeight = ScreenDimensions.GameAreaInPixels.height * .1f;
		var btnWidth = btnTex.width * btnHeight / btnTex.height; 
		var padding = ScreenDimensions.GameAreaInPixels.height * .03f;
		
        var pos = new Rect( Screen.width - btnWidth - padding,
                            Screen.height - btnHeight - padding,
                            btnWidth,
                            btnHeight);

		if (GUI.Button(pos, btnTex, PHIUtil.MakeButtonGUIStyle()))
        {
            PlatformSpecifics.CreateAd();
            MenuManager.ForwardTo(MenuManager.MODE);
        }
	}
}
