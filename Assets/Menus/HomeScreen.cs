using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class HomeScreen : MenuScreen
{
	private float _startTime;
	private float _resetTimeout = 600f;
	
	public void Start()
	{
		Prefs.CurrentDifficulty = (int)ClimberMain.DifficultyLevel.Easy;
		gameObject.AddComponent<AutoplayMain>();
		_startTime = GameClock.CurrTime;
	}
	
	public void Update()
	{
		if (GameClock.CurrTime > _startTime + _resetTimeout)
			Application.LoadLevel("MenuMain");
	}
	
	// Use this for initialization
	public override void RenderMainLayer () {
		
		DrawElements();
		
        var style = new GUIStyle();
        style.alignment = TextAnchor.LowerRight;
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height), String.Format("v. {0} ", DefaultSetter.VERSION), style);
        GUI.Label(new Rect(0, 0, Screen.width-2, Screen.height-2), String.Format("v. {0} ", DefaultSetter.VERSION), style);
        style.normal.textColor = Color.white;
        GUI.Label(new Rect(0, 0, Screen.width - 1, Screen.height - 1), String.Format("v. {0} ", DefaultSetter.VERSION), style);
		
		GUI.DrawTexture(new Rect(0, 0, ScreenDimensions.GameAreaInPixels.width, ScreenDimensions.GameAreaInPixels.height*.6f), 
			TextureCache.GetCachedTexture("Menus/SplashScreen"), ScaleMode.ScaleToFit);

/*        if (GUI.Button(new Rect(0, 0, 100, 50), "reset unlocks"))
        {
            Prefs.PremiumPackUnlocked = false;
        }*/
/*        if (GUI.Button(new Rect(0, 0, 100, 50), "unlock endless"))
        {
            Prefs.PremiumPackUnlocked = true;
        }*/		
	}
}
