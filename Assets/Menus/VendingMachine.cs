using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class VendingMachine : MenuScreen
{
    public static VendingMachineRack CurrRack;

    protected bool _goingBack = false;

    protected float _itemWidth;

    private bool _mouseDown = false;
    private Texture2D _sceneTexture;
	private Texture2D _nextButtonTexture;
	private Texture2D _coinBackgroundTexture;

    protected List<GameObject> _cleanupList;
    protected List<VendingMachineRack> _racks;
	
	protected float FirstRackAt = .324f;
	protected float SecondRackAt = .092f;
	protected float ThirdRackAt = -.141f;	
	
	protected bool _showNextButton;

	// Use this for initialization
	public void Start () {
        //base it off height, since vending machine image might stretch offscreen horizontally, but will be fully on screen vertically
        _itemWidth = ScreenDimensions.ScreenDimensionsInMeters.y / 6f;
		var metersHeight = ScreenDimensions.ScreenDimensionsInMeters.y;
        var itemSpacing = ScreenDimensions.ScreenDimensionsInMeters.y / 8f;

        _sceneTexture = TextureCache.GetCachedTexture("Menus/Vending/FrontPanel") as Texture2D;
		_nextButtonTexture = Prefs.GetGameMode() == Prefs.MODE_ENDLESS ? TextureCache.GetCachedTexture("Menus/Vending/PlayAgain") as Texture2D : TextureCache.GetCachedTexture("Gui/Dialog/Next") as Texture2D;
		_showNextButton = Prefs.ShowNextLevelButtonOnVending;
		Prefs.ShowNextLevelButtonOnVending = false;
		
		_coinBackgroundTexture = TextureCache.GetCachedTexture("Menus/Vending/CoinBackground") as Texture2D;
		
        _cleanupList = new List<GameObject>();
        _racks = new List<VendingMachineRack>();

        _racks.Add(MakeRack(UpgradeManager.GetUpgrade(UpgradeType.StaminaSnack),
            new Vector3(0 - itemSpacing, FirstRackAt * metersHeight, 0)
        ));
        _racks.Add(MakeRack(UpgradeManager.GetUpgrade(UpgradeType.Coins),
            new Vector3(0 + itemSpacing, FirstRackAt * metersHeight, 0)
        ));

        _racks.Add(MakeRack(UpgradeManager.GetUpgrade(UpgradeType.Zippers),
            new Vector3(0 - itemSpacing, SecondRackAt * metersHeight, 0)
        ));
        _racks.Add(MakeRack(UpgradeManager.GetUpgrade(UpgradeType.BonkProofing),
            new Vector3(0 + itemSpacing, SecondRackAt * metersHeight, 0)
        ));
		
        _racks.Add(MakeRack(UpgradeManager.GetUpgrade(UpgradeType.ComboTimeout),
            new Vector3(0 - itemSpacing, ThirdRackAt * metersHeight, 0)
        ));
/*        _racks.Add(MakeRack(UpgradeManager.GetUpgrade(UpgradeType.Pie),
            new Vector3(0 - itemSpacing, ThirdRackAt * metersHeight, 0)
        ));*/

        //_racks.Add(MakeRack(VendingMachineRack.VendType.Booster, new int[] { 1, 5, 10 }, new Vector3(0, 0, 0)));
    }
	

	void Update () {
		//check for clicks on items
        if (Input.GetMouseButton(0) && !DialogSpawner.DialogShowing)
        {
            if (!_mouseDown)
            {
                _mouseDown = true;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                var hits = Physics.RaycastAll(ray);
                var dispenserHits = (from h in hits where h.collider.name.StartsWith("Rack") select h.collider.GetComponent<VendingMachineRack>()).ToList();
                foreach (var d in dispenserHits)
                {
                    if (d.CanDispense)
                    {
                        CurrRack = d;
                        if (Prefs.Money >= d.CurrCost)
                        {
                            DialogSpawner.ShowVendingDialog();
                        }
						//show coin purchasing dialog, even if can't purchase right now
                        else// if (InAppPurchasingManager.CanPurchaseDetermined && InAppPurchasingManager.CanPurchase)
						{
                            DialogSpawner.ShowVendingDialogGetMoreCoins();
						}
                    }
                }
            }
        }
        else
        {
            _mouseDown = false;
        }

	}

    public override void RenderMainLayer()
    {
        if (_goingBack)
            return;

        if (_sceneTexture == null)
            return;
		
		GUI.depth = 0;
        GUI.DrawTexture(new Rect(-1000, 0, Screen.width + 2000, Screen.height), _sceneTexture, ScaleMode.ScaleToFit);
		
		var nextWidth = Screen.height * .22f;
		var padding = Screen.height * .05f;
		var nextHeigt = _nextButtonTexture.height * (nextWidth / _nextButtonTexture.width);
		
		//draw Next button
		if (_showNextButton && GUI.Button(
			new Rect(	Screen.width - nextWidth - padding, 
						Screen.height - nextHeigt - padding,
						nextWidth,
						nextHeigt),
			_nextButtonTexture,
			PHIUtil.MakeButtonGUIStyle()))
		{
			ShowLoading = true;
			MenuManager.PopLast();
			MenuManager.LoadClassicModeFromVending();
		}
		
	}
	
	public override void RenderOverlays()
	{
//		GUI.depth = RenderDepths.DIALOG_OVERLAY;
		var width = Screen.height * .25f;
		var height = Screen.height * .12f;
		var textRect = new Rect(Screen.width - width, 0, width, height);
		var backgroundRect = new Rect(Screen.width - width, height - _coinBackgroundTexture.height, _coinBackgroundTexture.width, _coinBackgroundTexture.height);
		
		GUI.DrawTexture(backgroundRect, _coinBackgroundTexture);
        var moneyLabelStyle = PHIUtil.GetLargeTextStyle();
        moneyLabelStyle.alignment = TextAnchor.MiddleCenter;
        GUI.Label(textRect, string.Format("{0} Coins", Prefs.Money), moneyLabelStyle);
		GUI.depth = 0;
    }

    private VendingMachineRack MakeRack(Upgrade upgrade, Vector3 position)
    {
        var go = new GameObject("Rack" + upgrade.Type.ToString());
        var rb = go.AddComponent<Rigidbody>();
        var bc = go.AddComponent<BoxCollider>();
        bc.transform.localScale = new Vector3(_itemWidth, _itemWidth, 1);
        rb.isKinematic = true;
        rb.useGravity = false;
        go.transform.position = position;

        var rackClass = go.AddComponent<VendingMachineRack>();
        rackClass.Init(upgrade, position, _itemWidth);

        return rackClass;
    }

    public void OnDestroy()
    {
		//Debug.Log ("Destroy vending");
        foreach (var thing in _cleanupList)
        {
            Destroy(thing);
        }

        foreach (var r in _racks)
        {
			if (r != null)
			{
	            var go = r.gameObject;
	            r.Destroy();
				if (go != null)
		            Destroy(go);
			}
        }
		
		base.OnDestroy();
    }
}
