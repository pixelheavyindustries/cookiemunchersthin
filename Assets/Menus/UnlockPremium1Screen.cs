using UnityEngine;
using System;
using System.Collections;

public class UnlockPremium1Screen : MenuScreen
{
	// Use this for initialization
    public void Start()
    {
    }

	public override void RenderMainLayer () {
        var style = PHIUtil.MakeDialogTextStyle();
        GUI.Label(new Rect(ScreenDimensions.GameAreaInPixels.xMin + ScreenDimensions.GameAreaInPixels.width * .15f,
                            ScreenDimensions.GameAreaInPixels.yMin,
                            ScreenDimensions.GameAreaInPixels.width * .7f,
                            ScreenDimensions.GameAreaInPixels.height * .45f),
                            "Get Attack Mode now for only $0.99, and help free captured cookie munchers from evil aliens!", style);

        style.alignment = TextAnchor.LowerCenter;
        style.font = PHIUtil.GetMediumFont();
        GUI.Label(new Rect(ScreenDimensions.GameAreaInPixels.xMin,
                            ScreenDimensions.GameAreaInPixels.yMax * .65f,
                            ScreenDimensions.GameAreaInPixels.width,
                            ScreenDimensions.GameAreaInPixels.height * .2f),
                            "Already bought it?  Click the button to restore it to your device.", style);
    }
}
