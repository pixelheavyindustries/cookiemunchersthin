using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MenuProps {
    private static List<String> _screenStack;
    public static List<String> ScreenStack
    {
        get
        {
            if (_screenStack == null)
            {
//                Debug.Log("instantiate new screenstack");
                _screenStack = new List<string>();
            }

            return _screenStack;
        }

        private set { _screenStack = value; }
    }
	
	public static void ResetScreenStack()
	{
		_screenStack = new List<string>();
	}

}
