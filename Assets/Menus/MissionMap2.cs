using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DifficultyLevel = ClimberMain.DifficultyLevel;


public class MissionMap2 : MonoBehaviour {

    public MenuManager MenuManager;

    protected List<GameObject> _cleanupList;
    protected DifficultyLevel _difficulty;
    protected float _iconSize;
    protected Texture2D _lockedIconTexture;
    protected Texture2D[] _unlockedIconTextures;
    protected Texture2D _new;

    protected List<Vector3> _missionPositions;
    protected List<Vector2> _dotPositions;
    protected List<int> _starScores;
    protected Vector2 _endlessModePos;

    protected int _lowestMission;
    protected int _highestMissionPossible;
    protected int _highestUnlockedMission;
    protected int _currMission;
    protected int _numPages = 3;
    protected int _currPage = 0;
    protected int _missionsPerPage;

    protected GUIStyle _buttonStyle;
    protected Texture2D _downButtonTexture;
    protected Rect _downButtonRect;
    protected Texture2D _upButtonTexture;
    protected Rect _upButtonRect;

    protected GameObject _gameClockHolder;
    protected GameObject _cameraPositionMarker;
    protected Tweenk _cameraTweenk;

    protected bool _goingBack = false;
    protected bool _initialized = false;
    protected bool _loadingMission = false;

	// Use this for initialization
	void Start () {
        _gameClockHolder = new GameObject("GameClockHolder");
        _gameClockHolder.AddComponent<GameClock>();
        GameClock.Paused = false;

        //setup the camera
        Camera.main.fieldOfView = ScreenDimensions.CameraFov;

        //we can't add a tweenk to the camera, so we make a marker, add tweenk to it, and set camera pos to same position as marker
        _cameraPositionMarker = new GameObject("CameraPosMarker");
        _cameraTweenk = _cameraPositionMarker.AddComponent<Tweenk>();

        _difficulty = (DifficultyLevel)Prefs.CurrentDifficulty;
        _cleanupList = new List<GameObject>();
    }

    private void EnsureInitialized()
    {
        if (_initialized)
            return;

        _initialized = true;
        //load all the textures
        _iconSize = Screen.height / 8f;
        _lockedIconTexture = TextureCache.GetCachedTexture("Menus/MissionMapIconLocked") as Texture2D;
        _unlockedIconTextures = new Texture2D[4];
        _unlockedIconTextures[0] = TextureCache.GetCachedTexture("Menus/MissionMapIcon0") as Texture2D;
        _unlockedIconTextures[1] = TextureCache.GetCachedTexture("Menus/MissionMapIcon1") as Texture2D;
        _unlockedIconTextures[2] = TextureCache.GetCachedTexture("Menus/MissionMapIcon2") as Texture2D;
        _unlockedIconTextures[3] = TextureCache.GetCachedTexture("Menus/MissionMapIcon3") as Texture2D;
        _new = TextureCache.GetCachedTexture("Menus/New") as Texture2D;

        _downButtonTexture = TextureCache.GetCachedTexture("Menus/DownArrow") as Texture2D;
        _upButtonTexture = TextureCache.GetCachedTexture("Menus/UpArrow") as Texture2D;
        _upButtonRect = new Rect(ScreenDimensions.GameAreaInPixels.x + (ScreenDimensions.GameAreaInPixels.width - _downButtonTexture.width) / 2,
                                    ScreenDimensions.GameAreaInPixels.y,
                                    _downButtonTexture.width, _downButtonTexture.height);
        _downButtonRect = new Rect(ScreenDimensions.GameAreaInPixels.x + (ScreenDimensions.GameAreaInPixels.width - _downButtonTexture.width) / 2,
                                    ScreenDimensions.GameAreaInPixels.yMax - _downButtonTexture.height,
                                    _downButtonTexture.width, _downButtonTexture.height);

        //get all the numbers from Prefs
        _lowestMission = 1;
        _highestMissionPossible = Prefs.GetHighestMissionLevelPossible(_difficulty);
        _highestUnlockedMission = Prefs.GetHighestMissionLevelUnlocked(_difficulty);
        _currMission = Prefs.GetMissionLevel(_difficulty);
        _missionsPerPage = _highestMissionPossible / _numPages;
        _currPage = (int)((float)(_currMission-1) / (float)_missionsPerPage);

        _cameraPositionMarker.transform.position = new Vector3(0, _currPage * ScreenDimensions.GameAreaInMeters.height, 0 - ScreenDimensions.CameraDistance);
        Camera.main.transform.position = _cameraPositionMarker.transform.position;

        _missionPositions = CreateMissionPositions(_difficulty);
        _starScores = GetStarScores(_difficulty);
        _endlessModePos = TranslateIntoWorldCoords(.3f, 1.85f);

        CreateEarth();
        CreateMoon();

        _buttonStyle = PHIUtil.MakeButtonGUIStyle();
        //print("HighestUnlocked: " + _highestUnlockedMission + "    " + _highestMissionPossible + "    " + _currMission + "     " + _currPage);
    }

    // Update is called once per frame
    void Update()
    {
        EnsureInitialized();
	}

    public void OnGUI()
    {
        if (!_initialized || _goingBack)
            return;


        if (MenuSpawner.DrawBackButton())
        {
            _goingBack = true;
            MenuManager.Back();
            return;
        }

        if (_currPage > 0)
        {
            if (GUI.Button(_downButtonRect, _downButtonTexture, _buttonStyle))
            {
                _currPage--;
                _cameraTweenk.StopAll();
                _cameraTweenk.Y(_currPage * ScreenDimensions.GameAreaInMeters.height, 1.5f, Tweenk.Easing.EaseOutBounce);
            }
        }

        if (_currPage < _numPages - 1)
        {
            if (GUI.Button(_upButtonRect, _upButtonTexture, _buttonStyle))
            {
                _currPage++;
                _cameraTweenk.StopAll();
                _cameraTweenk.Y(_currPage * ScreenDimensions.GameAreaInMeters.height, 1.5f, Tweenk.Easing.EaseOutBounce);
            }
        }

        Camera.main.transform.position = _cameraPositionMarker.transform.position;
        var selectedMission = DrawMissionIcons();
        if (selectedMission > 0)
        {
            LoadFiniteMission(selectedMission);
        }

        if (_loadingMission)
        {
            GUI.depth = RenderDepths.DIALOG_OVERLAY;
            GUI.DrawTexture(ScreenDimensions.GameAreaInPixels, TextureCache.GetCachedTexture("Menus/Loading"), ScaleMode.ScaleToFit);
            GUI.depth = 0;
        }

    }

    private void LoadFiniteMission(int selectedMission)
    {
        Prefs.SetMissionLevel(_difficulty, selectedMission);
        _loadingMission = true;
        _cameraTweenk.Callback(0.2f, LoadClimberMain);
    }

    private void LoadEndlessMission()
    {
        _loadingMission = true;
        _cameraTweenk.Callback(0.2f, LoadClimberMain);
    }

    private bool LoadClimberMain()
    {
        Application.LoadLevel("ClimberMain");
        return true;
    }

    protected void CreateEarth()
    {
        var earthObj = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended, "Menus/MissionMapEarth", 50, true, 1, 1);
        earthObj.transform.position = new Vector3(0, ScreenDimensions.GameAreaInMeters.yMin, 1);
        _cleanupList.Add(earthObj);
    }

    protected void CreateMoon()
    {
        var moonObj = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended, "Menus/MissionMapMoon", 60, true, 1, 1);
        moonObj.transform.position = new Vector3(0, ScreenDimensions.GameAreaInMeters.yMax + ScreenDimensions.GameAreaInMeters.height * 1.8f, 1);
        _cleanupList.Add(moonObj);
    }

    protected int DrawMissionIcons()
    {
        var selectedMission = 0;

        for (int i = 0; i < _missionPositions.Count; i++)
        {
            var pos = TranslateIntoScreenCoords(_missionPositions[i].x, _missionPositions[i].y);
            //screen coords are upside down, which is dumb:
            pos.y = Screen.height - pos.y;
            if (DrawMissionIcon((i + 1).ToString(), pos, i >= _highestUnlockedMission, _starScores[i]))
                selectedMission = i+1;
        }



        return selectedMission;
    }

    protected bool DrawMissionIcon(string label, Vector3 pos, bool locked, int numStars)
    {
        var btnStyle = PHIUtil.MakeButtonGUIStyle();
        var tex = locked ? _lockedIconTexture : _unlockedIconTextures[numStars];
        var bgRect = new Rect(pos.x - _iconSize / 2f, pos.y - _iconSize / 2f, _iconSize, _iconSize);
        if (GUI.Button(bgRect, tex, btnStyle))
        {
            if (!locked)
            {
                return true;
            }
        }

        if (!locked)
        {
            var style = PHIUtil.GetMediumTextStyle();
            style.alignment = TextAnchor.LowerCenter;
            var labelRect = new Rect(pos.x - _iconSize / 2, pos.y - _iconSize / 2, _iconSize, _iconSize * .6f);
            GUI.Label(labelRect, label, style);

            var starsRect = new Rect(pos.x + _iconSize / 4f, pos.y - _iconSize / 2f, _iconSize, _iconSize);
            if (numStars == 0)
                GUI.DrawTexture(starsRect, _new, ScaleMode.ScaleToFit);
        }

        return false;
    }


    protected List<Vector3> CreateMissionPositions(ClimberMain.DifficultyLevel diff)
    {
        var positions = new List<Vector3>();
        switch (diff)
        {
            default:
                positions.Add(TranslateIntoWorldCoords(0.50f, 0.20f));
                positions.Add(TranslateIntoWorldCoords(0.65f, 0.35f));
                positions.Add(TranslateIntoWorldCoords(0.50f, 0.50f));
                positions.Add(TranslateIntoWorldCoords(0.30f, 0.65f));
                positions.Add(TranslateIntoWorldCoords(0.32f, 0.80f));

                positions.Add(TranslateIntoWorldCoords(0.37f, 1.28f));
                positions.Add(TranslateIntoWorldCoords(0.43f, 1.43f));
                positions.Add(TranslateIntoWorldCoords(0.68f, 1.53f));
                positions.Add(TranslateIntoWorldCoords(0.78f, 1.70f));
                positions.Add(TranslateIntoWorldCoords(0.80f, 1.85f));

                positions.Add(TranslateIntoWorldCoords(0.71f, 2.20f));
                positions.Add(TranslateIntoWorldCoords(0.75f, 2.35f));
                positions.Add(TranslateIntoWorldCoords(0.62f, 2.5f));
                positions.Add(TranslateIntoWorldCoords(0.47f, 2.62f));
                positions.Add(TranslateIntoWorldCoords(0.40f, 2.75f));
                break;
        }

        return positions;
    }

    protected List<int> GetStarScores(DifficultyLevel diff)
    {
        var scores = new List<int>();
        for (var i = 1; i <= _highestMissionPossible; i++)
        {
            scores.Add(Prefs.GetStarScore(diff, i));
        }

        return scores;
    }

    private Vector2 TranslateIntoWorldCoords(float x, float y)
    {
        return new Vector2(x * ScreenDimensions.GameAreaInMeters.width - ScreenDimensions.GameAreaInMeters.width/2,
            y * ScreenDimensions.GameAreaInMeters.height - ScreenDimensions.GameAreaInMeters.height/2);
    }

    private Vector3 TranslateIntoScreenCoords(float x, float y)
    {
        return Camera.main.WorldToScreenPoint(new Vector3(x, y, 0));
    }

    public void Destroy()
    {
        _goingBack = true;

        foreach (var thing in _cleanupList)
        {
            Destroy(thing);
        }

        Destroy(_gameClockHolder);
        Destroy(_cameraPositionMarker);
    }


    /*
    protected List<Vector2> CacheDotPositions(List<BezierDef> curves)
    {
        var positions = new List<Vector2>();
        var steps = 60f;
        var minDistSquared = (Screen.height / 20f) * (Screen.height / 20f);
        var lastPoint = new Vector2(-1000, -1000);
        foreach (var curve in curves)
        {
            for (var t = 0f; t <= 1f; t += 1f / steps)
            {
                var b = 1f - t;
                var point = curve.start * (b * b * b) + 3 * curve.cp1 * (b * b) * t + 3 * curve.cp2 * b * t * t + curve.end * t * t * t;
                var screenPoint = new Vector2(point.x * gameAreaDims.x, point.y * gameAreaDims.y);
                print("Screenpoint " + screenPoint);
                if ((screenPoint - lastPoint).sqrMagnitude >= minDistSquared)
                {
                    positions.Add(screenPoint);
                    lastPoint = screenPoint;
                    print("Added " + screenPoint);
                }
            }
        }

        return positions;
    }


    protected void DrawCurve(Vector2 start, Vector2 end, Vector2 control1, Vector2 control2)
    {
    }

    protected List<BezierDef> DefineCurves(DifficultyLevel diff)
    {
        var curves = new List<BezierDef>();
        switch (diff)
        {
            default:
                curves.Add(new BezierDef(.5f, .1f, .6f, .8f, .7f, .8f, .6f, 1f));
                curves.Add(new BezierDef(.6f, 1f, .3f, 1.3f, .3f, 1.3f, .2f, 1.5f));
                break;
        }

        return curves;
    }*/
}
