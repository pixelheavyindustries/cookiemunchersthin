using UnityEngine;
using System.Collections;

public class TweakingPanel : MenuPanel {

    protected ClimberMain.DifficultyLevel _difficulty;

	// Use this for initialization
    public void Start()
    {
	
	}
	
	// Update is called once per frame
	public void Update () {
	
	}

    public void OnGUI()
    {

        if (GUI.Button(new Rect(200, 25, 60, 30), "EASY"))
            _difficulty = ClimberMain.DifficultyLevel.Easy;

        if (GUI.Button(new Rect(275, 25, 60, 30), "HARD"))
            _difficulty = ClimberMain.DifficultyLevel.Hard;

        if (GUI.Button(new Rect(350, 25, 60, 30), "EXPERT"))
            _difficulty = ClimberMain.DifficultyLevel.Expert;

        if (GUI.Button(new Rect(125, 150, 100, 30), "RESET!"))
            DefaultSetter.SetDefaults();

        if (GUI.Button(new Rect(125, 200, 200, 30), "Unlock All"))
        {
            Prefs.HighestDifficultyUnlocked = (int)ClimberMain.DifficultyLevel.Expert;
            Prefs.SetHighestMissionLevelUnlocked(ClimberMain.DifficultyLevel.Easy, Prefs.GetHighestMissionLevelPossible(ClimberMain.DifficultyLevel.Easy));
            Prefs.SetHighestMissionLevelUnlocked(ClimberMain.DifficultyLevel.Hard, Prefs.GetHighestMissionLevelPossible(ClimberMain.DifficultyLevel.Hard));
            Prefs.SetHighestMissionLevelUnlocked(ClimberMain.DifficultyLevel.Expert, Prefs.GetHighestMissionLevelPossible(ClimberMain.DifficultyLevel.Expert));
        }

        if (GUI.Button(new Rect(125, 250, 100, 30), "Free $$$"))
            Prefs.Money += 20;

        /*        GUI.Label(new Rect(25, 125, 400, 50), "Settings for " + _difficulty.ToString());

                var y = 150;

                GUI.Label(new Rect(25, y, 100, 50), "Stamina Burn");
                Prefs.SetStaminaBurn(_difficulty, true, GUI.HorizontalSlider(new Rect(125, y + 10, 200, 30), 
                    Prefs.GetStaminaBurn(_difficulty, 1), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetStaminaBurn(_difficulty, 1), 2).ToString());
                Prefs.SetStaminaBurn(_difficulty, false, GUI.HorizontalSlider(new Rect(125, y + 30, 200, 30),
                    Prefs.GetStaminaBurn(_difficulty, 0), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y+20, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetStaminaBurn(_difficulty, 0), 2).ToString());

                y += 50;
                GUI.Label(new Rect(25, y, 100, 50), "Stamina Fill");
                Prefs.SetStaminaFill(_difficulty, true, GUI.HorizontalSlider(new Rect(125, y + 10, 200, 30),
                    Prefs.GetStaminaFill(_difficulty, 1), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetStaminaFill(_difficulty, 1), 2).ToString());
                Prefs.SetStaminaFill(_difficulty, false, GUI.HorizontalSlider(new Rect(125, y + 30, 200, 30),
                    Prefs.GetStaminaFill(_difficulty, 0), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y + 20, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetStaminaFill(_difficulty, 0), 2).ToString());

                y += 50;
                GUI.Label(new Rect(25, y, 100, 50), "Good Cookies");
                Prefs.SetGoodCookies(_difficulty, true, GUI.HorizontalSlider(new Rect(125, y + 10, 200, 30),
                    Prefs.GetGoodCookies(_difficulty, 1), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetGoodCookies(_difficulty, 1), 2).ToString());
                Prefs.SetGoodCookies(_difficulty, false, GUI.HorizontalSlider(new Rect(125, y + 30, 200, 30),
                    Prefs.GetGoodCookies(_difficulty, 0), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y + 20, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetGoodCookies(_difficulty, 0), 2).ToString());

                y += 50;
                GUI.Label(new Rect(25, y, 100, 50), "Bonus Occurence");
                Prefs.SetBonusOccurence(_difficulty, true, GUI.HorizontalSlider(new Rect(125, y + 10, 200, 30),
                    Prefs.GetBonusOccurence(_difficulty, 1), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetBonusOccurence(_difficulty, 1), 2).ToString());
                Prefs.SetBonusOccurence(_difficulty, false, GUI.HorizontalSlider(new Rect(125, y + 30, 200, 30),
                    Prefs.GetBonusOccurence(_difficulty, 0), 0.5f, 20.0f));
                GUI.Label(new Rect(325, y + 20, 100, 30), PHIUtil.RoundToPlaces(Prefs.GetBonusOccurence(_difficulty, 0), 2).ToString());
         * */
    }


}
