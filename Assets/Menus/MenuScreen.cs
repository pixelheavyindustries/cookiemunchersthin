using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MenuScreen : MonoBehaviour{

    public MenuManager MenuManager;
    public bool Showing = false;
	
	public Texture2D Background;
	
	public bool ShowBackButton = true;
	
	public bool LockEverything;
	public bool ShowLoading;
	private Texture2D _loadingTexture;

//    public Texture2D Background;

//    public bool ShowBack = true;
//    public Action<string> Callback;
    protected List<DrawableMenuElement> _elements;

/*    private static Vector3 _frontPositionOffset = new Vector3(15f, 0f, 2f);
    private static Vector3 _behindPositionOffset = new Vector3(-15f, 0f, -2f);
    private static Quaternion _frontRotationOffset = Quaternion.LookRotation(Vector3.right);
    private static Quaternion _backRotationOffset = Quaternion.LookRotation(Vector3.left);*/
    //    private static Quaternion _behindPositionOffset = new Vector3(10f, 0f, 2f);

    /*public enum AnimationDirection
    {
        FromBehind,
        FromFront,
    }*/

    public MenuScreen()
    {
        _elements = new List<DrawableMenuElement>();
		_loadingTexture = TextureCache.GetCachedTexture("Menus/Loading");
    }

	void Start () {
	}
	
	public void OnGUI()
	{
		if (!Showing)
			return;
		
		//render backgrounds
		if (Background != null)
	        GUI.DrawTexture(new Rect(-1000, 0, Screen.width + 2000, Screen.height), Background, ScaleMode.ScaleToFit);
	
		RenderMainLayer();
		
		//render dialogs
		DialogSpawner.RenderCurrentDialog();
		
		//render backbutton
		if (ShowBackButton && MenuSpawner.DrawBackButton())
			MenuManager.Back();
		
		//render overlays
		RenderOverlays();
		
		//render loading
		if (ShowLoading)
            GUI.DrawTexture(ScreenDimensions.GameAreaInPixels, _loadingTexture, ScaleMode.ScaleToFit);
	}
	
	public virtual void RenderMainLayer()
	{
		DrawElements();
	}
	
	public void DrawElements()
	{
		foreach (var el in _elements)
		{
			el.Draw();
		}
	}
	
	public virtual void RenderOverlays()
	{
	}
	
    public void AddElement(DrawableMenuElement dme)
    {
        if (_elements == null)
            _elements = new List<DrawableMenuElement>();

        _elements.Add(dme);
    }


    public void Show()
    {
        Showing = true;
    }

    public virtual void OnDestroy()
    {
/*        foreach (var el in _elements)
        {
            //on game end, these may have already been destroyed
            if (el != null && el.gameObject != null)
                Destroy(el.gameObject);
        }*/
    }
}
