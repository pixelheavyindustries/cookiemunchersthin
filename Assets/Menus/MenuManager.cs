using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{
    protected float _cameraFov = 60.0f;
    protected float _cameraZ = -50.0f;

    private GameObject _missionMapHolder;
    private GameObject _upgradesHolder;
    protected MenuScreen _currScreen;
    protected delegate MenuScreen SpawnScreen();
    protected Dictionary<string, SpawnScreen> _screenSpawns;
	public const string HOME = "Home";
    public const string MODE = "Mode";
    public const string DIFFICULTY = "Difficulty";
    public const string PURCHASE = "Purchase";
    public const string SCORES = "Scores";
    public const string TWEAKS = "Tweaks";
    public const string MISSIONS = "Missions";
    public const string UPGRADES = "Upgrades";
    public const string INFO = "Info";
    public const string MARKETLINKS = "MarketLinks";
    public const string INTROCOMIC = "IntroComic";
    public const string OUTRO_EASY = "OutroEasy";
    public const string OUTRO_HARD = "OutroHard";
    public const string OUTRO_EXPERT = "OutroExpert";
    public const float TransitionTime = 0f;//0.7f;
    protected float _backButtonExpiry = 0f;
//    private static List<String> _screenStack;

    private bool _mouseIsDown = false;
	
	private bool _loadingMission = false;
    protected GameObject _cameraPositionMarker;
    protected Tweenk _cameraTweenk;

	// Use this for initialization
	public void Start ()
	{
		//Debug.Log("MM.Start: " + String.Join(", ", MenuProps.ScreenStack.ToArray()));
        gameObject.AddComponent<GameClock>();
        DefaultSetter.SetConstantParams();
        if (!Prefs.HaveSetDefaults || Prefs.Version < DefaultSetter.MINIMUM_VERSION)
        {
//            Debug.Log("Setting defaults: " + Prefs.HaveSetDefaults + "    " + Prefs.Version);
            DefaultSetter.SetDefaults();
        }
        Prefs.HaveSetDefaults = true;

	    Camera.main.transform.position = new Vector3(0, 0, _cameraZ);
	    Camera.main.fieldOfView = _cameraFov;

		_screenSpawns = new Dictionary<string, SpawnScreen>();
        _screenSpawns.Add(HOME, MakeHomeScreen);
        _screenSpawns.Add(MODE, MakeModesScreen);
        _screenSpawns.Add(DIFFICULTY, MakeDifficultyScreen);
        _screenSpawns.Add(TWEAKS, TweakingScreen);
        _screenSpawns.Add(INFO, MakeInfoScreen);
        _screenSpawns.Add(PURCHASE, MakeUnlockPremium1Screen);
        _screenSpawns.Add(INTROCOMIC, MakeIntroComicScreen);
		_screenSpawns.Add (UPGRADES, MakeUpgradesScreen);
		_screenSpawns.Add (OUTRO_EASY, MakeOutroEasy);
		_screenSpawns.Add (OUTRO_HARD, MakeOutroHard);
		_screenSpawns.Add (OUTRO_EXPERT, MakeOutroExpert);
		
        //we can't add a tweenk to the camera, so we make a marker, add tweenk to it, and set camera pos to same position as marker
        _cameraPositionMarker = new GameObject("CameraPosMarker");
        _cameraTweenk = _cameraPositionMarker.AddComponent<Tweenk>();
		
		if (MenuProps.ScreenStack.Count == 0)
	        MenuProps.ScreenStack.Add(MenuManager.HOME);
		
        ShowImmediate(MenuProps.ScreenStack.Last());
    }
	
	// Update is called once per frame
	public void Update ()
	{
	    CheckForClicks();
	}

    public void OnGUI()
    {
        if (_loadingMission)
        {
            GUI.depth = RenderDepths.DIALOG_OVERLAY;
            GUI.DrawTexture(ScreenDimensions.GameAreaInPixels, TextureCache.GetCachedTexture("Menus/Loading"), ScaleMode.ScaleToFit);
            GUI.depth = 0;
        }
		
        if (Input.GetKeyDown(KeyCode.Escape) && Time.time > _backButtonExpiry)
        {
            _backButtonExpiry = Time.time + .5f;
            Back();
        }
    }

    private void CheckForClicks()
    {
        if (!_mouseIsDown && Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            var hits = Physics.RaycastAll(ray);
		
			IEnumerable<RaycastHit> fake = Enumerable.Empty<RaycastHit>();
			fake.GetEnumerator();
			
			RaycastHit? closestHit = null;
			float closestDist = 10000000000;
			foreach (RaycastHit hit in hits)
			{
				if (hit.collider.gameObject.GetComponent<MenuElement>() != null)
				{
					var sqrDist = (hit.collider.gameObject.transform.position - Camera.main.transform.position).sqrMagnitude;
					if (closestHit.HasValue || sqrDist < closestDist)
					{
						closestDist = sqrDist;
						closestHit = hit;
					}
				}
			}
			
            if (closestHit != null)
            {
                closestHit.Value.collider.gameObject.GetComponent<MenuElement>().HandleClick();
            }
        }

        _mouseIsDown = Input.GetMouseButton(0);
    }

    public void ShowImmediate(string id)
    {
        //Debug.Log("ShowImmediate: " + id);
        Camera.main.transform.position = new Vector3(0, 0, _cameraZ);
        Camera.main.fieldOfView = _cameraFov;

        if (id == MISSIONS)
        {
            LoadMissions();
        }
        else
        {
            var screenSpawn = _screenSpawns[id];
            _currScreen = screenSpawn();
            _currScreen.Show();
        }
    }
	
	public static void PopLast()
	{
        if (MenuProps.ScreenStack.Count() == 0)
            return;
		
        MenuProps.ScreenStack.RemoveAt(MenuProps.ScreenStack.Count() - 1);
	}
	
	public static void PushScreen(string screenId)
	{
		MenuProps.ScreenStack.Add (screenId);
	}
	
	public static void ResetScreensToModes()
	{
		MenuProps.ResetScreenStack();
		MenuProps.ScreenStack.Add (HOME);
		MenuProps.ScreenStack.Add (MODE);
	}
			

    public void ForwardTo(string id)
    {
        Camera.main.transform.position = new Vector3(0, 0, _cameraZ);
		DialogSpawner.CloseAll();

        if (!_screenSpawns.ContainsKey(id) && id != MISSIONS && id != UPGRADES)
		{
            //we got passed an id we don't have anything for
			return;
		}

        if (MenuProps.ScreenStack.Count() > 0)
        {
            DestroyMissionsOrUpgrades();
            if (_currScreen != null)
            {
                Destroy(_currScreen.gameObject, TransitionTime);
            }
        }

        MenuProps.ScreenStack.Add(id);
        ShowCurrScreen();
    }
	
	public void Back()
	{
        //Debug.Log("Back");
        if (MenuProps.ScreenStack.Count() <= 1)
            return;
		
		DialogSpawner.CloseAll();
		
        DestroyMissionsOrUpgrades();
        if (_currScreen != null)
        {
            Destroy(_currScreen.gameObject, TransitionTime);
        }
        MenuProps.ScreenStack.RemoveAt(MenuProps.ScreenStack.Count() - 1);

        Camera.main.transform.position = new Vector3(0, 0, _cameraZ);
        ShowCurrScreen();

        //clean up any dialogs we might have hanging open
        DialogSpawner.CloseAll();
    }

    private void ShowCurrScreen()
    {
        var screenId = MenuProps.ScreenStack.Last();
        if (screenId == MISSIONS)
        {
            LoadMissions();
        }
        else
        {
            var screenSpawn = _screenSpawns[screenId];
            _currScreen = screenSpawn();
            _currScreen.Show();
            Camera.main.transform.position = new Vector3(0, 0, _cameraZ);
            Camera.main.fieldOfView = _cameraFov;
        }
    }

    private void DestroyMissionsOrUpgrades()
    {
        if (MenuProps.ScreenStack.Last() == MISSIONS)
        {
            var mm = _missionMapHolder.GetComponent<MissionMap2>() as MissionMap2;
            mm.Destroy();
            Destroy(_missionMapHolder);
        }
        else if (MenuProps.ScreenStack.Last() == UPGRADES)
        {
/*            var vm = _upgradesHolder.GetComponent<VendingMachine>() as VendingMachine;
            vm.Destroy();
            Destroy(_upgradesHolder);*/
        }
    }

    public void Refresh()
    {
        Destroy(_currScreen.gameObject, TransitionTime);
        var screenSpawn = _screenSpawns[MenuProps.ScreenStack.Last()];
        _currScreen = screenSpawn();
        _currScreen.Show();
    }
	
    private bool LoadClimberMain()
    {
		DialogSpawner.CloseAll();
        Application.LoadLevel("ClimberMain");
        return true;
    }
	
    protected void ForwardToMissions(ClimberMain.DifficultyLevel diff)
    {
        //print("Loading " + diff + "  Highest: " + Prefs.HighestDifficultyUnlocked);
        if ((int)diff > Prefs.HighestDifficultyUnlocked)
            return;

        Prefs.CurrentDifficulty = (int)diff;
		print("Loading " + Prefs.GetGameMode());
        switch (Prefs.GetGameMode())
        {
            case Prefs.MODE_BONUSROUND:
            case Prefs.MODE_ENDLESS:
            case Prefs.MODE_TRAINING:
				_loadingMission = true;
				_cameraTweenk.Callback(0.2f, LoadClimberMain);
                break;
            case Prefs.MODE_CLASSIC:
                ForwardTo(MISSIONS);
                break;
            case Prefs.MODE_ATTACK:
                //we can't let them get to the purchasing screen if IAP's aren't supported on this device
/*                if (BreakerPanel.PremiumPack1Enabled &&
                    (InAppPurchasingManager.CanPurchase || Prefs.Premium1Unlocked))
                {
                    if (Prefs.AttackModeUnlocked)
                        Application.LoadLevel("ClimberMain");
                    else
                        ForwardTo(MenuManager.ACTIVATEENDLESS);
                }*/
                break;
        }
    }

    protected void LoadMissions()
    {
        PlatformSpecifics.ShowAd();

        _missionMapHolder = new GameObject("MissionMapHolder");
        var mm = _missionMapHolder.AddComponent<MissionMap2>() as MissionMap2;
        mm.MenuManager = this;
    }

/*    protected void LoadUpgrades()
    {
        PlatformSpecifics.ShowAd();

        _upgradesHolder = new GameObject("UpgradesHolder");
        var vm = _upgradesHolder.AddComponent<VendingMachine>() as VendingMachine;
        vm.MenuManager = this;
    }*/
	
    #region Menu Spawns

//RESTORE IF MUSIC    private static bool _havePlayedMenuMusic = false;
    public MenuScreen MakeHomeScreen()
    {
        PlatformSpecifics.HideAd();

/*RESTORE IF MUSIC        if (!_havePlayedMenuMusic)
        {
            CookieSoundManager.PlayMenuMusic();
            _havePlayedMenuMusic = true;
        }*/
		
        var go = new GameObject("HomeScreen");
        var screen = go.AddComponent<HomeScreen>();
        screen.ShowBackButton = false;
		screen.MenuManager = this;
		
        screen.AddElement(DrawableMenuElement.FromRelativePosition("Home.Start", "Menus/ButtonStart", new Vector2(0.5f, 0.74f), .13f, delegate(String id)
        {
            if (!Prefs.HaveShownIntroComic)
            {
                Prefs.HaveShownIntroComic = true;
                ForwardTo(INTROCOMIC);
            }
            else
			{
                ForwardTo(MODE);
			}
        }));

        screen.AddElement(DrawableMenuElement.FromRelativePosition("Home.Story", "Menus/ButtonStory", new Vector2(0.5f, 0.9f), .13f, delegate(String id)
        {
            ForwardTo(INTROCOMIC);
        }));

        screen.AddElement(DrawableMenuElement.FromRelativePosition("Home.Facebook", "Menus/f_logo", new Vector2(0.1f, 0.9f), .09f, delegate(String id)
        {
			Application.OpenURL("http://www.facebook.com/CookieMunchers");
		}));

		return screen;
    }

    public MenuScreen MakeIntroComicScreen()
    {
        PlatformSpecifics.HideAd();

        var go = new GameObject("IntroComicScreen");
        var screen = go.AddComponent<IntroComicScreen>();
		screen.Background = TextureCache.GetCachedTexture("Menus/Comics/IntroComic") as Texture2D;
        screen.MenuManager = this;
        return screen;
    }


    public MenuScreen MakeModesScreen()
    {
        PlatformSpecifics.ShowAd();

        var go = new GameObject("ModesScreen");
        var screen = go.AddComponent<MenuScreen>();
		screen.MenuManager = this;
		
//        screen.AddElement(MenuSpawner.Button("Menus/ButtonTrainingMode",
/*        screen.AddElement(DrawableMenuElement.FromRelativePosition("Modes.Training", "Menus/ButtonTrainingMode",
                                                    new Vector2(0.6f, 0.20f), .2f,
                                                    delegate(String id) {
                                                        if (DialogSpawner.DialogShowing) return;

                                                        Prefs.SetGameMode(Prefs.MODE_TRAINING);
                                                        ForwardToMissions(0);
                                                    }));*/
		
//        screen.AddElement(MenuSpawner.Button("Menus/ButtonClassicMode",
        var classicButton = DrawableMenuElement.FromRelativePosition("Modes.Classic", "Menus/ButtonClassicMode",
                                                    new Vector2(0.3f, 0.29f), .2f,
                                                    delegate(String id) {
                                                        if (DialogSpawner.DialogShowing) return;

                                                        Prefs.SetGameMode(Prefs.MODE_CLASSIC);
                                                        ForwardTo(DIFFICULTY);
                                                    });
//		classicButton.Locked = !Prefs.GetTrainingComplete();
		screen.AddElement(classicButton);

        var isEditor = false;
#if UNITY_EDITOR
        isEditor = true;
#endif

        if (BreakerPanel.EndlessModeEnabled )//&&
//            (Prefs.PremiumPackUnlocked || InAppPurchasingManager.CanPurchase || isEditor))
        {
//            screen.AddElement(MenuSpawner.Button("Menus/ButtonEndlessMode",
            var endless = DrawableMenuElement.FromRelativePosition("Modes.Endless", "Menus/ButtonEndlessMode",
                                                    new Vector2(0.7f, 0.53f), .2f,
                                                    delegate(String id) {
                                                        if (DialogSpawner.DialogShowing) return;
                                                        //if (!GoToPurchasingScreen(Prefs.PremiumPackUnlocked, InAppPurchasingManager.PREMIUM_PACK))
                                                        //{
                                                            Prefs.SetGameMode(Prefs.MODE_ENDLESS);
                                                            ForwardTo(DIFFICULTY);
                                                        //}
                                                    }, !Prefs.GetTrainingComplete());
			screen.AddElement(endless);
        }

        //screen.AddElement(MenuSpawner.Button("Menus/UpgradesButton",
        screen.AddElement(DrawableMenuElement.FromRelativePosition("Modes.Upgrades", "Menus/UpgradesButton",
                                                new Vector2(0.35f, 0.85f), .17f,
                                                delegate(String id) {
                                                    if (DialogSpawner.DialogShowing) return;

                                                    ForwardTo(UPGRADES);
                                                }));
        //screen.AddElement(MenuSpawner.Button("Menus/SettingsButton",
        screen.AddElement(DrawableMenuElement.FromRelativePosition("Modes.Settings", "Menus/SettingsButton",
                                                new Vector2(0.65f, 0.85f), .17f,
                                                delegate(String id) {
                                                    if (DialogSpawner.DialogShowing) return;

                                                    ForwardTo(INFO);
                                                }));
		
        return screen;
    }
	
	public void LoadClassicModeFromVending()
	{
		DialogSpawner.CloseAll();
        Application.LoadLevel("ClimberMain");
	}

    private bool GoToPurchasingScreen(bool alreadyHave, String productId)
    {
        if (alreadyHave)
            return false;

        ForwardTo(PURCHASE);
        return true;
    }

    public MenuScreen MakeDifficultyScreen()
    {
        PlatformSpecifics.ShowAd();

        var go = new GameObject("DifficultyScreen");
        var screen = go.AddComponent<MenuScreen>();
		screen.MenuManager = this;
		
        screen.AddElement(DrawableMenuElement.FromRelativePosition("Difficulty.Choose", "Menus/ChooseDifficulty",
                                                    new Vector2(0.5f, 0.15f), .17f,
                                                    delegate(String id) {}));
		
        screen.AddElement(DrawableMenuElement.FromRelativePosition("Difficulty.Easy", "Menus/ButtonEasy",
                                                    new Vector2(0.5f, 0.4f), .14f,
                                                    delegate(String id) { if (DialogSpawner.DialogShowing) return; ForwardToMissions(ClimberMain.DifficultyLevel.Easy); }));

        var easyAndExpertLocked = !Prefs.GetTrainingComplete();
		screen.AddElement(DrawableMenuElement.FromRelativePosition("Difficulty.Hard", "Menus/ButtonHard",
                                                    new Vector2(0.5f, 0.6f), .14f,
                                                    delegate(String id) { if (DialogSpawner.DialogShowing) return; ForwardToMissions(ClimberMain.DifficultyLevel.Hard); },
													easyAndExpertLocked));
		
		
		screen.AddElement(DrawableMenuElement.FromRelativePosition("Difficulty.Expert", "Menus/ButtonExpert",
                                                    new Vector2(0.5f, 0.8f), .14f,
                                                    delegate(String id) { if (DialogSpawner.DialogShowing) return; ForwardToMissions(ClimberMain.DifficultyLevel.Expert); },
													easyAndExpertLocked));
        return screen;
    }

    public MenuScreen TweakingScreen()
    {
        var go = new GameObject("DifficultyScreen");
        var screen = go.AddComponent<MenuScreen>();
        screen.MenuManager = this;

        //var panelGo = new GameObject("TweakingPanel");
        //panelGo.AddComponent<TweakingPanel>();
        //screen.AddElement(panelGo);

        return screen;
    }

    public MenuScreen MakeInfoScreen()
    {
        PlatformSpecifics.HideAd();

        var go = new GameObject("InfoScreen");
        var screen = go.AddComponent<InfoScreen>();
        screen.MenuManager = this;

        return screen;
    }

    public MenuScreen MakeUnlockPremium1Screen()
    {
        PlatformSpecifics.HideAd();

        var go = new GameObject("UnlockPremium1Screen");
        var screen = go.AddComponent<UnlockPremium1Screen>();
        screen.MenuManager = this;
        return screen;
    }
	
	public MenuScreen MakeUpgradesScreen()
	{
        _upgradesHolder = new GameObject("UpgradesHolder");
        var vm = _upgradesHolder.AddComponent<VendingMachine>() as VendingMachine;
        vm.MenuManager = this;
		return vm;
	}

	public MenuScreen MakeOutroEasy()
	{
        var go = new GameObject("OutroEasyScreen");
        var screen = go.AddComponent<OutroScreen>();
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroEasy1"));
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroEasy2"));
        screen.MenuManager = this;
        return screen;
	}
	
	public MenuScreen MakeOutroHard()
	{
        var go = new GameObject("OutroHardScreen");
        var screen = go.AddComponent<OutroScreen>();
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroHard1"));
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroHard2"));
        screen.MenuManager = this;
        return screen;
	}
	
	public MenuScreen MakeOutroExpert()
	{
        var go = new GameObject("OutroHardScreen");
        var screen = go.AddComponent<OutroScreen>();
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroExpert1"));
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroExpert2"));
		screen.Backgrounds.Add(TextureCache.GetCachedTexture("Menus/Comics/OutroExpert3"));
        screen.MenuManager = this;
        return screen;
	}

    #endregion

}
