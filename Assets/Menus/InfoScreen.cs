using UnityEngine;
using System.Collections;

public class InfoScreen : MenuScreen {

    public override void RenderMainLayer()
    {

        var style = PHIUtil.MakeDialogTextStyle();
        style.alignment = TextAnchor.MiddleCenter;
        GUI.Label(new Rect(0, Screen.height * 0.1f, Screen.width, Screen.height * 0.35f),
            string.Format("Cookie Munchers to the Moon!\nVersion {0}\nCopyright 2011, 2012 by Pixel Heavy Industries LLC", DefaultSetter.VERSION), 
            style);

        if (!DialogSpawner.DialogShowing && GUI.Button(new Rect(Screen.width - 10, 0, 13, 13), "", style))
        {
            Application.LoadLevel("WinScenes");
        }
		
		if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .4f, Screen.width * .5f, Screen.height * .07f), "Simulate No Store Response: " + InAppPurchasingManager.SimulateNoStoreResponse.ToString()))
		{
			InAppPurchasingManager.SimulateNoStoreResponse = !InAppPurchasingManager.SimulateNoStoreResponse;
			InAppPurchasingManager.SessionStatus = InAppPurchasingManager.Status.Uninitialized;
	        IOSInAppPurchasingManager.RestartLoad();
	        IOSInAppPurchasingManager.Init();
		}

		if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .5f, Screen.width * .5f, Screen.height * .07f), "Simulate Bad Prod List: " + InAppPurchasingManager.SimulateBadProductList.ToString()))
		{
			InAppPurchasingManager.SimulateBadProductList = !InAppPurchasingManager.SimulateBadProductList;
			InAppPurchasingManager.SessionStatus = InAppPurchasingManager.Status.Uninitialized;
	        IOSInAppPurchasingManager.RestartLoad();
	        IOSInAppPurchasingManager.Init();
		}

		if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .6f, Screen.width * .5f, Screen.height * .07f), "Simulate Bad Response: " + InAppPurchasingManager.SimulateStoreErrorResponse.ToString()))
		{
			InAppPurchasingManager.SimulateStoreErrorResponse = !InAppPurchasingManager.SimulateStoreErrorResponse;
			InAppPurchasingManager.SessionStatus = InAppPurchasingManager.Status.Uninitialized;
	        IOSInAppPurchasingManager.RestartLoad();
			IOSInAppPurchasingManager.Init();
		}

/*        if (!DialogSpawner.DialogShowing && GUI.Button(new Rect(0, Screen.height * .3f, Screen.width, Screen.height * .15f),
            "Privacy Policy >",
            style))
        {
            Application.OpenURL("http://pixelheavyindustries.com/privacy/");
        }

        if (!DialogSpawner.DialogShowing && GUI.Button(new Rect(0, Screen.height * .5f, Screen.width, Screen.height * .15f),
            "End User License\nAgreement >",
            style))
        {
            Application.OpenURL("http://pixelheavyindustries.com/eula/");
        }*/

        var btnRect = new Rect(ScreenDimensions.GameAreaInPixels.x,
            ScreenDimensions.GameAreaInPixels.y + ScreenDimensions.GameAreaInPixels.height * .8f,
            ScreenDimensions.GameAreaInPixels.width,
            ScreenDimensions.GameAreaInPixels.height * .2f);
        if (!DialogSpawner.DialogShowing && GUI.Button(btnRect, TextureCache.GetCachedTexture("Menus/ButtonReset"), PHIUtil.MakeCenteredButtonGUIStyle()))
            DialogSpawner.ShowResetDialog();
    }
}
