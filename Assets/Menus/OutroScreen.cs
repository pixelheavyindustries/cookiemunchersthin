using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OutroScreen : MenuScreen {
	
	public List<Texture2D> Backgrounds;
	private int _currScreen = 0;
	private bool _buttonDown;
	
	public void Start()
	{
		ShowBackButton = false;
	}
	
	public OutroScreen()
	{
		Backgrounds = new List<Texture2D>();
	}	
	
    public override void RenderMainLayer()
    {
		if (_currScreen < Backgrounds.Count)
		{
			Background = Backgrounds[_currScreen];
		}
		else if (_currScreen == Backgrounds.Count)
		{
			Background = null;
			RenderCompleteScreen();
		}
		else
		{
			MenuManager.ResetScreensToModes();
			Application.LoadLevel("MenuMain");
		}	
		
		if (RenderNextButton())
			_currScreen++;
	}
	
	protected bool RenderNextButton()
	{
		var btnTex = TextureCache.GetCachedTexture("Gui/Dialog/Next") as Texture2D;
		var btnHeight = ScreenDimensions.GameAreaInPixels.height * .1f;
		var btnWidth = btnTex.width * btnHeight / btnTex.height; 
		var padding = ScreenDimensions.GameAreaInPixels.height * .03f;
		
        var pos = new Rect( Screen.width - btnWidth - padding,
                            Screen.height - btnHeight - padding,
                            btnWidth,
                            btnHeight);
        var down = (GUI.Button(pos, btnTex, PHIUtil.MakeButtonGUIStyle()));
		
		//need to only return true on the first hit, not the hold.  Otherwise, we'll fly thru all the pages really quickly
		if (!_buttonDown && down)
		{
			_buttonDown = true;
			return true;
		}
		
		if (!down)
			_buttonDown = false;
		
		return false;
	}
	
	protected void RenderCompleteScreen()
	{
        var diff = (ClimberMain.DifficultyLevel)Prefs.CurrentDifficulty;

        var style = PHIUtil.MakeDialogTextStyle();
        var text = string.Format("{0} mode complete!", (diff).ToString());
        GUI.Label(new Rect(0, Screen.height/5, Screen.width, Screen.height/5), text, style);

        var totalAchievedStars = 0;
        var numMissions = Prefs.GetHighestMissionLevelPossible(diff);
        for (var i = 1; i <= numMissions; i++)
            totalAchievedStars += Prefs.GetStarScore(diff, i);
        var totalAvailableStars = 3 * numMissions;

        text = string.Format("{0}/{1} stars", totalAchievedStars, totalAvailableStars);
        GUI.Label(new Rect(0, Screen.height * 2 / 5, Screen.width, Screen.height / 5), text, style);
	}
	
}
