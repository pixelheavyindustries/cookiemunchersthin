using UnityEngine;
using System.Collections;
using System;

public class MenuMain : MonoBehaviour {
	
//	public MonoBehaviorIAP Purchasing;

	// Use this for initialization
	void Start () {
        //track the start
        Logging.LogSession();
#if UNITY_IPHONE
        IOSInAppPurchasingManager.Init();
#endif
        ScreenDimensions.Initialize(PlatformSpecifics.AllowAds && !Prefs.PremiumPackUnlocked);

        TextureCache.PreCacheTextures();

        gameObject.AddComponent<MenuManager>();
//        gameObject.AddComponent<MuteButton>();
		
		//Purchasing = gameObject.AddComponent<MonoBehaviorIAP>();
	}

    // Update is called once per frame
    void Update()
    {
	
	}

    void OnGUI()
    {
                var statusText = "Determined? " + IOSInAppPurchasingManager.CanPurchaseDetermined + "\n" +
                                    "Available?  " + IOSInAppPurchasingManager.CanPurchase + "\n" +
                                    "Gotten Products?  " + IOSInAppPurchasingManager.AreProductsRetrieved + "\n" +
                                    "Premium?   " + Prefs.PremiumPackUnlocked + "\n" + 
									"Sess Status? " + IOSInAppPurchasingManager.SessionStatus.ToString() + "\n" +
									"Load Tries? " + IOSInAppPurchasingManager.NumLoadTries.ToString();
                var guiStyle = new GUIStyle();
                guiStyle.normal.textColor = Color.black;
                GUI.Label(new Rect(200, 0, 200, 300), statusText, guiStyle);
                guiStyle.normal.textColor = Color.white;
                GUI.Label(new Rect(200, 0, 200, 300), statusText, guiStyle);
    }
}
