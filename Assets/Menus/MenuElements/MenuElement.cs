using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MenuElement : MonoBehaviour
{
	static MenuElement()
	{
		Enumerable.Empty<RaycastHit>().GetEnumerator();
	}
	
    public enum MenuElementType
    {
        Continuation,
        Button,
        HorizontalScroller,
        VerticalScroller,
    }

    public String Id;
    public MenuElementType Type;
    public bool Enabled;
    public Vector3 HomePosition;

    //enhance this setter to change whatever needs changing
    public bool Visible { get; set; }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public virtual void HandleClick()
    {
        
    }
}
