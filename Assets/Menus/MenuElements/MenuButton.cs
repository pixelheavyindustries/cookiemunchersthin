using System;
using UnityEngine;
using System.Collections;

public class MenuButton : MenuElement {
     
    public Action<String> ClickCallback;

    public MenuButton()
    {
        Type = MenuElementType.Button;
    }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void HandleClick()
    {
        //print("Clicked: " + Id);
        ClickCallback(Id);
    }
}
