using UnityEngine;
using System;
using System.Collections;

public class DrawableMenuElement {
	
	public string ID;
	
	protected Texture2D texture;
	protected Rect position;
	protected Action<string> callback;
	public bool Locked;
	
	public static DrawableMenuElement FromRelativePosition(string id, string texPath, Vector2 pos, float height, Action<string> cb, bool locked = false)
	{
		var tex = TextureCache.GetCachedTexture(texPath + (locked ? "Locked" : "")) as Texture2D;
		var newHeight = height * ScreenDimensions.GameAreaInPixels.height;
		var newWidth = tex.width * newHeight / tex.height;
		var centerX = ScreenDimensions.GameAreaInPixels.width * pos.x;	
		var centerY = ScreenDimensions.GameAreaInPixels.height * pos.y;	
		var rectPos = new Rect(centerX - newWidth/2, centerY - newHeight/2, newWidth, newHeight);
		
		var element = new DrawableMenuElement(id, tex, rectPos, cb);
		element.Locked = locked;
		return element;
	}
	
	public DrawableMenuElement(string id, Texture2D tex, Rect pos, Action<string> cb)
	{
		ID = id;
		texture = tex;
		position = pos;
		callback = cb;
	}
	
	public void Draw()
	{
		GUI.DrawTexture(position, texture, ScaleMode.ScaleToFit);
		
		if (!Locked)
		{
        	var style = PHIUtil.MakeCenteredButtonGUIStyle();
			style.fixedWidth = position.width;
			style.fixedHeight = position.height;
			
			if (GUI.Button(position, "", style))
			{
				callback(ID);
			}
		}
	}
}
