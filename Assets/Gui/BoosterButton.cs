using UnityEngine;
using System.Collections;

public class BoosterButton : MonoBehaviour {

    private Texture2D[] _buttons;

    private Rect _position;
    public Rect HitZone { get { return _position; }}
    private int _stage;
    private int _numStages;
    private int[] _thresholds;
    private int _maxThreshold;
    private float[] _boostTimes;
    private int _fuelCount = 0;

    private GUIStyle _style;
    private GUIStyle _shadowStyle;

	// Use this for initialization
	void Start () {
    }

    public void Init(Rect position)
    {
        _style = new GUIStyle();
        _shadowStyle = new GUIStyle();
        _style.fontSize = _shadowStyle.fontSize = 15;
        _style.fontStyle = _shadowStyle.fontStyle = FontStyle.Bold;
        _style.normal.textColor = Color.white;
        _shadowStyle.normal.textColor = Color.black;
        _style.alignment = _shadowStyle.alignment = TextAnchor.LowerCenter;

        _thresholds = new int[] {0, 10, 25, 60};
        _boostTimes = new float[] {0, 2f, 3f, 5f };
        _numStages = 3;
        _maxThreshold = _thresholds[_numStages];

        _buttons = new Texture2D[4];
        _buttons[0] = Resources.Load("Gui/BoosterDisabled") as Texture2D;
        _buttons[1] = Resources.Load("Gui/BoosterStage1") as Texture2D;
        _buttons[2] = Resources.Load("Gui/BoosterStage2") as Texture2D;
        _buttons[3] = Resources.Load("Gui/BoosterStage3") as Texture2D;
        _position = position;

//        print("Init: " + _position + "   " + HitZone);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnGUI()
    {
        GUI.depth = 0;
        GUI.DrawTexture(_position, _buttons[_stage], ScaleMode.ScaleAndCrop, true);
        /*        var fuelGoal = _thresholds[Mathf.Min(_stage + 1, _numStages)];
                GUI.Label(new Rect(_position.x + 1, _position.y + 1, _position.width, _position.height), 
                    _fuelCount + "/" + fuelGoal, _shadowStyle);
                GUI.Label(_position, _fuelCount + "/" + fuelGoal, _style);*/
    }

    public float Click()
    {
        if (_stage > 0)
        {
            var boost = _boostTimes[_stage];
            _stage = 0;
            _fuelCount = 0;
            return boost;
        }

        return 0f;
    }

    public void AddFuel(int howMuch)
    {
        _fuelCount += howMuch;
        _fuelCount = Mathf.Min(_fuelCount, _maxThreshold);

        if (_fuelCount >= _thresholds[_numStages] || _stage == _numStages)
            _stage = _numStages;
        else
        {
            //take _stage from 0 to the highest threshold we've passed
            while (_fuelCount >= _thresholds[_stage+1] && _stage < _numStages)
                _stage++;
        }
    }
}
