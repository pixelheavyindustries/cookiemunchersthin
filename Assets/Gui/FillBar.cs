using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FillBar : MonoBehaviour {

    //size and position of the fillbar.  
    //Width will be respected more than height.  
    //Any extra height will be padded with transparency on the bottom
    protected Rect _fillSize;
    protected Rect _labelSize;
    public Rect Size
    {
        get { return new Rect(_labelSize.x, _labelSize.y, _labelSize.width + _fillSize.width, _labelSize.height); }
        set
        {
            //we completely ignore value.height, which we shouldn't.  we should really compress the object horizontally if value.height is too small
            _labelSize = new Rect(value.x, value.y, value.width * LabelBarProportion, 0);
            _fillSize = new Rect(value.x + _labelSize.width, value.y, value.width - _labelSize.width, 0);
            _scalingFactor = _fillSize.width / BaseTexture.width;
            _fillSize.height = _labelSize.height = BaseTexture.height * _scalingFactor;
			_cookieChunkDestinationPos = new Vector2(value.x + value.width/2, value.y + value.height/2);
        }
    }
    protected float _scalingFactor;

    //this should be measured in photoshop.  It should not change regardless of how big or small we're displaying the fillbar
    public Vector2 FillOffset;
    public float MaxFillWidth;
    public float FillTextureHeight;

    public float FlashPeriod = .5f;
    public float FlashDuration = .25f;
    public float FlashThreshold = .2f;

    //layers are drawn in this order
    public Texture BaseTexture;
    //When bar gets too low, this will be alternated in to make it flash
    public Texture FlashBaseTexture;
    //will be drawn at _fillOffset relative to BaseTexture
    public Texture FillTexture;
    //must be same dimensions as BaseTexture
    public Texture OverlayTexture;

    //this will get placed left or right of the fillbar
    public Texture LabelTexture;
    public bool LabelOnLeft = true;
    //label will be 30% of the width, fillbar will be the rest
    public float LabelBarProportion { get { return LabelTexture == null ? .000001f : .3f; } }
	
	//we need to ease the fill from where it is to targetFillAmount
    private float _fillAmount;
	private float _targetFillAmount;
    public float FillAmount { 
        get { return _targetFillAmount; } 
        set { 
            _targetFillAmount = Mathf.Max(0, Mathf.Min(value, 1));
        } 
    }
	
	protected Queue<CookieChunk> _cookieChunks;
	protected Vector2 _cookieChunkDestinationPos;
	public Texture2D CookieChunkTexture;
	protected float _cookieChunkSize = 1f;
	
    private bool _initialized = false;

    public void Start()
    {
		_cookieChunks = new Queue<CookieChunk>();
		_cookieChunkSize = Screen.width / 10f;
        _initialized = true;
    }

    public void OnGUI()
    {
        if (!_initialized)
            return;
		
		_fillAmount = (_fillAmount * 29f + _targetFillAmount) / 30f;
		
        if (!BaseTexture || !FillTexture || !OverlayTexture)
        {
//            Debug.LogError("Fillbar is missing one of the textures.  Assign it in the inspector.");
            return;
        }

        GUI.depth = RenderDepths.TUMMY_METER;

		var targetPos = _fillSize.x + (FillOffset.x + MaxFillWidth * _fillAmount ) * _scalingFactor;
		
		//draw animated cookie chunks
		foreach(var cc in _cookieChunks)
		{
			if (!cc.Complete && CookieChunkTexture != null)
			{
				var t = GameClock.CurrTime - cc.Start;
				var x = Tweenk.Easing.EaseOutCirc(t, cc.FromPos.x, targetPos - cc.FromPos.x, cc.Duration);
				var y = Tweenk.Easing.EaseNone(t, cc.FromPos.y, _cookieChunkDestinationPos.y- cc.FromPos.y, cc.Duration);
				GUI.DrawTexture(new Rect(	x - _cookieChunkSize/2,
											y - _cookieChunkSize /2,
											_cookieChunkSize,
											_cookieChunkSize), 
								CookieChunkTexture);
			}
		}		
		
		//Draw tummy meter
        if (_fillAmount <= FlashThreshold && GameClock.CurrTime % FlashPeriod < FlashDuration)
            GUI.DrawTexture(_fillSize, FlashBaseTexture, ScaleMode.ScaleToFit, true);
        else
            GUI.DrawTexture(_fillSize, BaseTexture, ScaleMode.ScaleToFit, true);

        var fillTextureSize = new Rect(
            _fillSize.x + FillOffset.x * _scalingFactor,
            _fillSize.y + FillOffset.y * _scalingFactor,
            MaxFillWidth * _fillAmount * _scalingFactor,
            FillTextureHeight * _scalingFactor);
        GUI.DrawTexture(fillTextureSize, FillTexture, ScaleMode.StretchToFill, true);

        GUI.DrawTexture(_fillSize, OverlayTexture, ScaleMode.ScaleToFit, true);

        if (LabelTexture != null)
            GUI.DrawTexture(_labelSize, LabelTexture, ScaleMode.ScaleToFit, true);

		GUI.depth = 0;
		
		while(_cookieChunks != null && _cookieChunks.Count > 0 && _cookieChunks.Peek().Complete)
			_cookieChunks.Dequeue();
    }
	
	protected class CookieChunk
	{
		public Vector2 FromPos;
		public float Duration;
		public float Start;
		float Value;
		public bool Complete { get { return Start + Duration < GameClock.CurrTime; }}		
	}
	
	public void Send(Vector3 fromPos, int howMany)
	{
		for(var i = 0; i < howMany; i++)
			SendOne(fromPos, .65f + .2f * i);
	}
	
	private void SendOne(Vector3 fromPos, float duration)
	{
		var fromPos2D = Camera.main.WorldToScreenPoint(fromPos);
		fromPos2D.y = Screen.height - fromPos2D.y;
		var chunk = new CookieChunk{
			FromPos = fromPos2D,
			Start = GameClock.CurrTime,
			Duration = duration
		};
		
		_cookieChunks.Enqueue(chunk);
	}
}

//actual vs indicated?


