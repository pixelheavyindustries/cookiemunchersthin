using UnityEngine;
using System.Collections;

public class Altimeter : MonoBehaviour {

    public IAltitudeProvider _mainRef;
    private Rect _position;
    private float _height;
    protected GUIStyle _style;

    private Rect _startPos;
    private Rect _endPos;
    private Texture2D _startTex;
    private Texture2D _endTex;
    private Texture2D _caretTex;

	// Use this for initialization
	public void Init (Rect position, IAltitudeProvider mainRef) {
        _mainRef = mainRef;
        _position = position;
        _height = position.height;
        _startTex = Resources.Load("Gui/Altimeter/Start") as Texture2D;
        _startPos = new Rect(_position.x, _position.y, _startTex.width, _height);

        _endTex = Resources.Load("Gui/Altimeter/End") as Texture2D;

        _endPos = new Rect(_position.x + position.width - _height, _position.y, _height, _height);

        _caretTex = Resources.Load("Gui/Altimeter/Caret") as Texture2D;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnGUI()
    {
        if (_mainRef == null || _position == null || _startTex == null || _endTex == null || _caretTex == null)
            return;

        GUI.depth = 0;

//        GUI.DrawTexture(_startPos, _startTex);
        GUI.DrawTexture(_endPos, _endTex);

        var altRatio = _mainRef.GetAltitude() / _mainRef.GetWinAltitude();
        var width = _endPos.x - _startPos.x;
        GUI.DrawTexture(new Rect(_startPos.x, _startPos.y, width * altRatio, _height), _caretTex);
//        GUI.DrawTexture(new Rect(_startPos.x + (_endPos.x - _startPos.x) * altRatio, _startPos.y, _caretTex.width, _caretTex.height),
//            _caretTex);
    }
}
