using UnityEngine;
using System.Collections;

public class MuteButton : MonoBehaviour {

    private Texture2D _texMuted;
    private Texture2D _texUnmuted;
    private GUIStyle _buttonStyle;

	// Use this for initialization
	void Start () {
        _texMuted = TextureCache.GetCachedTexture("Gui/ButtonMuteMuted");
        _texUnmuted = TextureCache.GetCachedTexture("Gui/ButtonMute");
        _buttonStyle = PHIUtil.MakeButtonGUIStyle();

        if (Prefs.Muted)
            AudioListener.volume = 0.0f;
    }

    void OnGUI()
    {
        var width = ScreenDimensions.GameAreaInPixels.width / 5f;

        if (Prefs.Muted)
        {
            if (GUI.Button(new Rect(ScreenDimensions.GameAreaInPixels.width - width,
                ScreenDimensions.GameAreaInPixels.yMax - width, 
                width, width), 
                _texMuted, _buttonStyle))
            {
                Prefs.Muted = false;
                AudioListener.volume = 1.0f;
            }
        }
        else             
        {
            if (GUI.Button(new Rect(ScreenDimensions.GameAreaInPixels.width - width,
                ScreenDimensions.GameAreaInPixels.yMax - width,
                width, width), 
                _texUnmuted, _buttonStyle))
            {
                Prefs.Muted = true;
                AudioListener.volume = 0.0f;
            }
        }

    }
}
