using System;
using UnityEngine;
using System.Collections;

public class Dialog : MonoBehaviour {

    protected Rect _position;
    protected DialogDef _def;
    protected Func<DialogDef, bool> _doRender;
    protected bool _showContents = false;

    private Texture2D _texture;

    public int Id
    {
        get;
        private set;
    }


    public void Init(int id, DialogDef def)
    {
        Id = id;
        _def = def;
        _doRender = _def.RenderContents;
        _position = def.Rect.GetValueOrDefault(new Rect(-400, 0, Screen.width + 800, Screen.height));
        _texture = def.Background ?? TextureCache.GetCachedTexture("Gui/Dialog/DialogBackground");
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void OnGUI () {

//        print("Dialog.OnGUI: " + _position);
        if (_texture == null)
            return;

        GUI.depth = RenderDepths.DIALOG;
        GUI.DrawTexture(_position, _texture, ScaleMode.ScaleToFit);
		
        GUI.depth = RenderDepths.DIALOG_ELEMENT;		
        _doRender(_def);
		GUI.depth = 0;
    }

    public void Show()
    {
        //animate the size and maybe alpha

        //then call this
        ShowComplete();
    }

    public bool ShowComplete()
    {
        _showContents = true;
        return true;
    }

    public void Hide()
    {
        //animate out, then call this
        HideComplete();
    }

    public bool HideComplete()
    {
        Destroy(this.gameObject);
        Destroy(this);
        return true;
    }
}
