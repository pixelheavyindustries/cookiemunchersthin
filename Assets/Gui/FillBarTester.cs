using System;
using UnityEngine;
using System.Collections;

public class FillBarTester : MonoBehaviour {

    private FillBar _staminaBar;
	private GameObject _muncher;

	// Use this for initialization
	public void Start () {
        new GameObject("GameClockHolder").AddComponent<GameClock>();
	}
	
	// Update is called once per frame
    public void Update()
    {
        if (_staminaBar == null)
        {
            _staminaBar = LevelSpawner.MakeFillBar(new Rect(Screen.width/2 - 150, 0, 300, 60), TextureCache.GetCachedTexture("Gui/StaminaBarLabel") as Texture);
            Camera.main.transform.position = new Vector3(0, 0, -10f);
        }
		
		if (_muncher == null)
		{
			_muncher = LevelSpawner.MakeCookie(false, Cookie.CookieShape.Circle, Cookie.Color.Orange, 1f, 1f, 7f, false);
			var cookieScript = _muncher.GetComponent<Cookie>();
			cookieScript.HomePos = new Vector3(0, -4f, 0);
		}
		
		_muncher.transform.position = new Vector3(Mathf.Sin (Time.time), 0, 0);
	}
	
	public void OnGUI()
	{
		if (GUI.Button(new Rect(20, 500, 200, 80), "Add 1"))
		{
			_staminaBar.FillAmount += +.08f;
			_staminaBar.Send(_muncher.transform.position, 1);
		}
		if (GUI.Button(new Rect(240, 500, 200, 80), "Add 1"))
		{
			_staminaBar.FillAmount += +.08f;
			_staminaBar.Send(_muncher.transform.position, 4);
		}
		if (GUI.Button(new Rect(460, 500, 200, 80), "Add 1"))
		{
			_staminaBar.FillAmount = 0;
		}
	}
}
