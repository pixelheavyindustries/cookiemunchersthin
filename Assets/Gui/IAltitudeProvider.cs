using UnityEngine;
using System.Collections;

public interface IAltitudeProvider
{
    float GetAltitude();
    float GetWinAltitude();
}
