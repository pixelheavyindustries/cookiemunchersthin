using System;
using UnityEngine;
using System.Collections;

public class WingRocker : MonoBehaviour
{

    public float MaxAngle = 10.0f;
    public float CycleTime = 3.0f;
    public Vector3 Axis = Vector3.up;

    private float _startTime;

	// Use this for initialization
	void Start ()
	{
	    _startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update ()
	{
        var timeFactor = ((Time.time - _startTime )% CycleTime) / CycleTime;
        transform.rotation = Quaternion.AngleAxis(90, Vector3.forward) * Quaternion.AngleAxis((float)Math.Sin(timeFactor*Math.PI*2) * MaxAngle, Vector3.forward);
	}
}
