using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum UpgradeType
{
    Booster,
    StaminaSnack,
    Coins,
    Zippers,
    GoodCookies,
    MaxSpeed,
    BonkProofing,
    StaminaBurn,
    StaminaFill,
    BonusBoostSpeed,
    BonusBoostCombosRequired,
    BonusBoostDuration,
    ComboTimeout,
	MinigameToken,
}

public class Upgrade
{
    public readonly UpgradeType Type;
    public readonly string Name;
    public readonly float Default;

    public List<UpgradeLevel> Upgrades;

    public Upgrade(UpgradeType type, string name, float def, List<UpgradeLevel> upgrades)
    {
        Name = name;
        Type = type;
        Default = def;
        Upgrades = upgrades;
    }

    //levels run from 0, which is the default state before purchasing any, to number of levels
    public int CurrLevel { get { return Prefs.GetUpgradeLevel(Type); } }
    public int MaxLevel { get { return Upgrades.Count; } }
}

public class UpgradeLevel
{
    public string Description;
    public float Value;
    public int Price;
}

public class UpgradeManager {
    protected static bool _initialized = false;
    protected static Dictionary<UpgradeType, Upgrade> _upgrades;
	
	protected static int PRICE_1 = 100;
	protected static int PRICE_2 = 200;
	protected static int PRICE_3 = 400;
	
    protected static void EnsureInitialized()
    {
        if (!_initialized)
        {
            _initialized = true;
            _upgrades = new Dictionary<UpgradeType, Upgrade>();

            var boosterUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 4,
                    Description = "Activated it by getting lots of combos.", 
                },
            };
            _upgrades.Add(UpgradeType.Booster, new Upgrade(UpgradeType.Booster, "Emergency Booster", 0f, boosterUpgrades));

            var snackUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_1,
                    Value = 215f,//200f
                    Description = "Makes donuts more common.", 
                },
                new UpgradeLevel() { Price = PRICE_2,
                    Value = 160f,//150f
                    Description = "Makes donuts even more common.", 
                },
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 130f,//100f
                    Description = "Makes donuts even MORE common.", 
                },
            };
            _upgrades.Add(UpgradeType.StaminaSnack, new Upgrade(UpgradeType.StaminaSnack, "Donut", 275f, snackUpgrades));

            var coinUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_1,
                    Value = 43f,
                    Description = "Makes coins more common.", 
                },
                new UpgradeLevel() { Price = PRICE_2,
                    Value = 35f,
                    Description = "Makes coins even more common.", 
                },
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 28f,
                    Description = "Makes coins even MORE common.", 
                },
            };
            _upgrades.Add(UpgradeType.Coins, new Upgrade(UpgradeType.Coins, "Coins", 50f, coinUpgrades));

            var zipperUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_1,
                    Value = 200f,
                    Description = "Makes cupcakes more common.", 
                },
                new UpgradeLevel() { Price = PRICE_2,
                    Value = 150f,
                    Description = "Makes cupcakes even more common.", 
                },
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 100f,
                    Description = "Makes cupcakes even MORE common.", 
                },
            };
            _upgrades.Add(UpgradeType.Zippers, new Upgrade(UpgradeType.Zippers, "Zippers", 275f, zipperUpgrades));


            var bonkUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_1,
                    Value = 1f,
                    Description = "Makes asteroids hurt less.", 
                },
                new UpgradeLevel() { Price = PRICE_2,
                    Value = 2f,
                    Description = "Makes asteroids hurt even less.",  
                },
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 3f,
                    Description = "Makes asteroids hurt the least.", 
                },
            };
            _upgrades.Add(UpgradeType.BonkProofing, new Upgrade(UpgradeType.BonkProofing, "Bonk Proofing", 0f, bonkUpgrades));

            var comboTimeoutUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_1,
                    Value = .95f,
                    Description = "Keeps combos going a little longer.", 
                },
                new UpgradeLevel() { Price = PRICE_2,
                    Value = 1.1f,
                    Description = "Keeps combos going a lot longer.", 
                },
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 1.25f,
                    Description = "Keeps combos going waaaaaaaay longer.", 
                },
            };
            _upgrades.Add(UpgradeType.ComboTimeout, new Upgrade(UpgradeType.ComboTimeout, "Easier Combos", .8f, comboTimeoutUpgrades));
			
            var pieUpgrades = new List<UpgradeLevel>(){
                new UpgradeLevel() { Price = PRICE_1,
                    Value = 600f,
                    Description = "More pie slices.", 
                },
                new UpgradeLevel() { Price = PRICE_2,
                    Value = 450f,
                    Description = "Even more pie slices.", 
                },
                new UpgradeLevel() { Price = PRICE_3,
                    Value = 300f,
                    Description = "Even MORE pie slices.", 
                },
            };
            _upgrades.Add(UpgradeType.MinigameToken, new Upgrade(UpgradeType.MinigameToken, "Pie", 800f, pieUpgrades));
        }
    }

    public static Upgrade GetUpgrade(UpgradeType type)
    {
        EnsureInitialized();
        return _upgrades[type];
    }

    public static float GetUpgradeValue(UpgradeType type)
    {
        var currLevel = Prefs.GetUpgradeLevel(type);
        var upgrade = GetUpgrade(type);

        if (currLevel == 0)
            return upgrade.Default;

        return upgrade.Upgrades[currLevel-1].Value;
    }

    public static void DoUpgrade(UpgradeType type)
    {
        EnsureInitialized();
        var currLevel = Prefs.GetUpgradeLevel(type);
        Prefs.Money -= _upgrades[type].Upgrades[currLevel].Price;
        Prefs.SetUpgradeLevel(type, currLevel + 1);
    }
}
