using UnityEngine;
using System.Collections;

public class Fireworks : FightingEntity {
	
	public float Duration = 1.5f;
	public float MaxScale = 10;
	protected Tweenk _tweenk;
	
	void Start()
	{
		base.Start ();
		_tweenk = gameObject.AddComponent<Tweenk>();
		_tweenk.XScale(MaxScale, Duration, Tweenk.Easing.EaseOutSine);
		_tweenk.YScale(MaxScale, Duration, Tweenk.Easing.EaseOutSine);
		_tweenk.Callback(Duration, DoDestroy);
	}
	
	// Update is called once per frame
	public bool DoDestroy () {
		_tweenk.StopAll();
		Destroy(gameObject);
		return true;
	}
}
