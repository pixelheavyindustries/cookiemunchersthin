using UnityEngine;
using System.Collections;

public class Coin : FightingEntity
{
    public float MaxAngle = 10.0f;
    public float CycleTime = 3.0f;
    public Vector3 Axis = Vector3.up;
    private float _startTime;

    public int CoinValue = 1;

    public void Update()
    {
        base.Update();

        var timeFactor = ((Time.time - _startTime) % CycleTime) / CycleTime;
        transform.rotation = Quaternion.AngleAxis(360 * timeFactor, Vector3.up);
    }
}
