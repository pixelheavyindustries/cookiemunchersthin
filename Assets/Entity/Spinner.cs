using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {

    public Vector3 Axis;
    public float Speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.AngleAxis((Time.time % 10000) * Speed, Axis);
	}
}
