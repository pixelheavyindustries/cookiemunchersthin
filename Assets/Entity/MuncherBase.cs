using UnityEngine;
using System.Collections;

public class MuncherBase : FightingEntity {
	
    public Cookie.Color BaseColor;
    public Cookie.Color SpotColor;
    public Cookie.CookieShape Shape;
	
    public GameObject ClosedMouthFace;
    public GameObject OpenMouthFace;

    public void OpenMouth()
    {
        PHIUtil.SetChildVisibility(ClosedMouthFace, false, false);
        PHIUtil.SetChildVisibility(OpenMouthFace, true, false);
    }
    public void CloseMouth()
    {
        PHIUtil.SetChildVisibility(ClosedMouthFace, true, false);
        PHIUtil.SetChildVisibility(OpenMouthFace, false, false);
    }	
}


