using UnityEngine;
using System.Collections;
using System.Linq;

public class AutoPilotCookieMuncher : CookieMuncher {
	
	private float idealCookieDist;
	float lastTargetTime = 0f;
	float pickNewTargetAt = 0f;
	bool waitToPickNewTarget = true;
	
	public void Start()
	{
		base.Start ();
		idealCookieDist = ScreenDimensions.GameAreaInMeters.height * .5f;
	}
	
	public override void Update () 
	{	
		if (TargetObj == null)
		{
			if (waitToPickNewTarget)
			{
				pickNewTargetAt = GameClock.CurrTime + Random.Range(.5f, 1f);
				waitToPickNewTarget = false;
			}
			else if (GameClock.CurrTime > pickNewTargetAt)
			{
				lastTargetTime = GameClock.CurrTime;
				var goodCookies = MainRef.GetGoodCookies();
				
				if (goodCookies.Count > 0)
				{
					var targetCookies = (from c in goodCookies 
										where c.gameObject.transform.position.y > gameObject.transform.position.y
										orderby GetAwesomenessFactor2 (gameObject, c.gameObject) ascending 
										select c).ToList();
					if (targetCookies.Count > 0)
					TargetObj = targetCookies.First().gameObject;
					waitToPickNewTarget = true;
				}
			}
		}
		
		base.Update ();
	}

	private float GetAwesomenessFactor(GameObject _muncher, GameObject _cookie)
	{
		var vertDist = _cookie.transform.position.y - _muncher.transform.position.y;
		var horizDist = Mathf.Abs(_muncher.transform.position.x - _cookie.transform.position.x);
		var isInConeOfAwesome = _cookie.transform.position.y > _muncher.transform.position.y &&
								horizDist < vertDist / 1.2f;
		
		if (isInConeOfAwesome)
		{
			return vertDist;
		}
		//we mostly want to key off the vertical distance, so we try to minimize the effects of sideways separation
		var sidewaysDivisor = 3f;
		var horizDistFactor =  Mathf.Abs(horizDist) * sidewaysDivisor;
		var vertDistFactor =  Mathf.Abs(Mathf.Abs(vertDist) - idealCookieDist);
		return 100 * (horizDist + vertDist);
	}
	
	private float GetAwesomenessFactor2(GameObject muncherObj, GameObject cookieObj)
	{
		var vectorToCookie = muncherObj.transform.position - cookieObj.transform.position;
		var maxDistPenalty = vectorToCookie.sqrMagnitude > Mathf.Pow (ScreenDimensions.GameAreaInMeters.height * .3f, 2f) ? 1000 : 0;
		var muncherVel = new Vector3(FakeVelocity.x/2, FakeVelocity.y, 0f);
		return 85 * Vector3.Dot(muncherVel.normalized, vectorToCookie.normalized) 
			   + vectorToCookie.magnitude * 1
			   - maxDistPenalty;
	}
}
