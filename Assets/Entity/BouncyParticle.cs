using System;
using System.Collections.Generic;
using UnityEngine;

public class BouncyParticle : MonoBehaviour
{
    public static List<float> SineWave = new List<float>(new[] { 0.1f, 0.7f, 0.9f, 1.0f, 0.9f, 0.7f, 0.1f });
    public static List<float> Puff = new List<float>(new []{0.1f, 0.7f, 0.9f, 1.0f, 0.96f, 0.9f, 0.81f});
    public static List<float> PopCorn = new List<float>(new[] { 0.01f, 1f, 0.95f, .93f, 0.9f, 0.81f, .7f, .55f, .37f, .2f, 0.01f });

    public List<float> Scales = SineWave;
    public float Duration = 1f;
    private float _startTime;
    private float _originalScale;

	// Use this for initialization
	void Start ()
	{
	    _startTime = Time.time;
        _originalScale = this.gameObject.transform.localScale.x;
        if (Scales != null && Scales.Count > 0)
            gameObject.transform.localScale = new Vector3(Scales[0], Scales[0], 1);
    }
	
	// Update is called once per frame
	void Update ()
	{
	    var timeRatio = (Time.time - _startTime)/Duration;
        if (timeRatio > 1 && this.gameObject != null)
        {
            Destroy(this.gameObject);
            return;
        }

        if (Scales == null || Scales.Count == 0)
            return;

        if (Scales.Count == 1)
        {
            gameObject.transform.localScale = new Vector3(_originalScale * Scales[0], _originalScale * Scales[0], 1);
            return;
        }

	    int indexBefore = (int)Math.Floor(timeRatio*(Scales.Count-1));
	    int indexAfter = (int)Math.Ceiling(timeRatio*(Scales.Count-1));
	    var startEndRatio = timeRatio*(Scales.Count - 1)%1;
        float interpolatedScale = _originalScale * Scales[indexBefore] + 
                                (_originalScale * Scales[indexAfter] - _originalScale * Scales[indexBefore]) 
                                * startEndRatio;
        gameObject.transform.localScale = new Vector3(interpolatedScale, interpolatedScale, 1);
	}
}
