using UnityEngine;
using System.Collections;

public class BillboardAnimation : MonoBehaviour {

    public float Duration;
    public int[] Frames;
    public int TotalFrames;
    private int _currentFrameIndex = -1;
    private float _frameWidth = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (_frameWidth == 0)
            _frameWidth = 1.0f / TotalFrames;
        
        float ratio = (Time.time % Duration) / Duration;
        int frame = (int)(ratio * (float)TotalFrames);
        int newFrameIndex = Frames[frame];
        
        
        if (newFrameIndex != _currentFrameIndex)
        {
            _currentFrameIndex = newFrameIndex;

            if (newFrameIndex == -1)
            { 
                //dispatch a message telling object to destroy itself
            }

            var meshFilter = GetComponent<MeshFilter>();
            var mesh = meshFilter.mesh;
            var vertices = mesh.vertices;
            var triangles = mesh.triangles;
            mesh.Clear();

            mesh.vertices = vertices;
            mesh.triangles = triangles;

            float left = (float)newFrameIndex / TotalFrames;
            float right = left + _frameWidth;
            mesh.uv = new Vector2[] { new Vector2(left, 0), new Vector2(left, 1), new Vector2(right, 1), new Vector2(right, 0) };
            mesh.RecalculateNormals();
            //print("BillboardAnimation: getting frame index: " + newFrameIndex + " left: " + left + " right: " + right);
            gameObject.transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
        }
    }
}
