using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CookieDefinition
{
    public Cookie.Color? BaseColor;
    public Cookie.CookieShape? Shape;

    public CookieDefinition(Cookie.Color? baseColor, Cookie.CookieShape? shape)
    {
        BaseColor = baseColor;
        Shape = shape;
    }
}


public class Cookie : FightingEntity {


    public enum Color
    {
        None = 0,
        Pink,
        Orange,
        Blue,
        Green,
        Red,
        Turquoise,
//        Yellow,
    }

    public enum CookieShape
    {
        Circle = 0,
        Square,
        Triangle,
        Star,
    }

    public Vector3 HomePos;
    public float CookieMoveTime = 3f;
    public float CookieMoveDist = 4f;
    public float TimeOffset;

    public CookieShape Shape;
    public Cookie.Color BaseColor;
    public bool Good;

    public GameObject ClickZone;

    public LevelBase MainRef;

    private bool _selected = false;
    public bool Selected { get { return _selected; } set { _selected = value; PHIUtil.SetChildVisibility(ClickZone, _selected, true); } }

	// Use this for initialization
	public virtual void Start () {
        base.Start();
        HitCategory = HitCheckCategory.Badguy;
        DamagePoints = 0;
        HitPoints = 1;
	}
	
	// Update is called once per frame
    public virtual void Update()
    {
        if (TimeOffset == 0)
            TimeOffset = UnityEngine.Random.RandomRange(0f, CookieMoveTime);

        if (HomePos != null && CookieMoveTime > 0f)
        {
            var timeRatio = ((GameClock.CurrTime + TimeOffset) % CookieMoveTime) / CookieMoveTime;
            gameObject.transform.position = HomePos + new Vector3(Mathf.Sin(timeRatio * Mathf.PI * 2) * CookieMoveDist, 0, 0);
        }

        base.Update();
	}
	
}
