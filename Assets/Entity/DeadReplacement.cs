using System;
using UnityEngine;
using System.Collections;

public class DeadReplacement
{
    public Vector3 Offset;
    public Func<GameObject> Replacement;
}
