using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class CookieMuncher : MuncherBase {

    public enum CookieMuncherState
    {
        OnGround,
        Falling,
        HoppingTo,
        Boosting,
    }

    public ClimberMain MainRef;

    public CookieMuncherState State;

    public ClimberMain.DifficultyLevel Difficulty;
    public bool HasHorns = false;
    public BoosterButton Booster;

    public Tongue Tongue;

    public float LeftLimit;
    public float RightLimit;
    public float StartingY;
    protected bool _haveJumped = false;
    public GameObject TargetObj = null;
	public bool Jumping { get { return TargetObj != null; }}
    public float GrabRange = 10f;
    public float MaxSpeed = 0f;
    public float GrabAcceleration = 50f;
    public float Gravity = 15f;
    public float Stamina = 100f;
    public float MaxStamina = 100f;
    public float StaminaBurn = 0f;
    public float StaminaFill = 0f;

    protected float _nextComboExpiry = 0;
    protected int _comboTotal = 1;

    protected Tweenk _tweenk;

    public float BoosterThrust = 40f;
    public float MaxBoostSpeed = 70f;
    private bool _boosting = false;
    public bool Boosting { get { return _boosting; } }
    private float _boosterEndTime = 0;

    protected bool _flyOffScreen = false;
    protected float _stopTime;

    protected GameObject _smokeTrail;

	// Use this for initialization
	void Start () {
        HitCategory = HitCheckCategory.Goodguy;
        DamagePoints = 0;
        HitPoints = 1;
        State = CookieMuncherState.OnGround;
        _tweenk = gameObject.AddComponent<Tweenk>();

        _smokeTrail = Instantiate(Resources.Load("Particles/Smoke/Smoke Trail")) as GameObject;
        _smokeTrail.gameObject.transform.parent = gameObject.transform;
        SetSmokeTrailShowing(false);
	}
	
	// Update is called once per frame
	public virtual void Update () {

        if (_flyOffScreen)
        {
            if (Time.time < _stopTime)
            {
                FakeVelocity = new Vector3(0, FakeVelocity.y + Time.deltaTime * BoosterThrust, 0);
                base.Update();
            }

            return;
        }

        if (GameClock.Paused)
            return;

        if (_haveJumped)
            Stamina -= StaminaBurn * Time.deltaTime;

        if (_boosting)
        {
            FakeVelocity = new Vector3(0, Math.Min(FakeVelocity.y + BoosterThrust * Time.deltaTime, MaxBoostSpeed), 0);
            if (GameClock.CurrTime > _boosterEndTime)
            {
                _boosting = false;
                FakeVelocity = new Vector3(0, Math.Min(FakeVelocity.y, MaxSpeed), 0);
                SetSmokeTrailShowing(false);
            }

        }
        else if (TargetObj == null)
        {
            HideTongue();

            if (gameObject.transform.position.y <= StartingY && !_haveJumped)
            {
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, StartingY, gameObject.transform.position.z);
                FakeVelocity = new Vector3();
            }
            else
            {
                FakeVelocity -= new Vector3(0, Gravity, 0) * Time.deltaTime;
            }
        }
        //we passed it on the way up
        else if (TargetObj.transform.position.y < gameObject.transform.position.y)
        {
            TargetObj = null;
            HideTongue();
        }
        //move toward targetcookie
        else
        {
            var vectorTo = TargetObj.gameObject.transform.position - gameObject.transform.position;
            ShowTongue();
            Tongue.Destination = vectorTo;

            vectorTo.Normalize();

            if (FakeVelocity.sqrMagnitude < MaxSpeed * MaxSpeed)
            {
                //accelerate towards
                FakeVelocity += vectorTo * GrabAcceleration * Time.deltaTime;
            }
            else
            {
                //angle our velocity towards the target
                FakeVelocity = vectorTo * FakeVelocity.magnitude;
            }
        }

        //if we're past the side limits and heading the wrong way
        if ((gameObject.transform.position.x < LeftLimit && FakeVelocity.x < 0) ||
            (gameObject.transform.position.x > RightLimit && FakeVelocity.x > 0))
        {
            gameObject.transform.position = new Vector3(PHIUtil.Clamp(gameObject.transform.position.x, LeftLimit, RightLimit),
                gameObject.transform.position.y,
                gameObject.transform.position.z);

            //only flip velocity if we're not boosting
//            if (!Controls.FingerDown)
                FakeVelocity = new Vector3(FakeVelocity.x * -1, FakeVelocity.y, 0);
        }

        FakeVelocity = new Vector3(FakeVelocity.x, FakeVelocity.y, 0);
        base.Update();
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0);
		if (_smokeTrail != null)
	        _smokeTrail.transform.position = gameObject.transform.position + new Vector3(0, 0, .1f);
        Tongue.gameObject.transform.position = gameObject.transform.position + new Vector3(0, 0f, -2.5f);
        //OpenMouthFace.transform.position = ClosedMouthFace.transform.position = gameObject.transform.position + new Vector3(0, 0, -1f);
        //because update can get called out of sync with Main.Update, we need to tell main to update the camera position anytiime we move
        MainRef.UpdatePositions();
	}

    private void HideTongue()
    {
        Tongue.Showing = false;
        CloseMouth();
    }

    private void ShowTongue()
    {
        Tongue.Showing = true;
        OpenMouth();
    }

    public int Likes(Cookie cookie)
    {
        var likes = Likes(cookie.BaseColor, cookie.Shape);
        return likes;
    }


    public int Likes(Cookie.Color baseColor, Cookie.CookieShape shape)
    {
        var baseCount = (BaseColor == baseColor) ? 1 : 0;
        var shapeCount = (Shape == shape) ? 1 : 0;

        switch (Difficulty)
        {
            case ClimberMain.DifficultyLevel.Easy:
                return baseCount;
            
            case ClimberMain.DifficultyLevel.Hard:
                return shapeCount + baseCount;

            case ClimberMain.DifficultyLevel.Expert:
                if (shapeCount == 1 || baseCount == 1)
                    return 0;
                return 2;
        }

        return 0;
    }

    public bool JumpForCookie(GameObject go)
    {
		if (TargetObj != null)
			return false;
		
        _haveJumped = true;
        TargetObj = go;
        //zero out the horizontal motion
        FakeVelocity = new Vector3(0, FakeVelocity.y, 0);

        //new cookie, reset combo
        //_comboTotal = 0;

        return true;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        var otherFe = other.GetComponent<FightingEntity>();
        if (otherFe != null && otherFe.HitCategory != HitCategory)
        {
            if (otherFe.GetType() == typeof(Cookie))
            {
                var cookie = otherFe as Cookie;
                if (Likes(cookie) > 0)
                {
                    CookieSoundManager.PlayRandomCookieMunch();

                    if (GameClock.CurrTime < _nextComboExpiry)
                    {
                        _comboTotal++;
						MainRef.MakePointSplash(_comboTotal, gameObject.transform.position);
//                        LevelSpawner.MakePointSplash(_comboTotal, gameObject.transform.position);
                        if (_comboTotal == Prefs.GetBonusBoostLevel())
                        {
                            DoComboBoost();
                            _comboTotal = 0;
                        }
                    }
                    else
                        _comboTotal = 1;

                    _nextComboExpiry = GameClock.CurrTime + Prefs.GetComboTimeLimit(Difficulty);

                    cookie.HitPoints = 0;
                    Stamina += StaminaFill;// *Mathf.Max(1, _comboTotal);
                    Stamina = Math.Min(Stamina, MaxStamina);
					MainRef.MakeCrumbs(gameObject.transform.position, 1);

                    if (Booster != null && _comboTotal > 1 && !_boosting)
                    {
                        Booster.AddFuel(_comboTotal);
                    }
                }
            }
            else if (otherFe.GetType() == typeof(Zipper))
            {
                var zipper = otherFe as Zipper;
                zipper.HitPoints = 0;
				Stamina += zipper.Stamina;
                DoComboBoost();
				MainRef.MakeCrumbs(zipper.transform.position + new Vector3(0, 0, 1), 4);
            }
            else if (otherFe.GetType() == typeof(Coin))
            {
                CookieSoundManager.PlayCoinSound();

                var coin = otherFe as Coin;
                Prefs.Money += coin.CoinValue;
                Prefs.LifetimeMoney += coin.CoinValue;
                Prefs.MoneyThisLevel += coin.CoinValue;
                coin.HitPoints = 0;
                //play a sound
            }
            else if (otherFe.GetType() == typeof(StaminaSnack))
            {
                var snack = otherFe as StaminaSnack;
                Stamina += snack.Stamina;
                snack.HitPoints = 0;
				MainRef.MakeCrumbs(snack.transform.position + new Vector3(0, 0, 1), 4);
            }
/*MIGRATE TO PIE_SLICE            else if (otherFe.GetType() == typeof(MinigameToken))
            {
                var token = otherFe as MinigameToken;
                //Stamina = MaxStamina;
                token.HitPoints = 0;
                var crumbs = GameObject.Instantiate(Resources.Load("Cookies/CookieCrumbsPrefab")) as GameObject;
                crumbs.transform.position = token.transform.position + new Vector3(0, 0, 1);
            }*/
            else if (otherFe.GetType() == typeof(Asteroid))
            {
                var asteroid = otherFe as Asteroid;
                FakeVelocity = FakeVelocity / 2;
                var damage = asteroid.DamagePoints - UpgradeManager.GetUpgradeValue(UpgradeType.BonkProofing);
               // Debug.Log("Caused " + damage + " damage");
                Stamina -= damage;
                asteroid.HitPoints = 0;
				MainRef.MakeAsteroidSmash(gameObject.transform.position);
                CookieSoundManager.PlayAsteroidSmash();
            }
            else if (otherFe.GetType() == typeof(Trigger))
            {
                var trigger = otherFe as Trigger;
                trigger.HitPoints = 0;
                if (trigger.Callback != null)
                    trigger.Callback();
            }
        }
    }
	
    private void DoComboBoost()
    {
        CookieSoundManager.PlayBonusBoostSound();
        var comboBoostFireworks = LevelSpawner.MakeComboBoost();
        comboBoostFireworks.transform.position = transform.position;
        FakeVelocity = new Vector3(FakeVelocity.x / 2, Prefs.ZipperSpeed, 0);

        TargetObj = null;
    }

    public void FlyOffScreen()
    {
        _flyOffScreen = true;
        _stopTime = Time.time + 10f;
        SetSmokeTrailShowing(true);
        HideTongue();
    }

    public void BoosterClick()
    {
        if (_boosting)
            return;

        var boostSeconds = Booster.Click();
        _boosterEndTime = GameClock.CurrTime + boostSeconds;
        _boosting = true;
        _comboTotal = 0;
        SetSmokeTrailShowing(true);
        HideTongue();
    }

    private void SetSmokeTrailShowing(bool showing)
    {
        _smokeTrail.GetComponent<ParticleEmitter>().emit = showing;
    }
}
