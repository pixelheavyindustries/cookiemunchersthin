using UnityEngine;
using System.Collections;

public class CarrotControls : MonoBehaviour {

    //we set this, and let the entity pick it up and use it how it wants
    public Vector3 CourseCorrection;

    //WARNING: SteerOnlyWhenFingerDown = false, VerticalScroller = false are both untested 

    public bool VerticalScroller = true;

    public Vector3 FingerPosition;
    public bool FingerDown = false;
    public bool SteerOnlyWhenFingerDown = true;

    public float CarrotDistance = 10;
    public float MaxCarrotOffset = 10;
    public float _carrotPosition;

    public float MaxCarrotMoveSpeed = 2;

    //normally, the ship will steer with a true speed relative to its forward speed
    //to make it steer faster, set ratio to something more than 1.  To make it steer slower, set to less than 1
    public float SidewaysForwardRatio = 1;

    public FightingEntity Entity;

	// Use this for initialization
	void Start () {
        CourseCorrection = new Vector3();
	}
	
	// Update is called once per frame
	void Update () {
        if (Entity == null)
        {
            return;
        }

        if (!FingerDown && SteerOnlyWhenFingerDown)
            return;

        if (FingerDown && FingerPosition != null)
        {
            _carrotPosition = VerticalScroller ? FingerPosition.x : FingerPosition.y;
        }

        var distance = VerticalScroller ?   _carrotPosition - Entity.gameObject.transform.position.x :
                                            _carrotPosition - Entity.gameObject.transform.position.y;

        //limit distance to max
        if (Mathf.Abs(distance) > MaxCarrotOffset)
            distance = MaxCarrotOffset * (distance / Mathf.Abs(distance));

        var velocity = VerticalScroller ? Entity.FakeVelocity.y : Entity.FakeVelocity.x;
        velocity *= SidewaysForwardRatio;
        velocity *= distance / CarrotDistance;
        CourseCorrection = VerticalScroller ? new Vector3(velocity, 0, 0) : new Vector3(0, velocity, 0);
		//print ("Course Correction: " + FingerPosition + "    " + CourseCorrection + "   " + Entity.FakeVelocity + "    " + _carrotPosition + "   " + distance);
		
		Entity.transform.position += CourseCorrection;
	}
}
