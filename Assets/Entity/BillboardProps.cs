using UnityEngine;
using System.Collections;

public class BillboardProps : MonoBehaviour {

    public const float defaultWidthMeters = 2f;
    public Vector2 OriginalDimsInMeters;
	
	//rotation stuff, off by default
    public float RotationTime = 0.0f;
    public Vector3 RotationAxis = Vector3.forward;
    private float _startTime;
	

    public Vector2 GetDimensionsInMeters()
    {
        return new Vector2(
            gameObject.transform.localScale.x * OriginalDimsInMeters.x,
            gameObject.transform.localScale.y * OriginalDimsInMeters.y);
    }

    public void SetDimensionsInMeters(float width, float height)
    {
        gameObject.transform.localScale = new Vector3(width / OriginalDimsInMeters.x, height / OriginalDimsInMeters.y, 1f);
    }

    public void SetWidthInMeters(float width)
    {
        var newXScale = width / OriginalDimsInMeters.x;
        var scaleChange = newXScale / gameObject.transform.localScale.x;
        var newYScale = gameObject.transform.localScale.y * scaleChange;
        gameObject.transform.localScale = new Vector3(newXScale, newYScale, 1);
    }

    public void SetHeightInMeters(float height)
    {
        var newYScale = height / OriginalDimsInMeters.y;
        var scaleChange = newYScale / gameObject.transform.localScale.y;
        var newXScale = gameObject.transform.localScale.x * scaleChange;
        gameObject.transform.localScale = new Vector3(newXScale, newYScale, 1);
    }

    // Use this for initialization
	void Start () {
		_startTime = Random.Range(0f, 100f);
	}
	
	// Update is called once per frame
	void Update () {
		if (RotationTime > 0)
		{
	        var timeFactor = ((Time.time - _startTime) % RotationTime) / RotationTime;
	        gameObject.transform.rotation = Quaternion.AngleAxis(360 * timeFactor, RotationAxis);
		}
	}
}
