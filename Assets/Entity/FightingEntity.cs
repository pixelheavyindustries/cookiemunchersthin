using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class FightingEntity : MonoBehaviour
{
    public enum HitCheckCategory
    {
        NoCheck,
        Goodguy,
        Badguy,
        BadguyShrapnel,
    }

    public Guid Id;

    public bool Dead = false;
    public float Lifespan;
    private float _startTime;
    public DeadReplacement DeadReplacement;
    public DeadReplacement HitDebris;
    public float HitDebrisChance = 1;
    public HitCheckCategory HitCategory = HitCheckCategory.NoCheck;

	public List<GameObject> Attachments;
	
    //sometimes we don't want to bother with attaching a Rigidbody, this allows us to still set a velocity
    public Vector3 FakeVelocity;

    //for billboarding (flat surfaces that always face camera)
    public bool Billboard = false;
    public float BillboardAngle;

    //if we receive damage less than this, ignore the damage and leave hitpoints the same
    public int DamageFloor;

    private int _hitPoints = 1;
    public int HitPoints
    {
        get
        {
            return _hitPoints;
        }
        set
        {
            //always increase, only decrease when damage is greater than damageFloor
            if (value > _hitPoints || _hitPoints - value >= DamageFloor)
                _hitPoints = value;
        }
    }

    public int DamagePoints;

//    public WeaponBase Gun;
//    public WeaponBase Special1;
//    public WeaponBase Special2;

    public AudioClip ImpactSound;
    public float ImpactSoundMaxPitchShift;
    public AudioClip DieSound;
    public float DieSoundMaxDoppler;
	
	public FightingEntity()
	{
		Attachments = new List<GameObject>();
	}
	
    // Use this for initialization
    public void Start()
    {
        Id = Guid.NewGuid();
        _startTime = GameClock.CurrTime;
    }

    // Update is called once per frame
    public void Update()
    {
        GetComponent<Rigidbody>().WakeUp();

        if (!Dead)
        {
            if (Lifespan > 0 && _startTime + Lifespan < GameClock.CurrTime)
                Dead = true;
            else if (HitPoints <= 0)
                Dead = true;
        }

        if (FakeVelocity.sqrMagnitude > 0 && gameObject != null)
        {
            transform.position = transform.position + FakeVelocity * Time.deltaTime;
        }

        //if we're billboarding the object, face it towards camera and then rotate it by BillboardAngle
        if (Billboard)
        {
            transform.LookAt(Camera.main.transform);
            transform.Rotate(transform.position - Camera.main.transform.position, BillboardAngle);
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        var otherFe = other.GetComponent<FightingEntity>();
        if (otherFe != null && otherFe.HitCategory != HitCategory)
        {
            HitPoints -= otherFe.DamagePoints;
            if (HitPoints > 0 && HitDebris != null && Random.Range(0f, 1f) <= HitDebrisChance)
            {
                var debris = HitDebris.Replacement();
                debris.gameObject.transform.position = gameObject.transform.position + HitDebris.Offset;
            }

            if (ImpactSound != null)
                SoundManager.Play(ImpactSound, Camera.main.transform.position, 1.0f, Random.Range(1.0f, ImpactSoundMaxPitchShift));
        }
    }

    protected virtual void OnTriggerStay(Collider other)
    {
    }


    public override bool Equals(object o)
    {
        if (!(o is FightingEntity))
        {
//            print("o is not a FightingEntity: " + o.GetType());
            return false;
        }

        return ((FightingEntity)o).Id == Id;
    }

    public override int GetHashCode()
    {
        return Id.ToString().GetHashCode();
    }
	
	public void AddAttachment(GameObject attachment)
	{
		Attachments.Add(attachment);
	}
	
	public virtual void OnDestroy()
	{
		foreach (var a in Attachments)
			Destroy(a);
	}
	
}
