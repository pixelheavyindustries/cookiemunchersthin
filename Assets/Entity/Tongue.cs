using UnityEngine;
using System.Collections;

public class Tongue : MonoBehaviour {

    public bool Showing = false;
    public Vector3 Destination;
    private GameObject _tongueObj;
    private BillboardProps _billboard;
    private GameObject _locationSource;

    public void Init(GameObject tongue, GameObject locationSource)
    {
        _tongueObj = tongue;
        _billboard = _tongueObj.GetComponent<BillboardProps>();
        _locationSource = locationSource;
    }
	
	// Update is called once per frame
	void Update () {
        PHIUtil.SetChildVisibility(gameObject, Showing, true);

        if (Showing)
        {
            _tongueObj.transform.position = _locationSource.transform.position + Destination / 2 + new Vector3(0, -.6f, -1.0f);
            var angle = Mathf.Atan2(Destination.x, -1f * Destination.y) * Mathf.Rad2Deg + 180;
            _tongueObj.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            _billboard.SetDimensionsInMeters(.6f, Destination.magnitude);
        }
	}
}
