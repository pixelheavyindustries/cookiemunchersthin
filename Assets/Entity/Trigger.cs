using System;
using UnityEngine;
using System.Collections;

public class Trigger : FightingEntity {

    public Func<bool> Callback;


    public void OnTriggerExit()
    {
        Destroy(this.gameObject);
    }
}
