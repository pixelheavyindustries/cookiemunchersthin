using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class ParticleSource : MonoBehaviour
{

    public Vector3 SpawnBox;
    public List<Func<GameObject>> Particles;
    public float Lifespan = 0;
    public float SpawnRate = 10;
    private float _lastSpawnTime = -100000f;

    public float MinSpeed;
    public float MaxSpeed;

    //together, these define the cone in which new particles can go
    //Vector3.up and 20 would send particles upwards in a 40 degree cone (up to 20 degrees off of up)
    //any vector and 180 will spawn particles in a totally random direction
    public Vector3 VelocityAxis;
    public float VelocityCone;

	void Start () {
	    if (Lifespan > 0)
            Destroy(this, Lifespan);
	}
	
	void Update () {
/*	    if (Time.time - _lastSpawnTime > 1/SpawnRate)
	    {
	        _lastSpawnTime = Time.time;
	        var particleDelegate = Particles[Random.Range(0, Particles.Count - 1)];
	        var particle = particleDelegate();
	        particle.transform.position = transform.position +
	                                      new Vector3(Random.Range(-1f * SpawnBox.x, SpawnBox.x),
                                                      Random.Range(-1f * SpawnBox.y, SpawnBox.y),
                                                      Random.Range(-1f * SpawnBox.z, SpawnBox.z));

            var vec = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f);
            vec.Normalize();
	        vec = vec*Random.Range(MinSpeed, MaxSpeed);

            if (transform.parent != null)
            {
                var parentFeScript = transform.parent.GetComponent<FightingEntity>();
                if (parentFeScript != null)
                    vec += parentFeScript.FakeVelocity;
            }

	        particle.GetComponent<FightingEntity>().FakeVelocity = vec;
	    }*/
	}
}
