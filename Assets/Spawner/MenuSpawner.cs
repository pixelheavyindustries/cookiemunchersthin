using UnityEngine;
using System.Collections;
using System;

public class MenuSpawner : SpawnerBase
{
    public static GameObject Button(string id, string text, Vector2 position, Action<string> callback)
    {
        var btn = MakeBillboard(ShaderTypeAlphaBlended, "Menus/Button", 75, true, 1, 1);
        btn.name = id + "_gameobject";

        var menuButtonScript = btn.AddComponent<MenuButton>();
        menuButtonScript.Id = id;
        menuButtonScript.ClickCallback = callback;

        AddBoxCollider(btn, new Vector3(4, 2, .01f));

        var textMesh = MakeTextMesh();
        textMesh.text = text;
        textMesh.transform.parent = btn.transform;
        textMesh.transform.localScale = new Vector3(.3f, .3f, .3f);

        PositionElement(position, btn);

        return btn;
    }

    public static GameObject Button(string texturePath, Vector2 position, Action<string> callback)
    {
        var btn = MakeBillboard(ShaderTypeAlphaBlended, texturePath, 25, true, 1, 1);
        btn.name = texturePath + "_gameobject";
        var menuButtonScript = btn.AddComponent<MenuButton>();
        menuButtonScript.Id = texturePath;
        menuButtonScript.ClickCallback = callback;

        AddBoxCollider(btn, new Vector3(14, 7, .01f));

        PositionElement(position, btn);

        return btn;
    }

    public static GameObject LockableButton(string texturePath, bool locked, Vector2 position, Action<string> callback)
    {
        var btn = Button(texturePath, position, callback);

        if (locked)
        {
            var lockObj = MakeBillboard(ShaderTypeAlphaBlended, "Menus/Lock", 100, true, 1, 1);
            var bbProps = lockObj.GetComponent<BillboardProps>();
            bbProps.SetWidthInMeters(9);
            lockObj.transform.position = btn.transform.position + new Vector3(0, 0, -.1f);
            lockObj.transform.parent = btn.transform;
        }

        return btn;
    }

    public static bool DrawBackButton()
    {
		GUI.depth = RenderDepths.DIALOG_OVERLAY;
        var size = ScreenDimensions.GameAreaInPixels.width / 6f;
        var choice = GUI.Button(new Rect(0, ScreenDimensions.GameAreaInPixels.yMin, size, size), 
            TextureCache.GetCachedTexture("Menus/BackButton"), 
            PHIUtil.MakeButtonGUIStyle());
		GUI.depth = 0;
		return choice;
    }

    public static GameObject BackButton(string id, Vector2 position, Action<string> callback)
    {
        var btn = MakeBillboard(ShaderTypeAlphaBlended, "Menus/BackButton", 75, true, 1, 1);
        btn.name = id + "_gameobject";
        var menuButtonScript = btn.AddComponent<MenuButton>();
        menuButtonScript.Id = id;
        menuButtonScript.ClickCallback = callback;

        AddBoxCollider(btn, new Vector3(2, 2, .01f));

        PositionElement(position, btn);

        return btn;
    }

    public static GameObject UpArrow(string id, Vector2 position, Action<string> callback)
    {
        var btn = MakeBillboard(ShaderTypeAlphaBlended, "Menus/UpArrow", 120, true, 1, 1);
        btn.name = id + "_gameobject";
        var menuButtonScript = btn.AddComponent<MenuButton>();
        menuButtonScript.Id = id;
        menuButtonScript.ClickCallback = callback;

        AddBoxCollider(btn, new Vector3(2, 2, .01f));

        PositionElement(position, btn);

        return btn;
    }
    public static GameObject DownArrow(string id, Vector2 position, Action<string> callback)
    {
        var btn = MakeBillboard(ShaderTypeAlphaBlended, "Menus/DownArrow", 120, true, 1, 1);
        btn.name = id + "_gameobject";
        var menuButtonScript = btn.AddComponent<MenuButton>();
        menuButtonScript.Id = id;
        menuButtonScript.ClickCallback = callback;

        AddBoxCollider(btn, new Vector3(2, 2, .01f));

        PositionElement(position, btn);

        return btn;
    }

    public static void PositionElement(Vector2 position, GameObject element)
    {
        var x = (-.5f + position.x) * ScreenDimensions.GameAreaInMeters.width;

        //unity's 3d space has positive y on top, negative on bottom.  Flip it around so it's more like Flash's pixel coord' system
        var y = (.5f + -1 * position.y) * ScreenDimensions.GameAreaInMeters.height;
        element.transform.position = new Vector3(x, y, 0);
		var menuElementScript = element.GetComponent<MenuElement>();
		menuElementScript.HomePosition = element.transform.position;
    }


}
