using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class SpawnerBase : MonoBehaviour
{
    private static Dictionary<String, Texture2D> _textureCache;
    private static Dictionary<String, UnityEngine.Object> _shaderCache;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

    public static FillBar MakeFillBar(Rect size, Texture labelTexture = null)
    {
        var go = new GameObject("FillBar");
        var bar = go.AddComponent<FillBar>();
        bar.BaseTexture = TextureCache.GetCachedTexture("Gui/FillBarBase");
        bar.FlashBaseTexture = TextureCache.GetCachedTexture("Gui/FillBarFlashBase");
        bar.FillTexture = TextureCache.GetCachedTexture("Gui/FillBarFill");
        bar.LabelTexture = labelTexture;
        bar.OverlayTexture = TextureCache.GetCachedTexture("Gui/FillBarOverlay");
		bar.CookieChunkTexture = TextureCache.GetCachedTexture("Cookies/CrumbsFlying1");
        bar.Size = size;
        bar.FillOffset = new Vector2(33, 28);
        bar.MaxFillWidth = 450f;
        bar.FillTextureHeight = 40;
        return bar;
    }

    #region UtilityFunctions

    public static TextMesh MakeTextMesh()
    {
        var textMeshObject = (GameObject)Instantiate(Resources.Load("Menus/MenuFont"));
        var textMesh = textMeshObject.GetComponent<TextMesh>();
        textMesh.font = (Font)Resources.Load("Fonts/GILLUBCD");
        textMesh.offsetZ = 0;

        return textMesh;
    }

    public static GameObject MakeBillboard(string shaderType, string texturePath, float pixelsPerMeter, bool repeat, int totalFrames, float duration)
    {
        var frames = new List<int>();
        for (int i=0; i<totalFrames; i++) frames.Add(i);
        if (!repeat)
            frames.Add(-1);

        return MakeBillboardInternal(shaderType, texturePath, pixelsPerMeter, frames.ToArray(), totalFrames, duration);
    }

/*    public static GameObject MakeBillboard(string texturePath, float pixelsPerMeter, int[] frames, int totalFrames, float duration)
    {
        var go = new GameObject("Billboard");
        go.AddComponent<MeshRenderer>();
        var meshFilter = go.AddComponent<MeshFilter>();
        var mesh = meshFilter.mesh;
        mesh.Clear();

        var tex = Resources.Load(texturePath) as Texture2D;
        var surfaceWidth = tex.width / (totalFrames * pixelsPerMeter);
        var surfaceHeight = tex.height / pixelsPerMeter;
        mesh.vertices = new Vector3[]
                            {
                                new Vector3(0 - surfaceWidth / 2, 0 - surfaceHeight / 2, 0), 
                                new Vector3(0 - surfaceWidth / 2, surfaceHeight / 2, 0), 
                                new Vector3(surfaceWidth / 2, surfaceHeight / 2, 0), 
                                new Vector3(surfaceWidth / 2, 0 - surfaceHeight / 2, 0),
                            };

        mesh.triangles = new int[] { 0, 1, 2, 0, 2, 3 };
        mesh.uv = new Vector2[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0), };
        mesh.RecalculateNormals();

        var renderer = go.GetComponent<Renderer>();
        renderer.castShadows = false;
        renderer.receiveShadows = false;
        renderer.material.shader = Shader.Find("Particles/Additive");
        renderer.material.mainTexture = tex;

        var animation = go.AddComponent<BillboardAnimation>();
        animation.Frames = frames;
        animation.TotalFrames = totalFrames;
        animation.Duration = duration;

        return go;
    }*/

    public static string ShaderTypeAdditive = "Self-Illum/Diffuse";
    public static string ShaderTypeAlphaBlended = "Mobile/Particles/Alpha Blended";

    private static GameObject MakeBillboardInternal(string shaderType, string texturePath, float pixelsPerMeter, int[] frames, int totalFrames, float duration)
    {
        if (String.IsNullOrEmpty(shaderType))
            shaderType = ShaderTypeAlphaBlended;

        Texture2D tex = TextureCache.GetCachedTexture(texturePath);

		float size = 1f;
		Mesh m = new Mesh();
		m.name = "Scripted_Plane_New_Mesh";
        var surfaceWidthScale = (float)tex.width / (totalFrames * pixelsPerMeter);
        var surfaceHeightScale = (float)tex.height / pixelsPerMeter;
		m.vertices = new Vector3[] {new Vector3(-surfaceWidthScale, -surfaceHeightScale, 0.01f), 
									new Vector3(surfaceWidthScale, -surfaceHeightScale, 0.01f), 
									new Vector3(surfaceWidthScale, surfaceHeightScale, 0.01f), 
									new Vector3(-surfaceWidthScale, surfaceHeightScale, 0.01f) };
		m.uv = new Vector2[] {new Vector2 (0f, 0f), new Vector2(1f, 0f), new Vector2 (1f, 1f), new Vector2 (0f, 1f)};
		m.triangles = new int[] {3, 2, 1, 0, 3, 1};
		m.RecalculateNormals();
		
		GameObject go = new GameObject("New_Plane_Fom_Script");
		go.AddComponent<MeshFilter>();
		go.GetComponent<MeshFilter>().mesh = m;		
		go.AddComponent<MeshRenderer>();
		go.GetComponent<MeshRenderer>().renderer.material.shader = Shader.Find (shaderType);
        var billboardProps = go.AddComponent<BillboardProps>();
        go.GetComponent<MeshRenderer>().renderer.material.mainTexture = tex;
			
        billboardProps.OriginalDimsInMeters = new Vector2(BillboardProps.defaultWidthMeters * surfaceWidthScale, 
                                                        BillboardProps.defaultWidthMeters * surfaceHeightScale);

        return go;
    }

    public static GameObject MakeLayeredObject(string shaderType, String[] layerPaths, float pixelsPerMeter, bool repeat, int totalFrames, float duration)
    { 
        var frames = new List<int>();
        for (int i=0; i<totalFrames; i++) frames.Add(i);
        if (!repeat)
            frames.Add(-1);

        return MakeLayeredObject(shaderType, layerPaths, pixelsPerMeter, frames.ToArray(), totalFrames, duration);
    }

    public static GameObject MakeLayeredObject(string shaderType, String[] layerPaths, float pixelsPerMeter, int[] frames, int totalFrames, float duration)
    {
        var go = new GameObject("LayeredObject");

        for (int i = 0; i < layerPaths.Length; i++)
        {
            var layer = MakeBillboardInternal(shaderType, layerPaths[i], pixelsPerMeter, frames, totalFrames, duration);
            layer.transform.parent = go.transform;
            layer.transform.position = new Vector3(0, 0, (float)i * -0.02f);
        }

        return go;
    }

    public static void AddBoxCollider(GameObject go, Vector3 dims)
    {
        var bc = go.AddComponent<BoxCollider>();

        //all colliders are going to have to be triggers, because kinematic colliders will not send collision messages from hitting other kinematic colliders http://unity3d.com/support/documentation/Manual/Physics.html (matrix at bottom of page)
        bc.isTrigger = true;
        bc.size = dims;
    }

    public static void AddSphereCollider(GameObject go, float radius)
    {
        var sc = go.AddComponent<SphereCollider>();
        //all colliders are going to have to be triggers, because kinematic colliders will not send collision messages from hitting other kinematic colliders http://unity3d.com/support/documentation/Manual/Physics.html (matrix at bottom of page)
        sc.isTrigger = true;
        sc.radius = radius;
    }


    #endregion
}
