using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogSpawner : MonoBehaviour {

    public static bool DialogShowing { get; private set; }
    protected static List<DialogDef> _pages;
    protected static int _currPage;
	protected static DialogDef _currDialogDef { 
		get { 
			if (_pages == null || _currPage < 0 || _currPage >= _pages.Count)
				{ return null; }
			
			return _pages[_currPage];
		}
	}
	
	private static float buttonsBottom = 0.9f;
	private static float buttonHeight = .141f;


    //if we want to not activate the close or next button for x seconds after dialog opens, we'll use this
    protected static float _dialogStartTime = 0;

    public static void ShowDialog(DialogDef dialog)
    {
        ShowDialogs(new List<DialogDef> { dialog });
    }

    public static void ShowDialogs(List<DialogDef> pages)
    {
        if (DialogShowing)
            return; 

        GameClock.Paused = true;

        DialogShowing = true;
        _pages = pages;
        _currPage = -1;
        Next();
    }

    public static void Next()
    {
        _dialogStartTime = Time.time;
        _currPage++;
        if (_currDialogDef == null)
        {
            GameClock.Paused = false;
            return;
        }
		
		//now RenderCurrentDialog will get called on every OnGUI of the current MenuScreen or level
    }
	
	public static void RenderCurrentDialog()
	{
		var def = _currDialogDef;
		if (def == null)
			return;
		
        var position = def.Rect.GetValueOrDefault(new Rect(-400, 0, Screen.width + 800, Screen.height));
        var texture = def.Background ?? TextureCache.GetCachedTexture("Gui/Dialog/DialogBackground");
		
        if (texture == null)
            return;

        GUI.DrawTexture(position, texture, ScaleMode.ScaleToFit);
        def.RenderContents(def);
		
	}
	
    public static void CloseCurrent(float minAge = 0f)
    {
        //print("Time.time: " + (Time.time -_dialogStartTime) + "    " + minAge);
        if (Time.time - _dialogStartTime > minAge)
        {
            InternalClose();
            Next();
        }
    }

    public static void CloseAll()
    {
        InternalClose();
    }

    protected static void InternalClose()
    {
        DialogShowing = false;
		_pages = null;
	}
	

/*    protected static Dialog InternalMakeDialog(int id, DialogDef def)
    {
        var go = new GameObject("Dialog" + id);
        var dialog = go.AddComponent<Dialog>();

        dialog.Init(id, def);

        return dialog;
    }*/


    public static void MakePauseDialog()
    {
        ShowDialog(new DialogDef { RenderContents = RenderPause });
    }

    public static bool RenderPause(DialogDef def)
    {
        RenderDialogTitle("Gui/Dialog/Paused");

        var result = RenderDialogBottomNew("Gui/Dialog/Quit", "Gui/Dialog/Resume");
        if (result == 1)
        {
            CloseCurrent();
            Application.LoadLevel("MenuMain");
        }
        else if (result == 2)
        {
            CloseCurrent();
            ClimberMain.Instance.PauseDialogUnpause();
        }

        return true;
    }

    public static void MakeLoseDialogEndlessRecord()
    {
        ShowDialog(new DialogDef { RenderContents = RenderLoseEndlessRecord, Rect = new Rect(-400, 0, Screen.width + 800, Screen.height) });
    }

    public static void MakeLoseDialogEndless()
    {
        ShowDialog(new DialogDef { RenderContents = RenderLoseEndless, Rect = new Rect(-400, 0, Screen.width + 800, Screen.height) });
    }

    public static void MakeLoseDialogHungry()
    {
        ShowDialog(new DialogDef { RenderContents = RenderLoseHungry, Rect = new Rect(-400, 0, Screen.width + 800, Screen.height) });
    }

    public static void MakeLoseDialogFall()
    {
        ShowDialog(new DialogDef { RenderContents = RenderLoseOhNoes, Rect = new Rect(-400, 0, Screen.width + 800, Screen.height) });
    }

    public static bool RenderLoseEndlessRecord(DialogDef def)
    {
        RenderDialogTitle("Gui/Dialog/EndlessOhNoes");
        RenderUpgradesQuitRetry();

        var textStyle = PHIUtil.GetLargeTextStyle();
        textStyle.alignment = TextAnchor.MiddleCenter;
        var textRectWidth = Screen.height / 3f;   //dialog interior width is approx 1/3 of height
        var textRect = new Rect((Screen.width - textRectWidth) / 2f, Screen.height * .3f, textRectWidth, Screen.height * .17f);
		var height = ClimberMain.Instance.TotalMissionDistance;
        GUI.Label(textRect, String.Format("New Record:\n{0} Meters!", PHIUtil.RoundToPlaces(height, 0)), textStyle);

        return true;
    }

    public static bool RenderLoseEndless(DialogDef def)
    {
        RenderDialogTitle("Gui/Dialog/EndlessOhNoes");
        RenderUpgradesQuitRetry();

        var textStyle = PHIUtil.GetLargeTextStyle();
        textStyle.alignment = TextAnchor.MiddleCenter;
        var textRectWidth = Screen.height / 3f;   //dialog interior width is approx 1/3 of height
        var textRect = new Rect((Screen.width - textRectWidth) / 2f, Screen.height * .3f, textRectWidth, Screen.height * .17f);
		var height = ClimberMain.Instance.TotalMissionDistance;
        GUI.Label(textRect, String.Format("You made it to\n{0} Meters!", PHIUtil.RoundToPlaces(height, 0)), textStyle);

        return true;
    }

    public static bool RenderLoseHungry(DialogDef def)
    {
        RenderDialogTitle("Gui/Dialog/TooMuchHungry");
        RenderUpgradesQuitRetry();

        return true;
    }

    public static bool RenderLoseOhNoes(DialogDef def)
    {
        RenderDialogTitle("Gui/Dialog/OhNoes");
        RenderUpgradesQuitRetry();

        return true;
    }

    public static void MakeWinDialog()
    {
        ShowDialog(new DialogDef { RenderContents = RenderWin, Rect = new Rect(-400, 0, Screen.width + 800, Screen.height) });
    }

    public static bool RenderWin(DialogDef def)
    {
        var diff = ClimberMain.Instance.Difficulty;

        RenderDialogTitle("Gui/Dialog/NomNomNom");
		
        var choice = RenderDialogBottomNew("Gui/Dialog/Quit", "Gui/Dialog/Next");
		if (choice == 1)
		{
            CloseCurrent();
			MenuManager.PopLast();
            Application.LoadLevel("MenuMain");
		}
		else if (choice == 2)
		{
            CloseCurrent();
            Application.LoadLevel("MenuMain");
		}
		
		
        //draw the level time
        var textStyle = PHIUtil.GetLargeTextStyle();
        textStyle.alignment = TextAnchor.MiddleCenter; 
        var textRectWidth = Screen.height / 3f;   //dialog interior width is approx 1/3 of height
        var textRect = new Rect((Screen.width - textRectWidth) / 2f, Screen.height * 2.5f / 10f, textRectWidth, Screen.height / 10f);
        GUI.Label(textRect, String.Format("{0} Seconds!", PHIUtil.RoundToPlaces(ClimberMain.Instance.TotalMissionTime, 1)), textStyle);

        //draw the stars
        var starOffTex = TextureCache.GetCachedTexture("Gui/Dialog/WinDialogStarOff") as Texture2D;
        var starOnTex = TextureCache.GetCachedTexture("Gui/Dialog/WinDialogStarOn") as Texture2D;
        var starScore = Prefs.GetLastStarScore();
		
		//Debug.Log ("Got Star Score: " + starScore + "   Diff: " + diff + "   " + Prefs.GetMissionLevel(diff));
        var starHeight = Screen.height / 8f;
        var starWidth = starHeight;
        var starsWidth = starHeight * 3f;
        var starRect = new Rect((Screen.width - starsWidth) / 2f, Screen.height * 3.5f / 10f, starWidth, starHeight);
        GUI.DrawTexture(starRect, starScore >= 1 ? starOnTex : starOffTex);
        starRect.x += starWidth;
        GUI.DrawTexture(starRect, starScore >= 2 ? starOnTex : starOffTex);
        starRect.x += starWidth;
        GUI.DrawTexture(starRect, starScore >= 3 ? starOnTex : starOffTex);

        textRect.y = Screen.height * 5f / 10f;
        if (Prefs.MoneyThisLevel > 0)
            GUI.Label(textRect, String.Format("Found {0} {1}", Prefs.MoneyThisLevel, Prefs.MoneyThisLevel == 1 ? "coin" : "coins"), textStyle);

        return true;
    }



    public static void ShowResetDialog()
    {
        //print("Show reset dialog");
        ShowDialog(new DialogDef { RenderContents = RenderClearData });
    }

    public static bool RenderClearData(DialogDef def)
    {
        RenderDialogTitle("Menus/ClearAllData");
        var choice = RenderDialogBottomNew("Menus/ButtonOk", "Menus/Vending/Cancel");

        if (choice == 1)
        {
            DefaultSetter.SetDefaults();
            CloseCurrent();
        }
        else if (choice == 2)
            CloseCurrent();

        return true;
    }

    #region helpdialogs
    public static bool RenderFullScreenButton(DialogDef def)
    {
        var btnStyle = PHIUtil.MakeButtonGUIStyle();
        if (GUI.Button(new Rect(0, 0, Screen.width, Screen.height), "", btnStyle))
        {
            CloseCurrent(.8f);
        }

        return true;
    }


    #endregion

    public static void ShowVendingDialog()
    {
        //print("Show vending dialog");
        ShowDialog(new DialogDef { RenderContents = RenderVendingDialog });
    }

    public static void ShowVendingDialogGetMoreCoins()
    {
        //print("Show vending dialog");
        ShowDialog(new DialogDef { RenderContents = RenderVendingDialogGetMoreCoins });
    }

    public static void ShowVendingDialogCantAfford()
    {
        //print("Show vending dialog cant afford");
        ShowDialog(new DialogDef { RenderContents = RenderVendingDialogCantAfford });
    }
	
    public static bool RenderVendingDialog(DialogDef def)
	{
		return RenderVendingDialogInner(def, true, false);
	}
	
    public static bool RenderVendingDialogGetMoreCoins(DialogDef def)
	{
		return RenderVendingDialogInner(def, false, true);
	}
	
    public static bool RenderVendingDialogCantAfford(DialogDef def)
	{
		return RenderVendingDialogInner(def, false, false);
	}
	
    public static bool RenderVendingDialogInner(DialogDef def, bool canAfford, bool canGetCoins)
    {
        var rack = VendingMachine.CurrRack;
        if (rack == null)
            return true;
		
		var imageTop = 0.125f;
		var imageBottom = 0.367f;
		var descriptionBottomLimit = .5f;
		
        var medStyle = PHIUtil.MakeDialogTextStyle();
        medStyle.alignment = TextAnchor.UpperCenter;
        medStyle.font = PHIUtil.GetLargeFont();
		
        var largeStyle = PHIUtil.MakeDialogTextStyle();
        largeStyle.alignment = TextAnchor.LowerCenter;
        largeStyle.font = PHIUtil.GetLargeFont();
		
		var dialogRect = DialogSpawner.GetDialogContentRect();
		var height = Screen.height;
		
		//render image
		GUI.DrawTexture(new Rect(-1000, imageTop * height, Screen.width + 2000, (imageBottom - imageTop) * height), 
						Resources.Load (rack.GetImagePath()) as Texture, 
						ScaleMode.ScaleToFit);

		//render description
		var description = new GUIContent(rack.CurrDescription);
		var somePadding = Screen.height * .02f;
		var rect = new Rect(dialogRect.x + somePadding, imageBottom * height, dialogRect.width - somePadding * 2, (descriptionBottomLimit - imageBottom) * height);
		GUI.Label (rect, description, medStyle);
		
		var price = new GUIContent(String.Format ("{0} Coins", rack.CurrCost));
		GUI.Label (new Rect(dialogRect.x, 0f, dialogRect.width, (buttonsBottom - buttonHeight*2f) * height), price, largeStyle);

        int choice = 0;
		var buyButtonPath = "Menus/Vending/Buy";
		if (!canAfford && canGetCoins)
			buyButtonPath = "Menus/Vending/GetMoreCoins";
		if (!canAfford && !canGetCoins)
			buyButtonPath = "Menus/Vending/BuyDisabled";
        choice = RenderDialogBottomNew(buyButtonPath, "Menus/Vending/Cancel");
		
		//they haven't had time to click an actual button, so discard any input as a leftover click
		if (Time.time - _dialogStartTime < .25f)
			choice = 0;

        if (choice == 1)
        {
			if (canAfford)
			{
	            rack.Dispense();
    	        CloseCurrent();
			}
			else if(canGetCoins)
			{
				Debug.Log ("Showing coin purchase dialog");
				CloseCurrent();
				
				String productId;
				//IOS
				productId = IOSInAppPurchasingManager.CoinPack1000;
				//end IOS
				
				DialogSpawner.ShowCoinPurchasingDialog(productId);
			}
        }
        else if (choice == 2)
            CloseCurrent();
		
        return true;
    }
	
	public static void ShowCoinPurchasingDialog(String productId)
	{
		Debug.Log ("Calling restartload from showCoinPurchasingDialog");
		//if IOS
		IOSInAppPurchasingManager.RestartLoad();
		//end IOS
        ShowDialog(new DialogDef { RenderContents = RenderCoinPurchasingDialog, ExtraData = productId });
	}
	
	public static bool RenderCoinPurchasingDialog(DialogDef def)
	{
		//if (GUI.Button (new Rect(300, 240, 100, 30), "Retrieve Products"))
		//	IOSInAppPurchasingManager.PurchaseProduct("com.pixelheavyindustries.cookiemunchers.coinpacka");
		
		List<InAppPurchasingManager.ProductDetails> productList;
		String productId = def.ExtraData;
		InAppPurchasingManager.Status status;
		
		//if IOS
		status = IOSInAppPurchasingManager.SessionStatus;
		productList = IOSInAppPurchasingManager.AllProducts;
		//end if
		
		
		InAppPurchasingManager.ProductDetails product = null;
		for (var i=0; i<productList.Count; i++)
		{
			if (productList[i].ID == productId)
				product = productList[i];
		}
		
		var dialogRect = GetDialogContentRect();
	    var style = PHIUtil.MakeDialogTextStyle();
		var errorStyle = new GUIStyle(style);
		var errorCode = "EC" + ((int)InAppPurchasingManager.SessionStatus).ToString() + "-" + 
			(InAppPurchasingManager.CanPurchaseDetermined ? "1" : "0") + 
			(InAppPurchasingManager.CanPurchase ? "1" : "0") + 
			(InAppPurchasingManager.AreProductsRetrieved ? "1" : "0") + 
			InAppPurchasingManager.NumLoadTries.ToString() +
			(product == null ? "0" : "1");
		errorStyle.font = PHIUtil.GetMediumFont();
		int choice;
		var textRect = new Rect(dialogRect.xMin + dialogRect.width*.1f, 
								dialogRect.yMin + dialogRect.height*.2f, 
								dialogRect.width * .8f, 
								dialogRect.height * .3f);
				
		switch(status)
		{
		case InAppPurchasingManager.Status.Uninitialized:
		case InAppPurchasingManager.Status.LoadingProducts:
			GUI.Label (textRect, "Retrieving product list...", style);
			choice = RenderDialogBottomNew("", "Menus/Vending/Cancel");
			if (choice == 2)
			{
				CloseAll();
			}
			break;
			
		case InAppPurchasingManager.Status.LoadingProductsFailed:
			GUI.Label (textRect, "Error communicating with server.", style);
			GUI.Label (new Rect(Screen.width * .15f, textRect.yMax + 20, Screen.width*.15f, 100f), "Please check internet connection.\n" + InAppPurchasingManager.Error + "\n" + errorCode, errorStyle);
			choice = RenderDialogBottomNew("", "Menus/ButtonOk");
			if (choice == 2)
			{
				CloseAll();
			}
			break;
			
		case InAppPurchasingManager.Status.ReadyForPurchase:
			if (product == null)
			{
				GUI.Label (textRect, "Error communicating with server.", style);
				GUI.Label (new Rect(Screen.width * .15f, textRect.yMax + 20, Screen.width*.15f, 100f), "Please check internet connection.\nPCM" + errorCode, errorStyle);
				GUI.Label (new Rect(Screen.width * .15f, textRect.yMax + 20, Screen.width*.7f, 100f), errorCode, errorStyle);
				choice = RenderDialogBottomNew("", "Menus/ButtonOk");
				if (choice == 2)
				{
					CloseAll();
				}
				break;
			}
			
			GUI.Label (textRect, String.Format("Would you like to buy {0} for {1}?", product.Description, product.DisplayPrice), style);
			choice = RenderDialogBottomNew("Menus/Vending/Buy", "Menus/Vending/Cancel");
			if (choice == 2)
			{
				CloseAll();
			}
			else if (choice == 1)
			{
				IOSInAppPurchasingManager.PurchaseProduct(productId);
			}
			break;
			
		case InAppPurchasingManager.Status.InSession:
			GUI.Label (textRect, "Buying...", style);
			break;
			
		case InAppPurchasingManager.Status.PurchaseFailed:
			GUI.Label (textRect, "Purchase failed.", style);
			choice = RenderDialogBottomNew("", "Menus/ButtonOk");
			if (choice == 2)
			{
				CloseAll();
			}
			break;
			
		case InAppPurchasingManager.Status.PurchaseSuccessful:
			GUI.Label (textRect, "Purchase failed.", style);
			choice = RenderDialogBottomNew("", "Menus/ButtonOk");
			if (choice == 2)
			{
				CloseAll();
			}
			break;
			
		default:
			GUI.Label (textRect, "", style);
			break;
		}
		
		return true;
	}
	


    public static void RenderDialogTitle(string titlePath)
    {
		GUI.depth = RenderDepths.DIALOG_ELEMENT;
        var elementHeight = Screen.height * 2/5;

        var titleTex = (Texture2D)TextureCache.GetCachedTexture(titlePath);
        //if (titleTex == null)
        //    print("Dialog title missing texture: " + titlePath);

        GUI.DrawTexture(new Rect(0, Screen.height * .07f, Screen.width, elementHeight),
            titleTex, ScaleMode.ScaleToFit);
    }


    public static int RenderDialogBottom(String tex1Path)
    {
        int response = 0;
        var tex1 = (Texture2D)Resources.Load(tex1Path);
        //if (tex1 == null)
        //    print(string.Format("Missing texture: {0}", tex1Path));

        var style = PHIUtil.MakeButtonGUIStyle();

        var btnHeight = Screen.height / 8;
        var texScaling = (float)btnHeight / (float)tex1.height;
        var btnWidth = (float)tex1.width * texScaling;
        var halfRestartHeight = tex1.height / 2;

        if (GUI.Button(new Rect((Screen.width - btnWidth) / 2, Screen.height * 4 / 5 - btnHeight / 2, btnWidth, btnHeight), tex1, style))
        {
            response = 1;
        }

        return response;
    }

    public static int RenderDialogBottomNew(String tex1Path, String tex2Path)
    {
        int response = 0;
		
        var tex2 = (Texture2D)Resources.Load(tex2Path);

        var style = PHIUtil.MakeButtonGUIStyle();
		
		var btnPixHeight = buttonHeight * Screen.height;
        var texScaling = (float)btnPixHeight / (float)tex2.height;
        var btnWidth = (float)tex2.width * texScaling;

        Texture2D tex1;
		if (!String.IsNullOrEmpty(tex1Path))
		{
			tex1 = (Texture2D)Resources.Load(tex1Path);
        	if (GUI.Button(new Rect((Screen.width - btnWidth) / 2, (buttonsBottom * Screen.height - btnPixHeight * 2), btnWidth, btnPixHeight), tex1, style))
        	{
        	    response = 1;
        	}
		}

        if (GUI.Button(new Rect((Screen.width - btnWidth) / 2, (buttonsBottom * Screen.height - btnPixHeight), btnWidth, btnPixHeight), tex2, style))
        {
            response = 2;
        }

        return response;
    }
	
	
    public static int RenderDialogBottom(String tex1Path, String tex2Path)
    {
        int response = 0;
        var tex1 = (Texture2D)Resources.Load(tex1Path);
        //if (tex1 == null)
        //    print(string.Format("Missing texture: {0}", tex1Path));

        var tex2 = (Texture2D)Resources.Load(tex2Path);
        //if (tex2 == null)
        //    print(string.Format("Missing texture: {0}", tex1Path));

        var style = PHIUtil.MakeButtonGUIStyle();

        var btnHeight = Screen.height / 5;
        var texScaling = Math.Min(1f, (float)btnHeight / (float)tex1.height);
        var btnWidth = (float)tex1.width * texScaling;

        if (GUI.Button(new Rect((Screen.width - btnWidth) / 2, Screen.height * 3 / 5 - btnHeight / 2, btnWidth, btnHeight), tex1, style))
        {
            response = 1;
        }

        if (GUI.Button(new Rect((Screen.width - btnWidth) / 2, Screen.height * 4 / 5 - btnHeight / 2, btnWidth, btnHeight), tex2, style))
        {
            response = 2;
        }

        return response;
    }
	
	public static int RenderUpgradesQuitRetry()
	{
        int response = 0;
        var upgrades = TextureCache.GetCachedTexture("Gui/Dialog/Upgrades");
        var quit = TextureCache.GetCachedTexture("Gui/Dialog/Quit");
        var retry = TextureCache.GetCachedTexture("Gui/Dialog/Retry");

        var style = PHIUtil.MakeButtonGUIStyle();

        var btnHeight = Screen.height / 8;
        var texScaling = Math.Min(1f, (float)btnHeight / (float)upgrades.height);
        var btnWidth = (float)upgrades.width * texScaling;

        if (GUI.Button(new Rect(Screen.width / 2 - btnWidth/2, Screen.height * .5f, btnWidth, btnHeight), upgrades, style))
        {
			CloseCurrent ();
			Prefs.ShowNextLevelButtonOnVending = true;
			MenuProps.ScreenStack.Add (MenuManager.UPGRADES);
            Application.LoadLevel("MenuMain");
        }

        if (GUI.Button(new Rect(Screen.width / 2 - btnWidth/2, Screen.height * .635f, btnWidth, btnHeight), quit, style))
        {
			CloseCurrent ();
            Application.LoadLevel("MenuMain");
        }
		
        if (GUI.Button(new Rect(Screen.width / 2 - btnWidth/2, Screen.height * .77f, btnWidth, btnHeight), retry, style))
        {
			CloseCurrent ();
            Application.LoadLevel("ClimberMain");
        }

        return response;
	}


    public static float DIALOG_PADDING = .15f;
    public static float ARROW_DIALOG_HEAD_PADDING = .21f;
    public static float ARROW_DIALOG_TAIL_PADDING = .09f;

    public static Rect GetDialogRect()
    {
        return new Rect(-400, 0, Screen.width + 800, Screen.height);
    }

    public static Rect GetWideDialogRect()
    {
        return new Rect(0, 0, Screen.width, Screen.height);
    }
	
    public static Rect GetDialogContentRect()
    {
        //the width of the interior of the standard dialog is 2/5 of the total height, so find a screen.height * .4 chunk in the middle
        var width = Screen.height * .4f;
        return new Rect((Screen.width - width)/2f, Screen.height * DIALOG_PADDING, width, Screen.height * (1f - DIALOG_PADDING * 2f));
    }

    public static Rect GetDialogContentRectWide()
    {
        //print("Making dialogContentRect: " + Screen.width);
        return new Rect(0 - Screen.width/2, Screen.height * DIALOG_PADDING, Screen.width * 2, Screen.height * (1f - DIALOG_PADDING * 2f));
    }

    public static Rect GetArrowDownDialogRect(float pointAt)
    {
        return new Rect(-400, 0, Screen.width + 800, Screen.height - pointAt);
    }

    public static Rect GetArrowDownDialogContentRect(float pointAt)
    {
        var height = Screen.height - pointAt;
        return new Rect(0, height * ARROW_DIALOG_TAIL_PADDING,
                Screen.width, height * (1f - ARROW_DIALOG_TAIL_PADDING - ARROW_DIALOG_HEAD_PADDING));
    }

    public static Rect GetArrowUpDialogRect(float pointAt)
    {
        return new Rect(-400, Screen.height - pointAt, Screen.width + 800, pointAt);    //1.05 to give it a little extra padding
    }

    public static Rect GetWideArrowUpDialogRect(float pointAt, Vector2 contentSize)
    {
		var height = pointAt;
		var scaleFactor = height / contentSize.y;
		var width = contentSize.x * scaleFactor;
		
		if (width > Screen.width)
		{
			width = Screen.width;
			scaleFactor = width / contentSize.x;
			height = contentSize.y * scaleFactor;
		}
		
        return new Rect(0, Screen.height - pointAt, width, height); 
    }
	
    public static Rect GetArrowUpDialogContentRect(float pointAt)
    {
        var dialogHeight = pointAt;
        return new Rect(0, (Screen.height - pointAt) + dialogHeight * ARROW_DIALOG_HEAD_PADDING,
            Screen.width, dialogHeight * (1f - ARROW_DIALOG_TAIL_PADDING - ARROW_DIALOG_HEAD_PADDING));
    }
}
