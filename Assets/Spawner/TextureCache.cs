using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureCache {

    private static Dictionary<String, Texture2D> _textureCache;

    public static Texture2D GetCachedTexture(String texturePath)
    {
        if (_textureCache == null)
            _textureCache = new Dictionary<string, Texture2D>();

        if (!_textureCache.ContainsKey(texturePath))
        {
            CacheTexture(texturePath);
        }

        return _textureCache[texturePath];
    }

    public static void CacheTexture(string texturePath)
    {
        if (_textureCache == null)
            _textureCache = new Dictionary<string, Texture2D>();

        if (_textureCache.ContainsKey(texturePath))
            return;

        var tex = Resources.Load(texturePath) as Texture2D;
        if (tex == null)
        {
//            Debug.Log("************ COULD NOT FIND TEXTURE: " + texturePath + "*********************");
            throw (new Exception("MakeBillboardInternal: could not find texture: " + texturePath));
        }
        _textureCache.Add(texturePath, tex);
    }

    public static void PreCacheTextures()
    {
        TextureCache.CacheTexture("Menus/Comics/IntroComic");

        TextureCache.CacheTexture("FinishLine");
        TextureCache.CacheTexture("Gui/PauseButton");
        TextureCache.CacheTexture("Bonuses/x2");
        TextureCache.CacheTexture("Bonuses/x3");
        TextureCache.CacheTexture("Bonuses/x4");
        TextureCache.CacheTexture("Bonuses/Popsicle");
        TextureCache.CacheTexture("Bonuses/Donut");
        TextureCache.CacheTexture("Bonuses/PeanutBrittle");
        TextureCache.CacheTexture("Bonuses/PeanutBrittle2");
        TextureCache.CacheTexture("Bonuses/TapHere");
        TextureCache.CacheTexture("Cookies/Crumbs");
        TextureCache.CacheTexture("Cookies/OrangeTarget");

        TextureCache.CacheTexture("Gui/StaminaBarLabel");
        TextureCache.CacheTexture("Gui/FillBarBase");
        TextureCache.CacheTexture("Gui/FillBarFlashBase");
        TextureCache.CacheTexture("Gui/FillBarFill");
        TextureCache.CacheTexture("Gui/FillBarOverlay");

        TextureCache.CacheTexture("Grass");
        TextureCache.CacheTexture("Rock");
		
        TextureCache.CacheTexture("Menus/Vending/BonkProofing1");
        TextureCache.CacheTexture("Menus/Vending/ComboTimeout1");
        TextureCache.CacheTexture("Menus/Vending/Coins1");
        TextureCache.CacheTexture("Menus/Vending/StaminaSnack1");
        TextureCache.CacheTexture("Menus/Vending/Zippers1");
        TextureCache.CacheTexture("Menus/Vending/FrontPanel");
    }

}
