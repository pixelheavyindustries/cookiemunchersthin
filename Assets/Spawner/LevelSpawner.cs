using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class LevelSpawner : SpawnerBase {

//    private static Dictionary<Cookie.IngredientType, String> _ingredientPathsByType;
    private static List<GameObject> Entities;
	
    public static void Clear()
    {
        foreach (var e in Entities)
        {
            Destroy(e);
        }
    }

    private static void SaveEntity(GameObject go)
    {
        if (Entities == null)
            Entities = new List<GameObject>();

        Entities.Add(go);
    }


    //TODO: remove
    public static GameObject MakeSelector(float blockSize)
    {
        var dec = new GameObject("CookieHitZone");
        var rb = dec.AddComponent<Rigidbody>();
        var bc = dec.AddComponent<BoxCollider>();
        bc.transform.localScale = new Vector3(blockSize, blockSize, 1);
        rb.isKinematic = true;
        rb.useGravity = false;
        return dec;
    }

	
    public static GameObject MakeAutoPilotMuncher(bool hasHorns, Cookie.CookieShape shape, Cookie.Color baseColor, Cookie.Color spotColor, String eyesMouthPath, float blockSize, ClimberMain.DifficultyLevel difficulty, float subDiff, ClimberMain mainRef)//rockets?
    {
		var muncher = MakeMuncherCommonParts(shape, baseColor, eyesMouthPath, blockSize);
        var muncherScript = muncher.AddComponent<AutoPilotCookieMuncher>();
		return MakeMuncherInternal(muncher, muncherScript, hasHorns, shape, baseColor, spotColor, eyesMouthPath, blockSize, difficulty, subDiff, mainRef);
	}

    public static GameObject MakeMuncher(bool hasHorns, Cookie.CookieShape shape, Cookie.Color baseColor, Cookie.Color spotColor, String eyesMouthPath, float blockSize, ClimberMain.DifficultyLevel difficulty, float subDiff, ClimberMain mainRef)//rockets?
    {
		var muncher = MakeMuncherCommonParts(shape, baseColor, eyesMouthPath, blockSize);
        var muncherScript = muncher.AddComponent<CookieMuncher>();
		return MakeMuncherInternal(muncher, muncherScript, hasHorns, shape, baseColor, spotColor, eyesMouthPath, blockSize, difficulty, subDiff, mainRef);
	}
	
    public static GameObject MakeMuncherInternal(GameObject muncher, 
		CookieMuncher muncherScript,
		bool hasHorns, 
		Cookie.CookieShape shape, 
		Cookie.Color baseColor, 
		Cookie.Color spotColor, 
		String eyesMouthPath, 
		float blockSize, 
		ClimberMain.DifficultyLevel difficulty, 
		float subDiff, 
		ClimberMain mainRef)//rockets?
    {
        muncherScript.Shape = shape;
        muncherScript.HasHorns = hasHorns;
        muncherScript.BaseColor = baseColor;
        muncherScript.SpotColor = spotColor;
        muncherScript.Difficulty = difficulty;

        muncherScript.StaminaBurn = Prefs.GetStaminaBurn(difficulty, subDiff);
        muncherScript.StaminaFill = Prefs.GetStaminaFill(difficulty, subDiff);
        muncherScript.MaxSpeed = Prefs.GetMaxSpeed(difficulty);
        muncherScript.GrabRange = 10f;

        muncherScript.MainRef = mainRef;

        var tongue = MakeTongue(muncher);
        muncherScript.Tongue = tongue;
        tongue.gameObject.transform.parent = muncher.transform;

		AddFace(muncher, muncherScript);

        return muncher;
    }
	
	private static GameObject MakeMuncherCommonParts(Cookie.CookieShape shape, Cookie.Color baseColor, String eyesMouthPath, float blockSize)
	{
        List<String> paths = new List<String>() { "Munchers/Bases/" + baseColor.ToString() + "/" + shape.ToString() };

        var dec = MakeLayeredObject(ShaderTypeAlphaBlended, paths.ToArray(), 100, true, 1, 0);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        AddSphereCollider(dec, blockSize);
		
		return dec;
	}

	private static void AddFace(GameObject muncher, MuncherBase muncherScript)
	{
		var isStar = muncherScript.Shape == Cookie.CookieShape.Star;
        muncherScript.ClosedMouthFace = MakeBillboard(ShaderTypeAlphaBlended, isStar ? "Munchers/HappyFaceStar" : "Munchers/HappyFace1", 100, true, 1, 1);
        muncherScript.OpenMouthFace = MakeBillboard(ShaderTypeAlphaBlended, isStar ? "Munchers/OpenMouthStar" : "Munchers/OpenMouth1", 100, true, 1, 1);
        muncherScript.OpenMouthFace.transform.position = muncherScript.ClosedMouthFace.transform.position = muncher.transform.position + new Vector3(0, 0, -.5f);
        muncherScript.ClosedMouthFace.transform.parent = muncher.transform;
        muncherScript.OpenMouthFace.transform.parent = muncher.transform;
        PHIUtil.SetChildVisibility(muncherScript.OpenMouthFace, false, false);
	}
	
    public static Tongue MakeTongue(GameObject locationSource)
    {
        var go = new GameObject("TongueWrapper");
        var tongue = go.AddComponent<Tongue>();
        var tongueObj = MakeBillboard(ShaderTypeAlphaBlended, "Munchers/Tongue", 40, true, 1, 1);
        tongueObj.transform.parent = go.transform;
        tongue.Init(tongueObj, locationSource);

        return tongue;
    }
	
	public static GameObject MakeFlyerMuncher(Cookie.CookieShape shape, Cookie.Color baseColor, String eyesMouthPath, float blockSize)
	{
		var muncher = MakeMuncherCommonParts(shape, baseColor, eyesMouthPath, blockSize);
		
        var muncherScript = muncher.AddComponent<FlyerMuncher>();
		
		return muncher;
	}


    public static GameObject MakeCookie(bool isGood, Cookie.CookieShape shape, Cookie.Color baseColor, float blockSize, float missionRatio, float cookieMoveTime, bool showTargeting = false)
    {
        var cookiePath = "Cookies/Frosting/" + baseColor + "/" + shape.ToString();
        var dec = MakeBillboard(ShaderTypeAlphaBlended, cookiePath, 19, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetDimensionsInMeters(blockSize, blockSize);

        var cookieScript = dec.AddComponent<Cookie>();
        cookieScript.BaseColor = baseColor;
        cookieScript.Shape = shape;
        cookieScript.CookieMoveTime = cookieMoveTime;
        cookieScript.Good = isGood;
		
        //if it's not good, we won't need to know if we hit it
        if (isGood)
		{
            AddSphereCollider(dec, 1);//blockSize);
			
			if (showTargeting)
			{
		        var target = MakeBillboard(ShaderTypeAlphaBlended, "Cookies/OrangeTarget", 40, true, 1, 1);
				target.GetComponent<BillboardProps>().RotationTime = 6f;
				target.transform.parent = dec.transform;
				cookieScript.AddAttachment(target);
			}
		}
		
        return dec;
    }



    public static FightingEntity MakeZipper(float blockSize, ClimberMain.DifficultyLevel difficulty)
    {
        var dec = MakeBillboard(ShaderTypeAlphaBlended, "Bonuses/Popsicle", 78, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetDimensionsInMeters(blockSize * 1.8f, blockSize * 1.8f);

        AddSphereCollider(dec, 1);

        var zipperScript = dec.AddComponent<Zipper>();
        zipperScript.MaxSpeed = Prefs.ZipperSpeed;
        return zipperScript;
    }


    public static FightingEntity MakeCoin(float blockSize, int coinValue = 1)
    {
        var dec = MakeBillboard(ShaderTypeAlphaBlended, "Bonuses/Coin", 200, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
          rb.useGravity = false;
        AddSphereCollider(dec, blockSize * 1.3f);
        var coinScript = dec.AddComponent<Coin>();
        coinScript.CoinValue = coinValue;
        return coinScript;
		
/*		print("Spawning coin");
		
        var mesh = Resources.Load("Bonuses/CoinModel");
        var go = (GameObject)Instantiate(mesh);
        var rb = go.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        AddSphereCollider(go, blockSize / 1.3f);

        var coinScript = go.AddComponent<Coin>();
        coinScript.CoinValue = coinValue;
        return coinScript;*/
    }

    public static GameObject MakeComboBoost()
    {
//		return MakeFireworks(1).gameObject;
        var go = (GameObject)GameObject.Instantiate(Resources.Load("Explosions/Fireworks"));
		return go;
    }

	
    public static FightingEntity MakeFireworks(float blockSize)
    {
        var dec = MakeBillboard(ShaderTypeAlphaBlended, "Bonuses/Fireworks", 20, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetWidthInMeters(blockSize * 1.8f);

        var fireworksScript = dec.AddComponent<Fireworks>();
        return fireworksScript;
    }

	
    public static FightingEntity MakeStaminaSnack(float blockSize)
    {
        var dec = MakeBillboard(ShaderTypeAlphaBlended, "Bonuses/Donut", 300, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        AddSphereCollider(dec, 1);
        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetWidthInMeters(blockSize * 1.8f);

        var snackScript = dec.AddComponent<StaminaSnack>();
        return snackScript;
    }

/*    public static FightingEntity MakeMinigameToken(float blockSize)
    {
        var dec = MakeBillboard(ShaderTypeAlphaBlended, "Bonuses/Pie", 300, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        AddSphereCollider(dec, 1);
        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetWidthInMeters(blockSize * 1.8f);

        var tokenScript = dec.AddComponent<MinigameToken>();
        return tokenScript;
    }*/

    public static FightingEntity MakeAsteroid(float blockSize, ClimberMain.DifficultyLevel difficulty, float subDiff)
    {
		var path = "Bonuses/PeanutBrittle";
		if (Random.Range(0, 1) == 1)
			path = "Bonuses/PeanutBrittle2";
			
        var dec = MakeBillboard(ShaderTypeAlphaBlended, path, 100, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        AddSphereCollider(dec, 1f);

        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetWidthInMeters(blockSize * (2f + Random.Range(0f, 1.1f)) );

        var asteroidScript = dec.AddComponent<Asteroid>();
        asteroidScript.DamagePoints = (int)Prefs.GetAsteroidDamage(difficulty, subDiff);
        return asteroidScript;
    }

    public static FightingEntity MakeTrigger(float blockSize, Func<bool> callback)
    {
        var go = new GameObject("Trigger");
        var rb = go.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;

        AddSphereCollider(go, blockSize / 2f);

        var triggerScript = go.AddComponent<Trigger>();
        triggerScript.Callback = callback;
        return triggerScript;
    }

    public static GameObject MakePointSplash(int num, Vector3 location)
    {
        var POINT_SPLASH_Z = -2;

        if (num < 2 || num > 7)
            return null;

        var dec = MakeBillboard(ShaderTypeAlphaBlended, "Bonuses/x" + num, 70, true, 1, 1);
        var rb = dec.AddComponent<Rigidbody>();
        rb.isKinematic = true;
        rb.useGravity = false;
        dec.transform.position = new Vector3(location.x, location.y, POINT_SPLASH_Z);

        var bbProps = dec.GetComponent<BillboardProps>();
        bbProps.SetWidthInMeters(ScreenDimensions.BlockSize * 3);

        var puffScript = dec.AddComponent<BouncyParticle>();
        puffScript.Duration = 1f;
        puffScript.Scales = BouncyParticle.PopCorn;
        return dec;
    }


    public static GameObject MakeAltimeter(IAltitudeProvider mainRef, Rect position)
    {
        var go = new GameObject("Altimeter");
        var alt = go.AddComponent<Altimeter>();
        alt.Init(position, mainRef);

        return go;
    }


    public static BoosterButton MakeBoosterButton(Rect position)
    {
        var go = new GameObject("BoosterButton");

        var button = go.AddComponent<BoosterButton>();
        button.Init(position);

        return button;
    }

    public static FightingEntity SpawnBonus(float blockSize, ClimberMain.DifficultyLevel difficulty)
    {
        switch (Random.Range(1, 4))
        {
            case 1:
                return MakeZipper(blockSize, difficulty);

            case 2:
                return MakeCoin(blockSize);

            default:
                return MakeStaminaSnack(blockSize);
        }
    }


    public static FightingEntity SpawnObstacle(float blockSize, ClimberMain.DifficultyLevel difficulty, float subDiff)
    {
        return MakeAsteroid(blockSize, difficulty, subDiff);
    }

    #region Audio
    public static AudioClip BulletHit1()
    {
        return Resources.Load("Sound/BulletHit1") as AudioClip;
    }

    public static AudioClip BulletFire1()
    {
        return Resources.Load("Sound/BulletFire1") as AudioClip;
    }
    #endregion Audio

}
