using UnityEngine;
using System.Collections;

public class BuildConfig {
    public static bool IsWeb()
    {
        return (PlatformSpecifics.BuildType == "Web");
    }

    public static bool IsAndroid()
    {
        return (PlatformSpecifics.BuildType == "Android");
    }

    public static bool IsIos()
    {
        return (PlatformSpecifics.BuildType == "Ios");
    }
}
