using System.Linq;
using UnityEngine;
using System.Collections;

public class PHIUtil : MonoBehaviour {
    //this creates and destroys a giant flat plane, so it's not something to call multiple times per frame
    public static Vector2 GetScreenDimensions()
    {
        var upperLeft = Get3DPoint(0, 0);
        var lowerRight = Get3DPoint(Screen.width, Screen.height);
        return new Vector2(lowerRight.x - upperLeft.x, lowerRight.y - upperLeft.y);
    }


    private static Vector3 Get3DPoint(float x, float y)
    {
        var hitPlane = new GameObject("Get3DPointHitPlane");
        var boxCollider = hitPlane.AddComponent<BoxCollider>();
        boxCollider.size = new Vector3(1000, 1000, 0.01f);

        var rayStart = new Vector3(x, y, 0);
        Ray ray = Camera.main.ScreenPointToRay(rayStart);
        var hits = Physics.RaycastAll(ray);

        GameObject.Destroy(hitPlane);

        var clickSurfaceHit = (from h in hits where h.collider.name == "Get3DPointHitPlane" select h).ToList();
        if (clickSurfaceHit.Count > 0)
            return clickSurfaceHit.First().point;


        return new Vector3();
    }

    public static void SetScreenDimensions(Vector2 dimensions)
    {
    }

    public static void SetChildVisibility(GameObject go, bool visible, bool recursive)
    {
        var baseRenderer = go.GetComponent<Renderer>();
        if (baseRenderer != null)
        {
            baseRenderer.enabled = visible;
        }
		
        for (var i = 0; i < go.transform.GetChildCount(); i++)
        {
            var thisChild = go.transform.GetChild(i).gameObject;
            var renderer = thisChild.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.enabled = visible;
            }

            if (recursive)
                SetChildVisibility(thisChild, visible, true);
        }
    }

    public static float RoundToPlaces(float value, int numPlaces)
    {
        var multiplier = 1;
        for (var i = 0; i < numPlaces; i++)
            multiplier *= 10;

        return Mathf.Round(value * multiplier) / multiplier;
    }

    public static float Clamp(float value, float min, float max)
    {
        return Mathf.Max(min, Mathf.Min(value, max));
    }

    public static Vector2 MaximizeBoxWithinBounds(Vector2 box, Vector2 bounds)
    {
        return box;
    }

    public static GUIStyle MakeButtonGUIStyle()
    {
        var style = new GUIStyle();
        style.normal.background = style.active.background = style.hover.background = style.focused.background = null;
		style.imagePosition = ImagePosition.ImageOnly;
        return style;
    }

    public static GUIStyle MakeCenteredButtonGUIStyle()
    {
        var style = MakeButtonGUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        return style;
    }

    public static GUIStyle MakeDialogTextStyle()
    {
        var style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
        style.font = GetLargeFont();
        style.wordWrap = true;

        return style;
    }

    public static Font GetLargeFont()
    {
        if (BuildConfig.IsWeb())
            return Resources.Load("Fonts/casual_web_large") as Font;

		if (ScreenDimensions.GameAreaInPixels.height > 1400)
	        return Resources.Load("Fonts/casual_phone_xlarge") as Font;
		
		if (ScreenDimensions.GameAreaInPixels.height > 600)
	        return Resources.Load("Fonts/casual_phone_large") as Font;
		
        return Resources.Load("Fonts/casual_phone_med") as Font;

//        return Resources.Load("Fonts/GILSANUB") as Font;
    }

    public static Font GetMediumFont()
    {
        if (BuildConfig.IsWeb())
            return Resources.Load("Fonts/casual_web_med") as Font;

		if (ScreenDimensions.GameAreaInPixels.height > 1400)
	        return Resources.Load("Fonts/casual_phone_large") as Font;
		
		if (ScreenDimensions.GameAreaInPixels.height > 600)
	        return Resources.Load("Fonts/casual_phone_med") as Font;
		
        return Resources.Load("Fonts/casual_phone_small") as Font;
    }

    public static GUIStyle GetMediumTextStyle()
    {
        var style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
        style.font = GetMediumFont();
        style.wordWrap = true;

        return style;
    }

    public static GUIStyle GetLargeTextStyle()
    {
        var style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        style.normal.textColor = Color.white;
        style.font = GetLargeFont();
        style.wordWrap = true;

        return style;
    }
}
