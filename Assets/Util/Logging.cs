using System;
using UnityEngine;
using System.Collections;

public class Logging {

    private static bool _haveLoggedSession = false;
    public static void LogSession()
    {
/*        WWW www;
        if (!_haveLoggedSession)
            www = new WWW(String.Format("http://pixelheavyindustries.com/Log.php?Pid={0}&Version={1}", Prefs.Pid, DefaultSetter.NUMERIC_VERSION));

        _haveLoggedSession = true;*/
    }

    /*
     * Only use this if the user hasn't passed the level yet
     */
    public static void LogLevelFail(int time)
    {
/*        if (Prefs.GetGameMode() != Prefs.MODE_CLASSIC)
            return;

        var diff = (ClimberMain.DifficultyLevel)Prefs.CurrentDifficulty;
        var level = Prefs.GetMissionLevel(diff);
        var loggableLevel = (int)diff * 50 + level;
        var url = String.Format("http://pixelheavyindustries.com/Lose.php?Pid={0}&Level={1}&Time={2}&Version={3}&Upgrades={4}", Prefs.Pid, loggableLevel, time, DefaultSetter.NUMERIC_VERSION, GetUpgradesNumber());
        var www = new WWW(url);*/
    }

    /*
     * Only use this if the user hasn't passed the level yet
     */
    public static void LogLevelWin(int time, bool first)
    {
/*        var diff = (ClimberMain.DifficultyLevel)Prefs.CurrentDifficulty;
        var level = Prefs.GetMissionLevel(diff);
        var loggableLevel = (int)diff * 50 + level;
        var plays = Prefs.GetTotalPlays(diff, level);
        WWW www;
        String url;
        if (first)
        {
            url = String.Format("http://pixelheavyindustries.com/FirstWin.php?Pid={0}&Level={1}&Time={2}&Plays={3}&Version={4}&LifetimeCoins={5}&Upgrades={6}", Prefs.Pid, loggableLevel, time, plays, DefaultSetter.NUMERIC_VERSION, Prefs.LifetimeMoney, GetUpgradesNumber());
            //Debug.Log("FirstWin: " + url);
            www = new WWW(url);
        }
        else
        {
            url = String.Format("http://pixelheavyindustries.com/Win.php?Pid={0}&Level={1}&Time={2}&Version={3}&Upgrades={4}", Prefs.Pid, loggableLevel, time, DefaultSetter.NUMERIC_VERSION, GetUpgradesNumber());
            //Debug.Log("Win: " + url);
            www = new WWW(url);
        }*/
    }


    private static int GetUpgradesNumber()
    {
        int upgrades =  (int)Prefs.GetUpgradeLevel(UpgradeType.Booster) * 1000 +
                        (int)Prefs.GetUpgradeLevel(UpgradeType.StaminaSnack) * 100 +
                        (int)Prefs.GetUpgradeLevel(UpgradeType.Coins) * 10 +
                        (int)Prefs.GetUpgradeLevel(UpgradeType.Zippers);

        return upgrades;
    }

}
