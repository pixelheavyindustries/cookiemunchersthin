using UnityEngine;
using System.Collections;

public class GameClock : MonoBehaviour {

    private static GameClock _instance = null;
    public static bool Paused = false;
    public static float CurrTime;

	// Use this for initialization
	void Start () {
        if (_instance == null)
            _instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        if (!Paused)
            CurrTime += Time.deltaTime;
	}
}
