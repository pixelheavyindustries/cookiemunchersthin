using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections;
using Object = System.Object;

public class Tweenk : MonoBehaviour
{
    private TweenProps<float> _x;
    private TweenProps<float> _y;
    private TweenProps<float> _z;
    private TweenProps<float> _xScale;
    private TweenProps<float> _yScale;
    private TweenProps<float> _zScale;
    private TweenProps<Quaternion> _rotation;
    private CallbackProps _callback;
    public Guid Guid;

    private class TweenProps<T>
    {
        public T StartVal;
        public T EndVal;
        public float StartTime;
        public float Duration;
        public Func<float, float, float, float, float> EasingType;
        //public Func<bool> Callback;
    }

    public class CallbackProps
    {
        public float StartTime;
        public float Duration;
        public Func<bool> Callback;
    }

    public void Start()
    {
        Guid = Guid.NewGuid();
    }

    public void Update()
    {
        if (_x != null || _y != null || _z != null)
        {
            var newX = _x == null ? gameObject.transform.position.x : RunTween(_x);
            var newY = _y == null ? gameObject.transform.position.y : RunTween(_y);
            var newZ = _z == null ? gameObject.transform.position.z : RunTween(_z);
            gameObject.transform.position = new Vector3(newX, newY, newZ);

            if (_x != null && _x.StartTime + _x.Duration < GameClock.CurrTime)
                _x = null;

            if (_y != null && _y.StartTime + _y.Duration < GameClock.CurrTime)
                _y = null;

            if (_z != null && _z.StartTime + _z.Duration < GameClock.CurrTime)
                _z = null;
        }

        if (_xScale != null || _yScale != null || _zScale != null)
        {
            var newXScale = _xScale == null ? gameObject.transform.localScale.x : RunTween(_xScale);
            var newYScale = _yScale == null ? gameObject.transform.localScale.y : RunTween(_yScale);
            var newZScale = _zScale == null ? gameObject.transform.localScale.z : RunTween(_zScale);
            gameObject.transform.localScale = new Vector3(newXScale, newYScale, newZScale);

            if (_xScale != null && _xScale.StartTime + _xScale.Duration < GameClock.CurrTime)
                _xScale = null;

            if (_yScale != null && _yScale.StartTime + _yScale.Duration < GameClock.CurrTime)
                _yScale = null;

            if (_zScale != null && _zScale.StartTime + _zScale.Duration < GameClock.CurrTime)
                _zScale = null;
        }

        if (_rotation != null)
        {
            //can't pass a quaternion to easing func, so get value between 0 and 1, and pass it to Lerp()
            var val = _rotation.EasingType(GameClock.CurrTime - _rotation.StartTime, 0, 1, _rotation.Duration);
            if (GameClock.CurrTime >= _rotation.StartTime + _rotation.Duration)
            {
//                if (_rotation.Callback != null) _rotation.Callback();
                gameObject.transform.rotation = _rotation.EndVal;
                _rotation = null;
            }
            else
                gameObject.transform.rotation = Quaternion.Lerp(_rotation.StartVal, _rotation.EndVal, val);
        }


        if (_callback != null)
        {
            if (GameClock.CurrTime >= _callback.StartTime + _callback.Duration)
            {
                var c = _callback;
                _callback = null;
                c.Callback();
            }
        }
    }

    private float RunTween(TweenProps<float> tweenProps)
    {
        if (GameClock.CurrTime >= tweenProps.StartTime + tweenProps.Duration)
        {
            //if (tweenProps.Callback != null)
            //    tweenProps.Callback();
            return tweenProps.EndVal;
        }
        var t = (GameClock.CurrTime - tweenProps.StartTime);
        var val = tweenProps.EasingType(t, tweenProps.StartVal, tweenProps.EndVal - tweenProps.StartVal, tweenProps.Duration);
        return val;
    }

    public void X(float x, float duration, Func<float, float, float, float, float> easing)
    {
        _x = new TweenProps<float> { EasingType = easing, StartVal = gameObject.transform.position.x, EndVal = x, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void Y(float y, float duration, Func<float, float, float, float, float> easing)
    {
        _y = new TweenProps<float> { EasingType = easing, StartVal = gameObject.transform.position.y, EndVal = y, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void Z(float z, float duration, Func<float, float, float, float, float> easing)
    {
        _z = new TweenProps<float> { EasingType = easing, StartVal = gameObject.transform.position.z, EndVal = z, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void XScale(float x, float duration, Func<float, float, float, float, float> easing)
    {
        _xScale = new TweenProps<float> { EasingType = easing, StartVal = gameObject.transform.localScale.x, EndVal = x, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void YScale(float y, float duration, Func<float, float, float, float, float> easing)
    {
        _yScale = new TweenProps<float> { EasingType = easing, StartVal = gameObject.transform.localScale.y, EndVal = y, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void ZScale(float z, float duration, Func<float, float, float, float, float> easing)
    {
        _zScale = new TweenProps<float> { EasingType = easing, StartVal = gameObject.transform.localScale.z, EndVal = z, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void Rotation(Quaternion rotation, float duration, Func<float, float, float, float, float> easing)
    {
        _rotation = new TweenProps<Quaternion> { EasingType = easing, StartVal = gameObject.transform.rotation, EndVal = rotation, StartTime = GameClock.CurrTime, Duration = duration };
    }

    public void Callback(float duration, Func<bool> callback)
    {
        _callback = new CallbackProps { StartTime = GameClock.CurrTime, Duration = duration, Callback = callback };
    }

    public void StopX()
    {
        _x = null;
    }
    public void StopY()
    {
        _y = null;
    }
    public void StopZ()
    {
        _z = null;
    }
    public void StopRotation()
    {
        _rotation = null;
    }
    public void StopCallback()
    {
        _callback = null;
    }
    public void StopAll()
    {
        StopX();
        StopY();
        StopZ();
        StopRotation();
        StopCallback();
    }



    #region Easing Funcs

    public static class Easing
    {
	// ==================================================================================================================================
	// TWEENING EQUATIONS functions -----------------------------------------------------------------------------------------------------
	// (the original equations are Robert Penner's work as mentioned on the disclaimer)

		/**
		 * Easing equation function for a simple linear tweening, with no easing.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseNone (float t, float b, float c, float d) {
			return c*t/d + b;
		}
	
		/**
		 * Easing equation function for a quadratic (t^2) easing in: accelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInQuad (float t, float b, float c, float d) {
			return c*(t/=d)*t + b;
		}
	
		/**
		 * Easing equation function for a quadratic (t^2) easing out: decelerating to zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutQuad (float t, float b, float c, float d) {
			return -c *(t/=d)*(t-2) + b;
		}
	
		/**
		 * Easing equation function for a quadratic (t^2) easing in/out: acceleration until halfway, then deceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutQuad (float t, float b, float c, float d) {
			if ((t/=d/2) < 1) return c/2*t*t + b;
			return -c/2 * ((--t)*(t-2) - 1) + b;
		}
	
		/**
		 * Easing equation function for a quadratic (t^2) easing out/in: deceleration until halfway, then acceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInQuad (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutQuad (t*2, b, c/2, d);
			return EaseInQuad((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for a cubic (t^3) easing in: accelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInCubic (float t, float b, float c, float d) {
			return c*(t/=d)*t*t + b;
		}
	
		/**
		 * Easing equation function for a cubic (t^3) easing out: decelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutCubic (float t, float b, float c, float d) {
			return c*((t=t/d-1)*t*t + 1) + b;
		}
	
		/**
		 * Easing equation function for a cubic (t^3) easing in/out: acceleration until halfway, then deceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutCubic (float t, float b, float c, float d) {
			if ((t/=d/2) < 1) return c/2*t*t*t + b;
			return c/2*((t-=2)*t*t + 2) + b;
		}
	
		/**
		 * Easing equation function for a cubic (t^3) easing out/in: deceleration until halfway, then acceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInCubic (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutCubic (t*2, b, c/2, d);
			return EaseInCubic((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for a quartic (t^4) easing in: accelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInQuart (float t, float b, float c, float d) {
			return c*(t/=d)*t*t*t + b;
		}
	
		/**
		 * Easing equation function for a quartic (t^4) easing out: decelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutQuart (float t, float b, float c, float d) {
			return -c * ((t=t/d-1)*t*t*t - 1) + b;
		}
	
		/**
		 * Easing equation function for a quartic (t^4) easing in/out: acceleration until halfway, then deceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutQuart (float t, float b, float c, float d) {
			if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
			return -c/2 * ((t-=2)*t*t*t - 2) + b;
		}
	
		/**
		 * Easing equation function for a quartic (t^4) easing out/in: deceleration until halfway, then acceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInQuart (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutQuart (t*2, b, c/2, d);
			return EaseInQuart((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for a quintic (t^5) easing in: accelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInQuint (float t, float b, float c, float d) {
			return c*(t/=d)*t*t*t*t + b;
		}
	
		/**
		 * Easing equation function for a quintic (t^5) easing out: decelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutQuint (float t, float b, float c, float d) {
			return c*((t=t/d-1)*t*t*t*t + 1) + b;
		}
	
		/**
		 * Easing equation function for a quintic (t^5) easing in/out: acceleration until halfway, then deceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutQuint (float t, float b, float c, float d) {
			if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
			return c/2*((t-=2)*t*t*t*t + 2) + b;
		}
	
		/**
		 * Easing equation function for a quintic (t^5) easing out/in: deceleration until halfway, then acceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInQuint (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutQuint (t*2, b, c/2, d);
			return EaseInQuint((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for a sinusoidal (sin(t)) easing in: accelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInSine (float t, float b, float c, float d) {
			return -c * (float)Math.Cos(t/d * (Math.PI/2)) + c + b;
		}
	
		/**
		 * Easing equation function for a sinusoidal (sin(t)) easing out: decelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutSine (float t, float b, float c, float d) {
			return c * (float)Math.Sin(t/d * (Math.PI/2)) + b;
		}
	
		/**
		 * Easing equation function for a sinusoidal (sin(t)) easing in/out: acceleration until halfway, then deceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutSine (float t, float b, float c, float d) {
			return -c/2 * ((float)Math.Cos(Math.PI*t/d) - 1) + b;
		}
	
		/**
		 * Easing equation function for a sinusoidal (sin(t)) easing out/in: deceleration until halfway, then acceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInSine (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutSine (t*2, b, c/2, d);
			return EaseInSine((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for an exponential (2^t) easing in: accelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInExpo (float t, float b, float c, float d) {
			return (t==0) ? b : c * (float)Math.Pow(2, 10 * (t/d - 1)) + b - c * 0.001f;
		}
	
		/**
		 * Easing equation function for an exponential (2^t) easing out: decelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutExpo (float t, float b, float c, float d) {
			return (t==d) ? b+c : c * 1.001f * (float)(-Math.Pow(2, -10 * t/d) + 1) + b;
		}
	
		/**
		 * Easing equation function for an exponential (2^t) easing in/out: acceleration until halfway, then deceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutExpo (float t, float b, float c, float d) {
			if (t==0) return b;
			if (t==d) return b+c;
			if ((t/=d/2) < 1) return c/2 * (float)Math.Pow(2, 10 * (t - 1)) + b - c * 0.0005f;
			return c/2 * 1.0005f * (float)(-Math.Pow(2, -10 * --t) + 2) + b;
		}
	
		/**
		 * Easing equation function for an exponential (2^t) easing out/in: deceleration until halfway, then acceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInExpo (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutExpo (t*2, b, c/2, d);
			return EaseInExpo((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for a circular (sqrt(1-t^2)) easing in: accelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInCirc (float t, float b, float c, float d) {
			return -c * (float)(Math.Sqrt(1 - (t/=d)*t) - 1) + b;
		}
	
		/**
		 * Easing equation function for a circular (sqrt(1-t^2)) easing out: decelerating from zero velocity.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutCirc (float t, float b, float c, float d) {
			return c * (float)Math.Sqrt(1 - (t=t/d-1)*t) + b;
		}
	
		/**
		 * Easing equation function for a circular (sqrt(1-t^2)) easing in/out: acceleration until halfway, then deceleration.
 		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutCirc (float t, float b, float c, float d) {
			if ((t/=d/2) < 1) return -c/2 * (float)(Math.Sqrt(1 - t*t) - 1) + b;
			return c/2 * (float)(Math.Sqrt(1 - (t-=2)*t) + 1) + b;
		}
	
		/**
		 * Easing equation function for a circular (sqrt(1-t^2)) easing out/in: deceleration until halfway, then acceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInCirc (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutCirc (t*2, b, c/2, d);
			return EaseInCirc((t*2)-d, b+c/2, c/2, d);
		}
	
		/**
		 * Easing equation function for an elastic (exponentially decaying sine wave) easing in: accelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param a		Amplitude.
		 * @param p		Period.
		 * @return		The correct value.
		 */
		public static float EaseInElastic (float t, float b, float c, float d)
		{
		    return EaseInElastic(t, b, c, d, -9999, -9999);
		}

	    public static float EaseInElastic (float t, float b, float c, float d, float a, float p) {
			if (t==0) return b;  if ((t/=d)==1) return b+c;  if (p == -9999) p=d*.3f;
		    float s;
			if (a == -9999 || a < (float)Math.Abs(c)) { a=c; s=p/4; }
			else s = p/(2*(float)Math.PI) * (float)Math.Asin (c/a);
			return -(a*(float)Math.Pow(2,10*(t-=1)) * (float)Math.Sin( (t*d-s)*(2*Math.PI)/p )) + b;
		}
	
		/**
		 * Easing equation function for an elastic (exponentially decaying sine wave) easing out: decelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param a		Amplitude.
		 * @param p		Period.
		 * @return		The correct value.
		 */
		public static float EaseOutElastic (float t, float b, float c, float d)
		{
		    return EaseInElastic(t, b, c, d, -9999, -9999);
		}

		public static float EaseOutElastic (float t, float b, float c, float d, float a, float p) {
			if (t==0) return b;  if ((t/=d)==1) return b+c;  if (p == -9999) p=d*.3f;
		    float s;
			if (a == -9999 || a < Math.Abs(c)) { a=c; s=p/4; }
			else s = p/(2*(float)Math.PI) * (float)Math.Asin (c/a);
			return (a*(float)Math.Pow(2,-10*t) * (float)Math.Sin( (t*d-s)*(2*Math.PI)/p ) + c + b);
		}
	
		/**
		 * Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: acceleration until halfway, then deceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param a		Amplitude.
		 * @param p		Period.
		 * @return		The correct value.
		 */
        //public static float EaseInOutElastic (t:Number, b:Number, c:Number, d:Number, a:Number = Number.NaN, p:Number = Number.NaN):Number {
        //    if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
        //    var s:Number;
        //    if (!a || a < Math.abs(c)) { a=c; s=p/4; }
        //    else s = p/(2*Math.PI) * Math.asin (c/a);
        //    if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
        //    return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
        //}
	
		/**
		 * Easing equation function for an elastic (exponentially decaying sine wave) easing out/in: deceleration until halfway, then acceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param a		Amplitude.
		 * @param p		Period.
		 * @return		The correct value.
		 */
        //public static float EaseOutInElastic (t:Number, b:Number, c:Number, d:Number, a:Number = Number.NaN, p:Number = Number.NaN):Number {
        //    if (t < d/2) return easeOutElastic (t*2, b, c/2, d, a, p);
        //    return easeInElastic((t*2)-d, b+c/2, c/2, d, a, p);
        //}
	
		/**
		 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in: accelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
		 * @return		The correct value.
		 */
        //public static float EaseInBack (t:Number, b:Number, c:Number, d:Number, s:Number = Number.NaN):Number {
        //    if (!s) s = 1.70158;
        //    return c*(t/=d)*t*((s+1)*t - s) + b;
        //}
	
		/**
		 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out: decelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
		 * @return		The correct value.
		 */
        //public static float EaseOutBack (t:Number, b:Number, c:Number, d:Number, s:Number = Number.NaN):Number {
        //    if (!s) s = 1.70158;
        //    return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
        //}
	
		/**
		 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in/out: acceleration until halfway, then deceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
		 * @return		The correct value.
		 */
        //public static float EaseInOutBack (t:Number, b:Number, c:Number, d:Number, s:Number = Number.NaN):Number {
        //    if (!s) s = 1.70158;
        //    if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
        //    return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
        //}
	
		/**
		 * Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out/in: deceleration until halfway, then acceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @param s		Overshoot ammount: higher s means greater overshoot (0 produces cubic easing with no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent).
		 * @return		The correct value.
		 */
        //public static float EaseOutInBack (t:Number, b:Number, c:Number, d:Number, s:Number = Number.NaN):Number {
        //    if (t < d/2) return easeOutBack (t*2, b, c/2, d, s);
        //    return easeInBack((t*2)-d, b+c/2, c/2, d, s);
        //}
	
		/**
		 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in: accelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInBounce (float t, float b, float c, float d) {
			return c - EaseOutBounce (d-t, 0, c, d) + b;
		}
	
		/**
		 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out: decelerating from zero velocity.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutBounce (float t, float b, float c, float d) {
			if ((t/=d) < (1/2.75)) {
				return c*(7.5625f*t*t) + b;
			} else if (t < (2/2.75)) {
				return c*(7.5625f*(t-=(1.5f/2.75f))*t + .75f) + b;
			} else if (t < (2.5/2.75)) {
				return c*(7.5625f*(t-=(2.25f/2.75f))*t + .9375f) + b;
			} else {
				return c*(7.5625f*(t-=(2.625f/2.75f))*t + .984375f) + b;
			}
		}
	
		/**
		 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out: acceleration until halfway, then deceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseInOutBounce (float t, float b, float c, float d) {
			if (t < d/2) return EaseInBounce (t*2, 0, c, d) * .5f + b;
			else return EaseOutBounce (t*2-d, 0, c, d) * .5f + c*.5f + b;
		}
	
		/**
		 * Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in: deceleration until halfway, then acceleration.
		 *
		 * @param t		Current time (in frames or seconds).
		 * @param b		Starting value.
		 * @param c		Change needed in value.
		 * @param d		Expected easing duration (in frames or seconds).
		 * @return		The correct value.
		 */
		public static float EaseOutInBounce (float t, float b, float c, float d) {
			if (t < d/2) return EaseOutBounce (t*2, b, c/2, d);
			return EaseInBounce((t*2)-d, b+c/2, c/2, d);
		}
	}

 
    #endregion
}