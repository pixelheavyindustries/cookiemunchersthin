using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CookieManagerBase {

    private static Dictionary<string, object> _cache;

    public static void SetCookie(string key, string val)
    {
        if (_cache == null)
            _cache = new Dictionary<string, object>();

        _cache[key] = val;
        PlayerPrefs.SetString(key, val);
    }

    public static void SetCookie(string key, float val)
    {
        if (_cache == null)
            _cache = new Dictionary<string, object>();

        _cache[key] = val;
        PlayerPrefs.SetFloat(key, val);
    }

    public static void SetCookie(string key, int val)
    {
        if (_cache == null)
            _cache = new Dictionary<string, object>();

        _cache[key] = val;
        PlayerPrefs.SetInt(key, val);
    }

    public static string GetStringCookie(string key, string defVal)
    {
        if (_cache == null)
            _cache = new Dictionary<string, object>();

        if (_cache.ContainsKey(key))
            return (string)_cache[key];

        if (!PlayerPrefs.HasKey(key))
        {
            PlayerPrefs.SetString(key, defVal);
            _cache[key] = defVal;
            return defVal;
        }

        return PlayerPrefs.GetString(key);
    }

    public static float GetFloatCookie(string key, float defVal)
    {
        if (_cache == null)
            _cache = new Dictionary<string, object>();

        if (_cache.ContainsKey(key))
            return (float)_cache[key];

        if (!PlayerPrefs.HasKey(key))
        {
            PlayerPrefs.SetFloat(key, defVal);
            _cache[key] = defVal;
            return defVal;
        }

        return PlayerPrefs.GetFloat(key);
    }

    public static int GetIntCookie(string key, int defVal)
    {
        if (_cache == null)
            _cache = new Dictionary<string, object>();

        if (_cache.ContainsKey(key))
            return (int)_cache[key];

        if (!PlayerPrefs.HasKey(key))
        {
            PlayerPrefs.SetInt(key, defVal);
            _cache[key] = defVal;
            return defVal;
        }

        return PlayerPrefs.GetInt(key);
    }
}
