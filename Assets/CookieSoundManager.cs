using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CookieSoundManager : SoundManager {

    private static Dictionary<string, AudioClip> _cookieSounds;

    public static void PrecachedSounds()
    {
        LoadAndCacheSound("Sound/MenuMusic");
        LoadAndCacheSound("Sound/WinMusic");
    }

    private static AudioClip LoadAndCacheSound(string path)
    {
        if (_cookieSounds == null)
            _cookieSounds = new Dictionary<string, AudioClip>();

        if (!_cookieSounds.ContainsKey(path))
        {
            var sound = Resources.Load(path) as AudioClip;
            _cookieSounds.Add(path, sound);
        }

        return _cookieSounds[path];
    }

    public static void PlayRandomCookieMunch()
    {
        var path = string.Format("Sound/Cookie{0}", UnityEngine.Random.Range(1, 3));
        Play(LoadAndCacheSound(path));
    }

    public static void PlayAsteroidSmash()
    {
        Play(LoadAndCacheSound("Sound/AsteroidSmash"));
    }

    public static void PlayCoinSound()
    {
        Play(LoadAndCacheSound("Sound/CoinSound1"));
    }

    public static void PlayBonusBoostSound()
    {
        Play(LoadAndCacheSound("Sound/SmallExplosion"));
    }

    public static void PlayVendingMachineSound()
    {
        Play(LoadAndCacheSound("Sound/VendingMachineDrop"));
    }

    public static void PlayMenuMusic()
    {
        Play(LoadAndCacheSound("Sound/MenuMusic"));
    }

    public static void PlayWinMusic()
    {
        Play(LoadAndCacheSound("Sound/WinMusic"));
    }

}
