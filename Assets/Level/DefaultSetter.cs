using System;
using UnityEngine;
using System.Collections;
using DifficultyLevel = ClimberMain.DifficultyLevel;

public class DefaultSetter {
    public const string VERSION = "1.1.001";
    public const int NUMERIC_VERSION = 110001;   //format == xx.yy.zzz --> xxyyzzz
    public const int MINIMUM_VERSION = 90008;   //minimum version necessary for us not to reset defaults, which will clear game progress
    public const float MAXSPEED = 28f;

    public static void SetDefaults()
    {
//        Debug.Log("Resetting defauults");

        //TODO: save off purchases
        var muted = Prefs.Muted;
        var pid = Prefs.Pid;
        var premiumPackUnlocked = Prefs.PremiumPackUnlocked;
        PlayerPrefs.DeleteAll();

        //TODO: restore purchases
        Prefs.Muted = muted;
        Prefs.Pid = pid;
        Prefs.Version = DefaultSetter.NUMERIC_VERSION;
        Prefs.PremiumPackUnlocked = premiumPackUnlocked;

        Prefs.ZipperSpeed = 45f;
        Prefs.SetBonusBoostLevel(4);
        Prefs.SetBonusBoostSpeed(40f);

        foreach (UpgradeType type in Enum.GetValues(typeof(UpgradeType)))
            Prefs.SetUpgradeLevel(type, 0);

        Prefs.HighestDifficultyUnlocked = (int)DifficultyLevel.Expert;
		Prefs.HaveShownHardInstructions = false;
		Prefs.HaveShownExpertInstructions = false;

        foreach (var d in Enum.GetValues(typeof(DifficultyLevel)))
        {
            var theDiff = (DifficultyLevel)d;
			Prefs.SetEndlessRecord(theDiff, 0f);
            Prefs.SetHighestMissionLevelUnlocked(theDiff, 1);

            for (var i = 1; i < Prefs.GetHighestMissionLevelPossible(theDiff); i++)
                Prefs.SetStarScore(theDiff, i, 0);
        }
    }

    public static void SetConstantParams()
    {
        Prefs.SetBonkProofingMultiplier(3f);

        var diff = ClimberMain.DifficultyLevel.Easy;
        Prefs.SetHighestMissionLevelPossible(diff, 15);
        Prefs.SetMaxSpeed(diff, MAXSPEED);
        Prefs.SetCookieSeparation(diff, 1.1f);
        Prefs.SetStaminaBurn(diff, false, 6f);
        Prefs.SetStaminaBurn(diff, true, 7f);
        Prefs.SetStaminaFill(diff, false, 3.5f);
        Prefs.SetStaminaFill(diff, true, 3.5f);
        Prefs.SetWinAltitude(diff, false, 210f);
        Prefs.SetWinAltitude(diff, true, 1900f);
        Prefs.Set2StarScore(diff, false, 80f);
        Prefs.Set2StarScore(diff, true, 90f);
        Prefs.Set3StarScore(diff, false, 70f);
        Prefs.Set3StarScore(diff, true, 63f);
        Prefs.SetGoodCookies(diff, false, .25f);
        Prefs.SetGoodCookies(diff, true, .22f);
        Prefs.SetBonusOccurence(diff, false, .05f);
        Prefs.SetBonusOccurence(diff, true, .03f);
        Prefs.SetObstacleOccurence(diff, true, 0f);
        Prefs.SetObstacleOccurence(diff, false, 0f);
        Prefs.SetGoodCookieMaxSeparation(diff, false, 3f);
        Prefs.SetGoodCookieMaxSeparation(diff, true, 3.5f);
        Prefs.SetCookieMoveTime(diff, false, 1000f);
        Prefs.SetCookieMoveTime(diff, true, 1000f);


        diff = DifficultyLevel.Hard;
        Prefs.SetHighestMissionLevelPossible(diff, 15);
        Prefs.SetMaxSpeed(diff, MAXSPEED);
        Prefs.SetCookieSeparation(diff, 1.1f);
        Prefs.SetStaminaBurn(diff, false, 6f);
        Prefs.SetStaminaBurn(diff, true, 7.5f);
        Prefs.SetStaminaFill(diff, false, 4.0f);
        Prefs.SetStaminaFill(diff, true, 3.7f);
        Prefs.SetWinAltitude(diff, false, 1300f);
        Prefs.SetWinAltitude(diff, true, 2100f);
        Prefs.SetGoodCookies(diff, false, .35f);
        Prefs.SetGoodCookies(diff, true, .27f);
        Prefs.SetBonusOccurence(diff, false, .05f);
        Prefs.SetBonusOccurence(diff, true, .05f);
        Prefs.SetObstacleOccurence(diff, false, .04f);
        Prefs.SetObstacleOccurence(diff, true, .07f);
        Prefs.SetGoodCookieMaxSeparation(diff, false, 4f);
        Prefs.SetGoodCookieMaxSeparation(diff, true, 4.5f);
        Prefs.SetCookieMoveTime(diff, false, 100f);
        Prefs.SetCookieMoveTime(diff, true, 10f);
        Prefs.SetAsteroidDamage(diff, false, 15f);
        Prefs.SetAsteroidDamage(diff, true, 15f);
        Prefs.Set2StarScore(diff, false, 90f);
        Prefs.Set2StarScore(diff, true, 90f);
        Prefs.Set3StarScore(diff, false, 81f);
        Prefs.Set3StarScore(diff, true, 81f);


        diff = DifficultyLevel.Expert;
        Prefs.SetHighestMissionLevelPossible(diff, 15);
        Prefs.SetMaxSpeed(diff, MAXSPEED);
        Prefs.SetCookieSeparation(diff, 1f);
        Prefs.SetStaminaBurn(diff, false, 7f);
        Prefs.SetStaminaBurn(diff, true, 7.7f);
        Prefs.SetStaminaFill(diff, false, 4.0f);
        Prefs.SetStaminaFill(diff, true, 3.6f);
        Prefs.SetWinAltitude(diff, false, 1700f);
        Prefs.SetWinAltitude(diff, true, 2200f);
        Prefs.SetGoodCookies(diff, false, .4f);
        Prefs.SetGoodCookies(diff, true, .3f);
        Prefs.SetBonusOccurence(diff, false, .05f);
        Prefs.SetBonusOccurence(diff, true, .03f);
        Prefs.SetObstacleOccurence(diff, false, .04f);
        Prefs.SetObstacleOccurence(diff, true, .07f);
        Prefs.SetGoodCookieMaxSeparation(diff, false, 4f);
        Prefs.SetGoodCookieMaxSeparation(diff, true, 4.5f);
        Prefs.SetCookieMoveTime(diff, false, 100f);
        Prefs.SetCookieMoveTime(diff, true, 10f);
        Prefs.SetAsteroidDamage(diff, false, 25f);
        Prefs.SetAsteroidDamage(diff, true, 25f);
        Prefs.Set2StarScore(diff, false, 92f);
        Prefs.Set2StarScore(diff, true, 92f);
        Prefs.Set3StarScore(diff, false, 88f);
        Prefs.Set3StarScore(diff, true, 86f);
    }
}
