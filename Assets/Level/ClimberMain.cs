using System;
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class ClimberMain : LevelBase, IAltitudeProvider{

    //not a singleton, but often convenient to access statically
    public static ClimberMain Instance;

    protected CookieMuncher _muncher;
    protected List<Cookie.Color> _allColors;
    protected List<Cookie.CookieShape> _allShapes;

    protected const float STAMINA_BAR_HEIGHT = 30f;
    protected FillBar _staminaBar;
//	public FillBar StaminaBar { get {return _staminaBar; }}
    protected BoosterButton _boosterButton;

    protected float _middleLine;
    protected float _cookieMuncherMaxScreenY;
    protected float _cookieFillTriggerLine;
    protected float _cookieFillStartLine;
    protected float _cookieFillHeight;
    protected float _averageCookieSeparation;
    protected float _goodCookiesFactor;
    protected float _goodCookieMaxSeparation;
    protected float _cullingDistance;
    protected float _cullingLine;
    protected float _levelLoseDistance;
    protected float _levelLostLine;
    protected float _obstacleOccurence;
	protected float _cookieFillScreenPortion = 3f/5f;

    protected BonusSpawner SnackSpawner;
    protected BonusSpawner ZipperSpawner;
    protected BonusSpawner CoinSpawner;
    protected BonusSpawner MinigameTokenSpawner;

    protected float _lastGoodCookiePoint = -1000;
    protected Vector3 _lastSpawnPosition;


    protected bool _initialized = false;

    protected float _missionStartTime;
    public float TotalMissionTime;
    public float TotalMissionDistance;
    protected bool _won = false;
    protected float _winAltitude = 0;
    protected bool _lost = false;

    protected float _nextCullTime = 0f;

    public List<CookieDefinition> MatchingCookieDefs;
    public List<Cookie.Color> BadColors;
    public List<Cookie.CookieShape> BadShapes;

    public enum DifficultyLevel
    {
        Easy = 0,
//        Medium,
        Hard,
        Expert,
    }
    public DifficultyLevel Difficulty;
    public float SubDifficulty;
    public int MissionLevel;

    protected Rect _pauseButtonRect;
    protected float _startPos;
	
	protected bool _showCookieTargeting;

	// Use this for initialization
    public virtual void Start()
    {
        base.Start();

        Instance = this;
        Difficulty = (DifficultyLevel)Prefs.CurrentDifficulty;
        MissionLevel = Prefs.GetMissionLevel(Difficulty);
        SubDifficulty = (float)MissionLevel / (float)Prefs.GetHighestMissionLevelPossible(Difficulty);

        Prefs.SetTotalPlays(Difficulty, MissionLevel, Prefs.GetTotalPlays(Difficulty, MissionLevel) + 1);

        _allColors = new List<Cookie.Color>((Cookie.Color[])Enum.GetValues(typeof(Cookie.Color)));
        _allShapes = new List<Cookie.CookieShape>((Cookie.CookieShape[])Enum.GetValues(typeof(Cookie.CookieShape)));

        _missionStartTime = GameClock.CurrTime;

        _middleLine = 0f;
        _cookieFillStartLine = 0f;// -BlockSize;  //what should this be?
        _cookieFillTriggerLine = 0 - ScreenDimensions.BlockSize * ScreenDimensions.NumBlocksHigh;    //make sure we're well past this
        _cookieFillHeight = ScreenDimensions.ScreenDimensionsInMeters.y;
        _averageCookieSeparation = ScreenDimensions.BlockSize;//Prefs.GetCookieSeparation(Difficulty);
        _goodCookiesFactor = Prefs.GetGoodCookies(Difficulty, SubDifficulty);
        _goodCookieMaxSeparation = Prefs.GetGoodCookieMaxSeparation(Difficulty, SubDifficulty);

        SnackSpawner = new BonusSpawner(UpgradeType.StaminaSnack, true);
        CoinSpawner = new BonusSpawner(UpgradeType.Coins, true);
        ZipperSpawner = new BonusSpawner(UpgradeType.Zippers, true);
		MinigameTokenSpawner = new BonusSpawner(UpgradeType.MinigameToken, false);

        _obstacleOccurence = Prefs.GetObstacleOccurence(Difficulty, SubDifficulty);

        _cullingDistance = ScreenDimensions.ScreenDimensionsInMeters.y * .66f;
        _levelLoseDistance = ScreenDimensions.ScreenDimensionsInMeters.y / 2 + ScreenDimensions.BlockSize;

        //gauges
		MakeStaminaFillBar();

        var pauseButtonWidth = ScreenDimensions.GameAreaInPixels.width / 5f;
        _pauseButtonRect = new Rect(ScreenDimensions.GameAreaInPixels.x,
                    ScreenDimensions.GameAreaInPixels.yMax - pauseButtonWidth,
                    pauseButtonWidth,
                    pauseButtonWidth);

        gameObject.AddComponent<MuteButton>();

        _startPos = 0 - ScreenDimensions.ScreenDimensionsInMeters.y / 2 + ScreenDimensions.BlockSize;
        _muncher = CreateMuncher(Difficulty);
        _muncher.gameObject.transform.position = new Vector3(0, _startPos, -1);
        _cookieMuncherMaxScreenY = 0 - ScreenDimensions.ScreenDimensionsInMeters.y / 4;
        PopulateCookieMatchingData();

        DoCookieFill(_cookieFillStartLine, _cookieFillStartLine + 1);
        MakeBackgrounds(MissionLevel == 1, true);

        UpdatePositions();

        _initialized = true;

        PlatformSpecifics.CreateAd();
    }
	
    public bool ShowHardInstructions()
    {
        var likes = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetWideDialogRect(),
            Background = TextureCache.GetCachedTexture("Help/Flat/HowToPlayHard"),
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { likes });

        return true;
    }

    public bool ShowExpertInstructions()
    {
        var likes = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetWideDialogRect(),
            Background = TextureCache.GetCachedTexture("Help/Flat/HowToPlayExpert"),
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { likes });

        return true;
    }
	
	
	protected virtual void MakeStaminaFillBar()
	{
        var staminaBarSidePadding = ScreenDimensions.GameAreaInPixels.width/5;
        var staminaBarHeight = ScreenDimensions.GameAreaInPixels.height / 12f;
		
        _staminaBar = LevelSpawner.MakeFillBar(
            new Rect(ScreenDimensions.GameAreaInPixels.x + staminaBarSidePadding,
                 ScreenDimensions.GameAreaInPixels.yMin,
                ScreenDimensions.GameAreaInPixels.width - staminaBarSidePadding * 2,
                staminaBarHeight), TextureCache.GetCachedTexture("Gui/StaminaBarLabel"));
	}

    //testing, remove this
    protected int _goodCookiesRunningTotal = 0;

    protected virtual void DoCookieFill(float startPoint, float endPoint)
    {
        while (startPoint < endPoint)
        {
            FightingEntity fe;
            startPoint += _averageCookieSeparation;
            var newPos = new Vector3(	Random.Range(	ScreenDimensions.GameAreaInMeters.x + ScreenDimensions.BlockSize, 
														ScreenDimensions.GameAreaInMeters.xMax - ScreenDimensions.BlockSize), 
										startPoint, 0.3f);

            //make a bonus rather than a cookie?
            var rand = Random.Range(0f, 1f);
            if (!_muncher.Boosting && rand < _obstacleOccurence)
            {
                fe = LevelSpawner.SpawnObstacle(ScreenDimensions.BlockSize, Difficulty, SubDifficulty);
            }
            else
            {
                var alt = Camera.main.transform.position.y;
                fe = SnackSpawner.MaybeSpawnBonus(alt) ?? CoinSpawner.MaybeSpawnBonus(alt) ?? ZipperSpawner.MaybeSpawnBonus(alt) ?? MinigameTokenSpawner.MaybeSpawnBonus(alt);

                if (fe == null)
                {
                    var dist = startPoint - _lastGoodCookiePoint;
                    var maxDist = ScreenDimensions.BlockSize * _goodCookieMaxSeparation;
                    var forceGood = (dist > maxDist);
                    var goodCookieRand = Random.Range(0f, 1f);
                    var good = (goodCookieRand < _goodCookiesFactor) || forceGood;
                    if (good)
                    {
                        _goodCookiesRunningTotal++;
                        _lastGoodCookiePoint = startPoint;
                    }
                    else
                        _goodCookiesRunningTotal = 0;

                    fe = MakeCookie(good, Difficulty);
                    ((Cookie)fe).HomePos = newPos;
                }
            }
            _entities.Add(fe);
            fe.gameObject.transform.position = newPos;
            _lastSpawnPosition = fe.gameObject.transform.position;
        }
    }

    public void OnGUI()
    {		
        var style = PHIUtil.MakeButtonGUIStyle();
        if (GUI.Button(_pauseButtonRect,
            TextureCache.GetCachedTexture("Gui/PauseButton"), style))
        {
            Pause();
        }
		
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
		
		DialogSpawner.RenderCurrentDialog();
    }

    public void Pause()
    {
        if (!GameClock.Paused)
        {
            GameClock.Paused = true;
            DialogSpawner.MakePauseDialog();
        }
    }

    public void PauseDialogUnpause()
    {
        GameClock.Paused = false;
    }
	
	// Update is called once per frame
	public void Update () {
        var start = DateTime.Now;
        if (!_initialized)
            return;

        if (GameClock.Paused)
            return;

        base.Update();

        if (_won || _muncher.transform.position.y > _winAltitude)
        {
            WinLevel();
            return;
        }
        if (_lost)
        {
            return;
        }

        UpdatePositions();
        HandleInput();

        if (Time.time > _nextCullTime)
        {
            CullEnts();
            _nextCullTime = Time.time + 1;
        }
		
		if (_staminaBar != null)
	        _staminaBar.FillAmount = _muncher.Stamina / _muncher.MaxStamina;
		
		//if their stamina is 0 but they're jumping for a good cookie, don't kill them 
        if (_muncher.Stamina <= 0 && !_muncher.Jumping)
            LoseLevel(LoseReason.Hungry);
        else if (_muncher.transform.position.y < _cullingLine)
            LoseLevel(LoseReason.Fell);
    }

    public void UpdatePositions()
    {
        if (_muncher.gameObject.transform.position.y > _middleLine + _cookieMuncherMaxScreenY)
            _middleLine = _muncher.gameObject.transform.position.y - _cookieMuncherMaxScreenY;

        Camera.main.transform.position = new Vector3(0, _middleLine, 0 - ScreenDimensions.CameraDistance);
        _cullingLine = _middleLine - _cullingDistance;
        _levelLostLine = _middleLine - _levelLoseDistance;

        DoCookieFill(_lastSpawnPosition.y, _middleLine + ScreenDimensions.ScreenDimensionsInMeters.y * _cookieFillScreenPortion);
    }

    protected void CullEnts()
    {
        var culledEnts = (from e in _entities where e.gameObject.transform.position.y < _cullingLine select e.Dead = true).ToList();
        //levelBase will destroy any dead ents on the next pass
    }

    protected virtual CookieMuncher CreateMuncher(DifficultyLevel difficulty)
    {
        var hasHorns = false;
        var shape = _allShapes[Random.Range(0, _allShapes.Count)];
        var baseColor = _allColors[Random.Range(1, _allColors.Count)];
        var spotColor = Cookie.Color.None;

        //CLEANUP move this switch to spawner
        switch (difficulty)
        {
            case DifficultyLevel.Easy:
                shape = Cookie.CookieShape.Circle;
                break;

            case DifficultyLevel.Expert:
                hasHorns = true;
                break;
        }

        Prefs.SetLastMuncherColor(baseColor);
        Prefs.SetLastMuncherShape(shape);
        var muncher = LevelSpawner.MakeMuncher(hasHorns, shape, baseColor, spotColor, "HappyFace1", ScreenDimensions.BlockSize, Difficulty, SubDifficulty, this).GetComponent<CookieMuncher>();
        muncher.LeftLimit = ScreenDimensions.GameAreaInMeters.x;
        muncher.RightLimit = ScreenDimensions.GameAreaInMeters.xMax;

        return muncher;
    }

    protected void PopulateCookieMatchingData()
    {
        MatchingCookieDefs = new List<CookieDefinition>();

        MatchingCookieDefs.Add(new CookieDefinition(_muncher.BaseColor, _muncher.Shape));

        if (Difficulty >= DifficultyLevel.Hard)
        {
            MatchingCookieDefs.Add(new CookieDefinition(null, _muncher.Shape));
            MatchingCookieDefs.Add(new CookieDefinition(_muncher.BaseColor, null));
        }

        BadColors = (from color in _allColors 
                     where color != _muncher.BaseColor && color != _muncher.SpotColor && color != Cookie.Color.None 
                     select color).ToList();
        BadShapes = (from eachShape in _allShapes
                     where eachShape != _muncher.Shape
                     select eachShape).ToList();
    }

    protected Cookie.Color RandomColor(bool good)
    {
        return BadColors[Random.Range(0, BadColors.Count)];
    }

    protected Cookie.CookieShape RandomShape(DifficultyLevel difficulty, bool good)
    {
        if (difficulty <= DifficultyLevel.Easy)
            return _muncher.Shape;

        return BadShapes[Random.Range(0, BadShapes.Count)];
    }

    protected Cookie MakeCookie(bool good, DifficultyLevel difficulty)
    {
        var cookieMoveTime = Prefs.GetCookieMoveTime(difficulty, SubDifficulty);

        if ((difficulty != DifficultyLevel.Expert && good) ||
            (difficulty == DifficultyLevel.Expert && !good))
        {
            var matchingCookie = MakeMatchingCookie(difficulty, cookieMoveTime);
            if (difficulty == DifficultyLevel.Expert)
                matchingCookie.Good = false;

            return matchingCookie;
        }

        //this may allow a few random "good" cookies to be spawned, but it's ok for now
        var shape = RandomShape(difficulty, false);
        var baseColor = RandomColor(false);
        var cookieTurnedOutGoodAnyway = _muncher.Likes(baseColor, shape) > 0;

        var cookie = LevelSpawner.MakeCookie(cookieTurnedOutGoodAnyway, shape, baseColor, ScreenDimensions.BlockSize, SubDifficulty, cookieMoveTime, _showCookieTargeting).GetComponent<Cookie>();
        return cookie;
    }

    protected Cookie MakeMatchingCookie(DifficultyLevel difficulty, float cookieMoveTime)
    {
        var index = Random.Range(0, MatchingCookieDefs.Count);
        var startingPoint = MatchingCookieDefs[index];
        var baseColor = startingPoint.BaseColor.GetValueOrDefault(RandomColor(true));
        var shape = startingPoint.Shape.GetValueOrDefault(RandomShape(difficulty, true));

        var cookie = LevelSpawner.MakeCookie(true, shape, baseColor, ScreenDimensions.BlockSize, SubDifficulty, cookieMoveTime, _showCookieTargeting).GetComponent<Cookie>();
        return cookie;
    }
	
	public void MakePointSplash(int comboTotal, Vector3 position)
	{
		var splash = LevelSpawner.MakePointSplash(comboTotal, position);
		_decorations.Add (splash);
	}
	
	public void MakeCrumbs(Vector3 position, int howMany)
	{
        var crumbs = GameObject.Instantiate(Resources.Load("Cookies/CookieCrumbsPrefab")) as GameObject;
        crumbs.transform.position = position + new Vector3(0, 0, 1);
		_decorations.Add (crumbs);
		if (_staminaBar != null)
			_staminaBar.Send (position, howMany);
	}
	
	public void MakeAsteroidSmash(Vector3 position)
	{
        var gravel = GameObject.Instantiate(Resources.Load("Bonuses/AsteroidBitsPrefab")) as GameObject;
        gravel.transform.position = position + new Vector3(0, 0, 1);
		_decorations.Add (gravel);
	}
	
    //ALLOW DOUBLE JUMP LATER ON
    protected virtual void HandleInput()
    {
        if (!Input.GetMouseButton(0))
        {
            _mouseDown = false;
        }
        else
        {
            //we don't want user leaving finger on screen and flipping successive rows in same place, so make sure they've removed it before flipping another pair
            if (!_mouseDown)
            {
                _mouseDown = true;

                //this is completely jacked, we need to go back and redo this in a more orderly fashion
                if (_boosterButton != null && _boosterButton.HitZone.Contains(new Vector2(Input.mousePosition.x, ScreenDimensions.GameAreaInPixels.height - Input.mousePosition.y)))
                {
                    _muncher.BoosterClick();
                    return;
                }

                var point3d = Camera.main.ScreenToWorldPoint(
                    new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0 - Camera.main.transform.position.z));

                var start = DateTime.Now;
                var closeObjs = (from h in _entities
                                    where (h.transform.position - point3d).sqrMagnitude < (ScreenDimensions.BlockSize * 2) * (ScreenDimensions.BlockSize * 2)
                                    && IsGrabbable(h.gameObject)
                                    select h).OrderBy(h => (h.transform.position - point3d).sqrMagnitude).ToList();

                if (closeObjs.Count > 0)
                {
                    var obj = closeObjs.First().gameObject;
                    _muncher.JumpForCookie(obj);
                }
            }
        }
    }

    protected bool IsGrabbable(GameObject go)
    {
        if (go.GetComponent<Cookie>() != null && go.GetComponent<Cookie>().Good)
            return true;

        if (go.GetComponent<StaminaSnack>() != null)
            return true;

        if (go.GetComponent<MinigameToken>() != null)
            return true;

        if (go.GetComponent<Zipper>() != null)
            return true;
		
        return false;
    }

    public float GetAltitude()
    {
        if (_muncher == null)
            return 0;

        if (_won)
            return _winAltitude;

        return _muncher.transform.position.y;
    }

    public virtual float GetWinAltitude()
    {
        return _winAltitude;
    }
	
	public List<Cookie> GetGoodCookies()
	{
		List<Cookie> goodCookies = new List<Cookie>();
		foreach (var e in _entities)
		{
			var cookie = e.GetComponent<Cookie>();
			if (cookie != null && cookie.Good)
				goodCookies.Add (cookie);
		}
		
		return goodCookies;
	}

    public enum LoseReason
    {
        Hungry,
        Fell
    }

    protected virtual void LoseLevel(LoseReason reason)
    {
    }

    public override void CreateLevelLighting()
    {
    }

    protected bool ShowTutorialBoosts()
    {
        var defZippers = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetDialogRect(),
            Background = (Texture2D)Resources.Load("Help/Flat/Zippers"),
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };

        var defComboBoost = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetDialogRect(),
            Background = (Texture2D)Resources.Load("Help/Flat/ComboBoost"),
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { defZippers, defComboBoost });

        return true;
    }

	public override void OnDestroy()
	{
		if (_muncher != null && _muncher.gameObject != null)
			Destroy(_muncher.gameObject);
		
		base.OnDestroy();
	}
}
