using UnityEngine;
using System.Collections;

public class BonusSpawner {
    protected UpgradeType _upgradeType;
    protected float _nextSpawnAt = 0;
    protected float _standardSeparation;
    protected bool _doSpawns;

    public BonusSpawner(UpgradeType type, bool doSpawns)
    {
        _upgradeType = type;
        _standardSeparation = UpgradeManager.GetUpgradeValue(type);
        _nextSpawnAt = Random.Range(_standardSeparation / 10f, _standardSeparation);
        _doSpawns = doSpawns;
    }

    protected float GetNextSpawnAt()
    {
        var next = _nextSpawnAt + Random.Range(_standardSeparation / 2, _standardSeparation * 2);
        return next;
    }

    public FightingEntity MaybeSpawnBonus(float alt)
    {
        if (!_doSpawns)
            return null;

        if (alt >= _nextSpawnAt)
        {
            _nextSpawnAt = GetNextSpawnAt();
            switch (_upgradeType)
            {
                case UpgradeType.Coins:
                    return LevelSpawner.MakeCoin(ScreenDimensions.BlockSize);
                case UpgradeType.Zippers:
                    return LevelSpawner.MakeZipper(ScreenDimensions.BlockSize, ClimberMain.Instance.Difficulty);
                case UpgradeType.StaminaSnack:
                    return LevelSpawner.MakeStaminaSnack(ScreenDimensions.BlockSize);
//                case UpgradeType.Donut:
//                    return LevelSpawner.MakeDonut(ScreenDimensions.BlockSize);
            }
        }

        return null;
    }
}
