using System;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class LevelBase : MonoBehaviour {

    protected Tweenk _tweenk;
    protected List<FightingEntity> _entities;
	protected List<GameObject> _decorations;
    protected GameObject _clickSurface;

    protected bool _mouseDown = false;

    // Use this for initialization
    public virtual void Start()
    {
        CookieSoundManager.PrecachedSounds();

        //_dimensions.NumBlocksWide = 14;
        _tweenk = new GameObject("TweenkHolder").AddComponent<Tweenk>();

        //game clock needs to be added to something to get its static _instance setup
        new GameObject("GameClockHolder").AddComponent<GameClock>();
        GameClock.Paused = false;

        _entities = new List<FightingEntity>();
		_decorations = new List<GameObject>();

        gameObject.AddComponent<LevelSpawner>();

        CreateLevelLighting();

        Prefs.MoneyThisLevel = 0;
    }

    private Vector3 Get3DPoint(float x, float y)
    {
		return Camera.main.ScreenToWorldPoint(new Vector3(x,y, 1f));
    }

    public virtual void CreateLevelLighting()
    {
/*        GameObject skyLight = new GameObject("MainLight");
        skyLight.AddComponent<Light>();
        skyLight.light.type = LightType.Directional;
        skyLight.light.color = new Color(0.8f, 0.8f, 0.8f);
        skyLight.light.transform.Rotate(45, 0, 0);
        skyLight.light.transform.position = new Vector3(0, 20, 0);*/
    }

    // Update is called once per frame
    public virtual void Update()
    {
        SweepDeadEnts();
    }

    protected virtual void SweepDeadEnts()
    {
        var deadEnts = (from e in _entities where e.Dead select e).ToList();

        foreach (var de in deadEnts)
        {
            if (de.DeadReplacement != null)
            {
                var replacement = de.DeadReplacement.Replacement();
                replacement.gameObject.transform.position = de.gameObject.transform.position + de.DeadReplacement.Offset;
                var feScript = replacement.gameObject.GetComponent<FightingEntity>();
                if (feScript != null)
                    feScript.FakeVelocity = de.FakeVelocity;
            }
            _entities.Remove(de);
            Destroy(de.gameObject);
        }
		
		//only do a sweep of decorations every so often
		if (_decorations.Count > 50)
		{
			var livingDecs = from d in _decorations where d != null select d;
			_decorations = livingDecs.ToList();
			//Debug.Log("Swept decs, found this many: " + _decorations.Count);
		}
    }

    protected virtual void LoseLevel()
    {
    }

    protected virtual void WinLevel()
    {
    }

    protected virtual void WinGame()
    {
    }

    protected void MakeBackgrounds(bool makeSky, bool makePlatform = true)
    {
        var grassObject = GameObject.Find("Grass");
        var rockObject = GameObject.Find("Rock");

        if (makeSky)
            Destroy(rockObject);
        else
            Destroy(grassObject);

        return;

        var diff = (ClimberMain.DifficultyLevel)Prefs.CurrentDifficulty;
        var mission = Prefs.GetMissionLevel(diff);

        //if we're on the first mission, need to show the sky background
        if (makePlatform)
        {
            if (makeSky)
            {
                var grass = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended, "Grass", 300, true, 1, 1);
                var bbProps = grass.GetComponent<BillboardProps>();
                bbProps.SetWidthInMeters(ScreenDimensions.GameAreaInMeters.width * 1.3f);
                var grassHeight = bbProps.GetDimensionsInMeters().y;
                grass.transform.position = new Vector3(0, 0 - grassHeight / 2, 2f);
            }
            else
            {
                var rock = SpawnerBase.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended, "Rock", 300, true, 1, 1);
                var bbProps = rock.GetComponent<BillboardProps>();
                bbProps.SetWidthInMeters(ScreenDimensions.BlockSize * 10);
                rock.transform.position = new Vector3(0, 0 - ScreenDimensions.BlockSize * 3, 2);
            }
        }
    }
	
	
	public virtual void OnDestroy()
	{
//		var allDecorations = GameObject.FindGameObjectsWithTag("Enemy");
//		foreach(var e in allDecorations)
//			Destroy (e);
		
		foreach(var e in _entities)
		{
			if (e != null)
				Destroy(e.gameObject);
		}
		
		foreach(var dec in _decorations)
		{
			if (dec != null)
				Destroy(dec);
		}
	}
}
