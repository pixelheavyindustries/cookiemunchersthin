using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ClassicMode : ClimberMain {

    public override void Start()
    {
        base.Start();

        if (Difficulty == DifficultyLevel.Easy && MissionLevel == 2)
            _tweenk.Callback(.5f, ShowEasyInstructions1);
        else if (Difficulty == DifficultyLevel.Easy && MissionLevel == 3)
            _tweenk.Callback(.5f, ShowEasyInstructions2);
        else if (Difficulty == DifficultyLevel.Easy && MissionLevel == 4)
            _tweenk.Callback(.5f, ShowEasyInstructions3);
        else if (Difficulty == DifficultyLevel.Hard && MissionLevel == 1)
            _tweenk.Callback(.5f, ShowHardInstructions);
        else if (Difficulty == DifficultyLevel.Expert && MissionLevel == 1)
            _tweenk.Callback(.5f, ShowExpertInstructions);

        _winAltitude = GetWinAltitude();
        var finishLine = LevelSpawner.MakeBillboard(SpawnerBase.ShaderTypeAlphaBlended, "FinishLine", 100, true, 1, 1);
        var finishLineBB = finishLine.GetComponent<BillboardProps>() as BillboardProps;
        finishLineBB.SetWidthInMeters(ScreenDimensions.GameAreaInMeters.width * 1.2f);
        finishLine.transform.position = new Vector3(0, _winAltitude, 10);
    }

    public override float GetWinAltitude()
    {
        return Prefs.GetWinAltitude(Difficulty, SubDifficulty);
    }

    protected override void WinLevel()
    {
        if (!_won)
        {
            TotalMissionTime = GameClock.CurrTime - _missionStartTime;
			TotalMissionDistance = Camera.main.transform.position.y;
            var isFirstWin = MissionLevel == Prefs.GetHighestMissionLevelUnlocked(Difficulty);
            Logging.LogLevelWin((int)TotalMissionTime, isFirstWin);

            _muncher.FlyOffScreen();
            base.WinLevel();
            Func<bool> wi;

            var currStarScore = Prefs.GetStarScore(Difficulty, MissionLevel);
            var newStarScore = 1;
            if (TotalMissionTime <= Prefs.Get2StarScore(Difficulty, SubDifficulty))
                newStarScore = 2;
            if (TotalMissionTime <= Prefs.Get3StarScore(Difficulty, SubDifficulty))
                newStarScore = 3;
            Prefs.SetStarScore(Difficulty, MissionLevel, Math.Max(currStarScore, newStarScore));
			Prefs.SetLastStarScore(Math.Max(currStarScore, newStarScore));
			//Debug.Log ("Set start score" + Prefs.GetStarScore(Difficulty, MissionLevel));

            if (MissionLevel == Prefs.GetHighestMissionLevelPossible(Difficulty))
            {
                if (Difficulty == DifficultyLevel.Expert)
                    wi = delegate() { MenuManager.PushScreen(MenuManager.OUTRO_EXPERT); Application.LoadLevel("MenuMain"); return true; };
                else if (Difficulty == DifficultyLevel.Hard)
                    wi = delegate() { MenuManager.PushScreen(MenuManager.OUTRO_HARD); Application.LoadLevel("MenuMain"); return true; };
                else
                    wi = delegate() { MenuManager.PushScreen(MenuManager.OUTRO_EASY); Application.LoadLevel("MenuMain"); return true; };
            }
            else
            {
                wi = delegate() { DialogSpawner.MakeWinDialog(); return true; };

                Prefs.SetHighestMissionLevelUnlocked(Difficulty,
                    Math.Max(MissionLevel + 1, Prefs.GetHighestMissionLevelUnlocked(Difficulty)));
				
	            Prefs.SetMissionLevel(Difficulty, Prefs.GetMissionLevel(Difficulty) + 1);
				Prefs.ShowNextLevelButtonOnVending = true;
		        MenuProps.ScreenStack.Add(MenuManager.UPGRADES);
            }

            _tweenk.Callback(1.4f, wi);
        }

        _won = true;
    }

    protected override void LoseLevel(LoseReason reason)
    {
        _lost = true;
        GameClock.Paused = true;

        TotalMissionTime = GameClock.CurrTime - _missionStartTime;
		TotalMissionDistance = Camera.main.transform.position.y;
        Logging.LogLevelFail((int)TotalMissionTime);

        if (reason == LoseReason.Fell)
            DialogSpawner.MakeLoseDialogFall();
        else
            DialogSpawner.MakeLoseDialogHungry();
    }

    public bool ShowEasyInstructions1()
    {
        var dontEat = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetWideDialogRect(),
            Background = TextureCache.GetCachedTexture("Help/Flat/DontEatAllCookies"),
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { dontEat });

        return true;
    }
	
    public bool ShowEasyInstructions2()
    {
		var tummyBarPos = Screen.height * .92f;
		var texture = TextureCache.GetCachedTexture("Help/Flat/TummyMeter");
        var likes = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetWideArrowUpDialogRect(tummyBarPos, new Vector2(texture.width, texture.height)),
            Background = texture,
            ContentRect = DialogSpawner.GetArrowUpDialogContentRect(tummyBarPos)
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { likes });

        return true;
    }

    public bool ShowEasyInstructions3()
    {
        var likes = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetWideDialogRect(),
            Background = TextureCache.GetCachedTexture("Help/Flat/Boosts"),
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { likes });

        return true;
    }
	
	

}
