using UnityEngine;
using System.Collections;

public class Culler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
    void OnTriggerEnter(Collider other)
    {
        var ingredientScript = other.gameObject.GetComponent<Cookie>();
        //print("Culler hit " + other.gameObject + "   " + ingredientScript);

        if (ingredientScript != null)
        {
            //print("   Destroying ingredient " + ingredientScript);
            Destroy(other.gameObject);
        }
    }
}
