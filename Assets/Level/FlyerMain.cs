using UnityEngine;
using System.Collections;

public class FlyerMain : LevelBase {
	
	public FightingEntity Muncher {get; set;}
	
	private float _cameraBeginPos;
	private const float CAMERA_BEGIN_FACTOR = -0.4f;
	private float _cameraEndPos;
	private const float CAMERA_END_FACTOR = 0.4f;
	private CarrotControls _carrotControls;
	
	// Use this for initialization
	void Start () {
		base.Start ();
		var muncherObject = LevelSpawner.MakeFlyerMuncher(Prefs.GetLastMuncherShape(), Prefs.GetLastMuncherColor(), "HappyFace1", ScreenDimensions.BlockSize);
		Muncher = muncherObject.GetComponent<FlyerMuncher>();
		_carrotControls = Camera.main.gameObject.AddComponent<CarrotControls>();
		_carrotControls.Entity = Muncher;
		Muncher.FakeVelocity = new Vector3(0f, 10f, 0f);
		
		ScreenDimensions.Reinitialize(false);
		_cameraBeginPos = ScreenDimensions.GameAreaInMeters.y * CAMERA_BEGIN_FACTOR;
		_cameraEndPos = ScreenDimensions.GameAreaInMeters.y * CAMERA_END_FACTOR;
	}
	
	// Update is called once per frame
	void Update () {
		var timeFactor = 0f;
		var cameraY = GetCameraPos(Muncher.gameObject, _cameraBeginPos, _cameraEndPos, timeFactor);
        Camera.main.transform.position = new Vector3(0, cameraY, 0 - ScreenDimensions.CameraDistance);	
		
		if (Input.GetMouseButton(0))
		{
			var mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0 - Camera.main.transform.position.z));
			print ("Mouse: " + Input.mousePosition + "   " + mousePos);
			_carrotControls.FingerPosition = mousePos;
			_carrotControls.FingerDown = true;
		}
		else
			_carrotControls.FingerDown = false;
		
		
		base.Update();
	}
	
	private float GetCameraPos(GameObject muncher, float begin, float end, float timeFactor)
	{
		return muncher.transform.position.y;
	}
}
