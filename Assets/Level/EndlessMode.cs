using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndlessMode : ClimberMain {
    public override void Start()
    {
        base.Start();
        MissionLevel = 4;
        _winAltitude = float.MaxValue;
		
        if (Difficulty == DifficultyLevel.Hard && !Prefs.HaveShownHardInstructions)
		{
			Prefs.HaveShownHardInstructions = true;
			_tweenk.Callback(.5f, ShowHardInstructions);
		}
        else if (Difficulty == DifficultyLevel.Expert && !Prefs.HaveShownExpertInstructions)
		{
			Prefs.HaveShownExpertInstructions = true;
            _tweenk.Callback(.5f, ShowExpertInstructions);
		}
    }
	
	public void Update()
	{
		_muncher.StaminaBurn = GetStaminaBurn();
		
		base.Update();
	}
	
	protected float GetStaminaBurn()
	{
		var baseBurn = Prefs.GetStaminaBurn(Difficulty, SubDifficulty);
		var secondsToDoubleBurn = 300;
		var timeFactor = (GameClock.CurrTime - _missionStartTime) / secondsToDoubleBurn;
		var maxBurnMultiple = 4;
		
		return baseBurn * timeFactor * maxBurnMultiple;
	}

    protected override void LoseLevel(LoseReason reason)
    {
        _lost = true;
        GameClock.Paused = true;

//        TotalMissionTime = GameClock.CurrTime - _missionStartTime;
//        Logging.LogLevelFail((int)TotalMissionTime);

/*        if (TotalMissionTime > Prefs.GetEndlessRecord(Difficulty))
        {
            Prefs.SetEndlessRecord(Difficulty, TotalMissionTime);
            DialogSpawner.MakeLoseDialogEndlessRecord();
        }
        else
            DialogSpawner.MakeLoseDialogEndless();*/
		
		TotalMissionDistance = Camera.main.transform.position.y;
		//Debug.Log ("Complete: " + TotalMissionDistance);
        if (TotalMissionDistance > Prefs.GetEndlessHeightRecord(Difficulty))
        {
            Prefs.SetEndlessHeightRecord(Difficulty, TotalMissionDistance);
            DialogSpawner.MakeLoseDialogEndlessRecord();
        }
        else
            DialogSpawner.MakeLoseDialogEndless();
		
    }
}
