using UnityEngine;
using System.Collections;
using DifficultyLevel = ClimberMain.DifficultyLevel;

public class LevelComponentAdder : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var mode = Prefs.GetGameMode();
		switch (mode)
		{
		case Prefs.MODE_ENDLESS:
            Camera.main.gameObject.AddComponent<EndlessMode>();
			break;
		case Prefs.MODE_TRAINING:
            Camera.main.gameObject.AddComponent<Tutorial>();
			break;
		case Prefs.MODE_BONUSROUND:
            Camera.main.gameObject.AddComponent<BonusRound>();
			break;
		case Prefs.MODE_CLASSIC:
			if (Prefs.GetMissionLevel((ClimberMain.DifficultyLevel)Prefs.CurrentDifficulty) == 1 && Prefs.CurrentDifficulty == (int)DifficultyLevel.Easy)
    	        Camera.main.gameObject.AddComponent<Tutorial>();
			else
	            Camera.main.gameObject.AddComponent<ClassicMode>();
			break;
		}
    }
}
