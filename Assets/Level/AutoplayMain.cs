using UnityEngine;
using System.Collections;

public class AutoplayMain : ClimberMain {

	void Start () {
        base.Start();
		
        _winAltitude = float.MaxValue;
		
        SnackSpawner = new BonusSpawner(UpgradeType.StaminaSnack, false);
        CoinSpawner = new BonusSpawner(UpgradeType.Coins, false);
        ZipperSpawner = new BonusSpawner(UpgradeType.Zippers, false);
		MinigameTokenSpawner = new BonusSpawner(UpgradeType.MinigameToken, false);
		
        _cookieMuncherMaxScreenY = 0;// - ScreenDimensions.ScreenDimensionsInMeters.y / 5;
		_cookieFillScreenPortion = 1;
	}
	
	public void OnGUI()
	{}
	
	protected override void MakeStaminaFillBar()
	{
		//actually, DON'T.
	}
	
	
    protected override void HandleInput()
    {
		//and don't take any input either!
	}	
	
    protected override CookieMuncher CreateMuncher(DifficultyLevel difficulty)
    {
        var hasHorns = false;
        var shape = Cookie.CookieShape.Circle;
        var baseColor = _allColors[Random.Range(1, _allColors.Count)];
        var spotColor = Cookie.Color.None;

        Prefs.SetLastMuncherColor(baseColor);
        Prefs.SetLastMuncherShape(shape);
        var muncher = LevelSpawner.MakeAutoPilotMuncher(hasHorns, shape, baseColor, spotColor, "HappyFace1", ScreenDimensions.BlockSize, Difficulty, SubDifficulty, this).GetComponent<CookieMuncher>();
        muncher.LeftLimit = ScreenDimensions.GameAreaInMeters.x;
        muncher.RightLimit = ScreenDimensions.GameAreaInMeters.xMax;

        return muncher;
    }	
}
