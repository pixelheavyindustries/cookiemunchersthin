using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tutorial : ClassicMode {

    protected bool _cookieFillDone = false;
    protected Cookie _cookie1;

	// Use this for initialization
	void Start () {
		_showCookieTargeting = true;
        base.Start();		
        UpdatePositions();
        _tweenk.Callback(.5f, ShowTutorial2);
	}
	
	// Update is called once per frame
	void Update () {
        base.Update();
    }

    public override float GetWinAltitude()
    {
        return 200;
    }

    protected override CookieMuncher CreateMuncher(DifficultyLevel difficulty)
    {
        var muncher = LevelSpawner.MakeMuncher(false, Cookie.CookieShape.Circle, Cookie.Color.Orange, Cookie.Color.None, "HappyFace1", ScreenDimensions.BlockSize, Difficulty, SubDifficulty, this).GetComponent<CookieMuncher>();
        muncher.LeftLimit = ScreenDimensions.GameAreaInMeters.x;
        muncher.RightLimit = ScreenDimensions.GameAreaInMeters.xMax;

        Prefs.SetLastMuncherColor(Cookie.Color.Orange);
        Prefs.SetLastMuncherShape(Cookie.CookieShape.Circle);

        return muncher;
    }

    protected override void DoCookieFill(float startPoint, float endPoint)
    {
        if (_cookieFillDone)
            return;

        _cookieFillDone = true;

        MakeGoodCookieAt(new Vector3(-11f, 26f, 0f));
		var tapHere = LevelSpawner.MakeBillboard(LevelSpawner.ShaderTypeAlphaBlended, "Help/tapHere", 60, true, 1, 1);
		tapHere.transform.position = new Vector3(-5f, 29f, 0f);
		
        MakeGoodCookieAt(new Vector3(11f, 26f, 0f));
		var orHere = LevelSpawner.MakeBillboard(LevelSpawner.ShaderTypeAlphaBlended, "Help/orHere", 60, true, 1, 1);
		orHere.transform.position = new Vector3(5f, 29f, 0f);
  //      print("Spawning.  Game area: " + Dimensions.GameAreaInMeters.width);
  //      var trigger1 = LevelSpawner.MakeTrigger(ScreenDimensions.BlockSize, ShowTutorialSecondCookie);
  //      trigger1.gameObject.transform.position = new Vector3(0, 36f, 0);

        MakeBadCookieAt(new Vector3(-9f, 12f, 0f));
        MakeBadCookieAt(new Vector3(15f, 21f, 0f));
        MakeBadCookieAt(new Vector3(8f, 18f, 0f));
        MakeBadCookieAt(new Vector3(6f, 37f, 0f));

        MakeBadCookieAt(new Vector3(-12f, 41f, 0f));
        MakeBadCookieAt(new Vector3(9f, 63f, 0f));
        MakeBadCookieAt(new Vector3(-6f, 72f, 0f));
        MakeGoodCookieAt(new Vector3(0, 55, 0f));
        MakeGoodCookieAt(new Vector3(8f, 80.3f, 0f));
        MakeGoodCookieAt(new Vector3(-2f, 80, 0f));

//        MakeGoodCookieAt(new Vector3(-9f, 101, 0));
        MakeBadCookieAt(new Vector3(6f, 105, 0));
        MakeBadCookieAt(new Vector3(-3f, 109, 0));
        MakeBadCookieAt(new Vector3(12f, 113, 0));
  //      MakeGoodCookieAt(new Vector3(-6f, 122, 0));
        MakeGoodCookieAt(new Vector3(10f, 115, 0));
        MakeGoodCookieAt(new Vector3(0f, 115, 0));
        MakeBadCookieAt(new Vector3(12f, 140, 0));
//        MakeGoodCookieAt(new Vector3(-9f, 143, 0));
        MakeGoodCookieAt(new Vector3(-3f, 145, 0));
        MakeBadCookieAt(new Vector3(9f, 165, 0));
        MakeBadCookieAt(new Vector3(-12f, 167, 0));
        MakeGoodCookieAt(new Vector3(6f, 170, 0));
        MakeGoodCookieAt(new Vector3(-9f, 201, 0));
        MakeBadCookieAt(new Vector3(6f, 195, 0));
        MakeBadCookieAt(new Vector3(-3f, 199, 0));
        MakeBadCookieAt(new Vector3(12f, 213, 0));
        //MakeGoodCookieAt(new Vector3(-6f, 222, 0));
        MakeGoodCookieAt(new Vector3(3f, 231, 0));
        MakeBadCookieAt(new Vector3(9f, 240, 0));
        MakeBadCookieAt(new Vector3(-12f, 242, 0));
		
        MakeBadCookieAt(new Vector3(11f, 251, 0));
        MakeGoodCookieAt(new Vector3(2f, 247, 0));
        //MakeGoodCookieAt(new Vector3(-3f, 253, 0));
		
        MakeBadCookieAt(new Vector3(6f, 255, 0));
        MakeBadCookieAt(new Vector3(-5f, 262, 0));
        MakeBadCookieAt(new Vector3(9f, 268, 0));
        MakeGoodCookieAt(new Vector3(-1f, 271, 0));
       // MakeGoodCookieAt(new Vector3(7f, 276, 0));
		
        MakeGoodCookieAt(new Vector3(5f, 296, 0));
        MakeBadCookieAt(new Vector3(8f, 303, 0));
        MakeBadCookieAt(new Vector3(-9f, 305, 0));
        MakeBadCookieAt(new Vector3(4f, 315, 0));
        MakeBadCookieAt(new Vector3(-2f, 322, 0));
        //MakeGoodCookieAt(new Vector3(-3f, 310, 0));
        MakeGoodCookieAt(new Vector3(9f, 328, 0));
        MakeBadCookieAt(new Vector3(3f, 330, 0));
        MakeBadCookieAt(new Vector3(-5f, 335, 0));
        MakeBadCookieAt(new Vector3(6f, 345, 0));
        MakeBadCookieAt(new Vector3(-2f, 365, 0));
        MakeGoodCookieAt(new Vector3(1f, 340, 0));
		
    }

    protected Cookie MakeGoodCookieAt(Vector3 pos)
    {
		return MakeCookieAt(pos, true);
	}
	
    protected Cookie MakeBadCookieAt(Vector3 pos)
    {
		return MakeCookieAt(pos, false);
	}
	
    protected Cookie MakeCookieAt(Vector3 pos, bool good)
    {
        var fe = MakeCookie(good, Difficulty);
        var cookieScript = fe.GetComponent<Cookie>();
        cookieScript.HomePos = fe.transform.position = pos;
        cookieScript.CookieMoveDist = 0f;
        _entities.Add(fe);
		return fe;
    }

    protected void MakeSnackAt(Vector3 pos)
    {
        var fe = LevelSpawner.MakeStaminaSnack(ScreenDimensions.BlockSize) as StaminaSnack;
        var snackScript = fe.GetComponent<Cookie>();
        fe.transform.position = pos;
        _entities.Add(fe);
    }
	
    protected bool ShowTutorial2()
    {
        /*var muncherScreenPos = Camera.main.WorldToScreenPoint(_muncher.transform.position);
        var pointAt1 = muncherScreenPos.y * 1.07f;  //1.07 to give it a little extra padding, so we're not pointing at the middle
        var defThisIsMuncher = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetArrowDownDialogRect(pointAt1),
            Background = TextureCache.GetCachedTexture("Help/Flat/Likes"),
            ContentRect = DialogSpawner.GetArrowDownDialogContentRect(pointAt1)
        };

        var cookieScreenPos = Camera.main.WorldToScreenPoint(_cookie1.HomePos);
        var pointAt2 = cookieScreenPos.y * .93f;
        var defClickThisCookie = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetArrowUpDialogRect(pointAt2),
            Background = TextureCache.GetCachedTexture("Help/Flat/JumpForCookie"),
            ContentRect = DialogSpawner.GetArrowUpDialogContentRect(pointAt2)
        };*/
		
		var texture = TextureCache.GetCachedTexture("Help/Flat/MainTutorial");
        var defMainTutorial = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = DialogSpawner.GetWideDialogRect(),
            Background = texture,
            ContentRect = DialogSpawner.GetDialogContentRectWide()
        };		

        DialogSpawner.ShowDialogs(new List<DialogDef> { defMainTutorial });
        return true;
    }
	
	protected override void WinLevel()
	{
        if (!_won)
        {
            TotalMissionTime = GameClock.CurrTime - _missionStartTime;
            var isFirstWin = MissionLevel == Prefs.GetHighestMissionLevelUnlocked(Difficulty);
            Logging.LogLevelWin((int)TotalMissionTime, isFirstWin);

            _muncher.FlyOffScreen();
            Func<bool> wi;

            wi = delegate() { DialogSpawner.MakeWinDialog(); return true; };

            Prefs.SetStarScore(Difficulty, MissionLevel, 3);
			Prefs.SetLastStarScore(3);
			
//            Prefs.SetTrainingComplete(true);
            Prefs.SetHighestMissionLevelUnlocked(Difficulty,
                Math.Max(MissionLevel + 1, Prefs.GetHighestMissionLevelUnlocked(Difficulty)));
			
            Prefs.SetMissionLevel(Difficulty, Prefs.GetMissionLevel(Difficulty) + 1);
			Prefs.ShowNextLevelButtonOnVending = true;
	        MenuProps.ScreenStack.Add(MenuManager.UPGRADES);

            _tweenk.Callback(1.4f, wi);
        }

        _won = true;
	}

/*    protected bool ShowTutorialSecondCookie()
    {
        var climbRect = DialogSpawner.GetDialogRect();
        var defClimbToTop = new DialogDef
        {
            RenderContents = DialogSpawner.RenderFullScreenButton,
            Rect = climbRect,
            Background = TextureCache.GetCachedTexture("Help/Flat/ClimbToTop"),
            ContentRect = DialogSpawner.GetDialogContentRectWide(),
        };

        DialogSpawner.ShowDialogs(new List<DialogDef> { defClimbToTop });
        return true;
    }*/

}
