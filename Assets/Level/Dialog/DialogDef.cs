using System;
using UnityEngine;
using System.Collections;

public class DialogDef {
    public Func<DialogDef, bool> RenderContents;
    public Texture2D Background;
    public Rect? Rect;
    public Rect ContentRect;
	public String ExtraData;
}
